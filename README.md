﻿ Real-estate

Link to Db-schema
https://my.vertabelo.com/model/a9VcfblHMTJVMCBbhgGw1f5ZwypphJCP

It is a website to sell and buy real estates.

Has two properties to buy:

1.  Users can buy directly

2. Users can go into auction

The website has 7 main pages:

*  Home Page
	
	header
	has a call to action
	filter
	top properties
	testimonials
	how we operate
	footer
	
	
*  Listing for sale

	header
	list of properties
	buttons led to specific details
	filter
	contact form
	footer

*  Online Auction

	header
	list of properties
	button led to the auction
	filter
	contact form
	footer

*  About us

	header 
	about us text
	about us image
	our team
	footer 

*  Sell

	header 
	form to upload images, and information
	footer
	
*  News

	header
	posts
	images
	pagination
	footer 

*  Contact us

	header
	contact form
	contact information
	google map

In every page there is a call to action (search box)

The two situations: 

1.  The user should see a list of real estates that he/she can buy it directly,
  with images, video,price, and a details description

2.  The user should see a list of real estate that he/she can bids on it.
  He has access on the current bid.
  If he/she wanna go into it,he/she should sign up after agree on terms.
    
  

