import React, { Component } from "react";
import { withRouter } from "react-router";
//Components
import Test from "./Component/Test.js";
import About from "./Component/About.js";
import Contact from "./Component/Contact.js";
import Sell from "./Component/sell.js";
import Single from "./Component/listing_single.js";
import News from "./Component/news.js";
import Listing from "./Component/listing.js";
import Oneline from "./Component/Onlinelisting.js";
import Singleauction from "./Component/single_auction.js";
import Dashboard from "./Component/dashboard/main";
import Terms from "./Component/terms";
import Search from "./Component/smallComponent/search.js";
import Signin from "./Component/LoginButton";
import Dash from './Component/dashboard/main'
//Route
import { BrowserRouter as Router, Route } from "react-router-dom";

//CSS
import "bootstrap/dist/css/bootstrap.min.css";
import "./Component/styles/about_responsive.css";
import "./Component/styles/about_styles.css";
import "./Component/styles/contact_responsive.css";
import "./Component/styles/contact_styles.css";
import "./Component/styles/listings_responsive.css";
import "./Component/styles/listings_styles.css";
import "./Component/styles/listings_single_responsive.css";
import "./Component/styles/listings_single_styles.css";
import "./Component/styles/news_responsive.css";
import "./Component/styles/news_styles.css";
import "./Component/styles/responsive.css";
import "./plugins/colorbox/colorbox.css";
import "./Component/styles/main_styles.css";
//toast
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

//toast
//Authentication
import { pause, makeRequestUrl } from "./utils.js";
import * as auth0Client from "./auth";
const makeUrl = (path, params) =>
  makeRequestUrl(`http://localhost:8080/${path}`, params);

//Authentication

class App extends Component {
  
  state = {
    token: null,
    nick: null,
    checkingSession:true,
    isLoggedIn:false,
    currentPage :'/'   
  };
  
  //authentication
  updatePage = (props) => {
    this.setState({currentPage: props}, () => console.log(this.state.currentPage))
  }
  getPersonalPageData = async () => {
    try {
      const url = makeUrl(`mypage`);
      const response = await fetch(url, {
        headers: { Authorization: `Bearer ${auth0Client.getIdToken()}` }
      });
      const answer = await response.json();
      if (answer.success) {
        const message = answer.result;
        // we should see: "received from the server: 'ok, user <username> has access to this page'"
   
        toast(`received from the server: '${message}'`);
      } else {
        this.setState({ error_message: answer.message });
        toast.error(
          `error message received from the server: ${answer.message}`
        );
      }
    } catch (err) {
      this.setState({ error_message: err.message });
      toast.error(err.message);
    }
  };
  renderProfilePage = () => {
    if(this.state.checkingSession){
      return <p>validating session...</p>
    }
  }
  renderUser() {
    
    const isLoggedIn = auth0Client.isAuthenticated();
    console.log(auth0Client.isAuthenticated())
    if (isLoggedIn) {
    
     // console.log("render user login")
      // user is logged in
      return this.renderUserLoggedIn();
      
    } else {
     
     // console.log("render user out")
      return this.renderUserLoggedOut();
     
    }
  }
  renderUserLoggedOut() {
    return <button onClick={auth0Client.signIn}>Sign In</button>;
  }
  renderUserLoggedIn() {
    const nick = auth0Client.getProfile().name;
    return (
      <div>
        Hello, {nick}! <button onClick={auth0Client.signOut}>logout</button>
      </div>
    );
  }

  isLogging = false;
  login = async () => {
    if (this.isLogging === true) {
      return;
    }
    this.isLogging = true;
    try{
      await auth0Client.handleAuthentication();
      const name = auth0Client.getProfile().name
       // get the data from Auth0
        if(name==="test@mazad.com"){
      await this.getPersonalPageData() // get the data from our server
      toast(`${name} is logged in`)
      this.props.history.push('/dashboard')}
       else{
        this.props.history.push(`${this.state.currentPage}`)
    }
    }catch(err){
      this.isLogging = false
      toast.error(err.message);
    }
  }
  handleAuthentication = ({history}) => {
    this.login(history)
    return <p>wait...</p>
  }
  handleAuthentications = ({history}) => {
    this.login(history)
    return <p>wait...</p>
  }
async componentDidMount(){
  if (this.props.location.pathname === '/callback'){
    return
  }
  try {
    await auth0Client.silentAuth();
    await this.getPersonalPageData(); // get the data from our server
    this.forceUpdate();
  } catch (err) {
    if (err.error !== 'login_required'){
      console.log(err.error);
    }
  }

}
setPage = (props)=>
{
  console.log(props)
  //this.setState({currentPage:props});
}
  //authentication
  render() {
    return (
      <Router>
        <div className="App">
          <ToastContainer />
          <Route exact path="/" component={Test} />
          <Route exact path="/sell" component={Sell} />
          <Route path="/about" render={()=> <About />} ></Route>
          <Route path="/contact" component={Contact} />
          <Route exact path="/list" component={Listing} />
          <Route path="/news" component={News} />
          <Route path="/list/item/:id" component={Single} />
          <Route exact path="/auction" render={() => <Oneline updatePage = {this.updatePage} /> } />
          <Route exact path="/auction/item/:id" component={Singleauction} />
          <Route path="/terms" component={Terms} />
          <Route path="/search" component={Search} />
          <Route path="/login" render={()=><Signin/>}></Route>
          <Route path="/callback" render={this.handleAuthentication} />
          <Route path="/callback2"  />
          <Route
            exact
            path="/dashboard"
            render={() => {
              if (auth0Client.isAuthenticated()) {
              
                if(auth0Client.getProfile().name=="test@mazad.com")
                return( <><Dash />{this.renderUser()}</>);
              }
             return <div>{this.renderUser()}</div>;
            }}
          />
        </div>
      </Router>
    );
  }
}

export default withRouter(App);
