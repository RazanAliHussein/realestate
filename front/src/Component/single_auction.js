import React, { Component } from "react";
//Components
import contactform from "../Component/smallComponent/contactform";
import Bidform from "../Component/smallComponent/bidform";
import Mainnav from "./smallComponent/mainnav";
import Signin from './LoginButton'
//Style
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
//toast
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

//toast

//fa icon
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faTwitter,
  faLinkedin,
  faGithub,
  faFacebook,
  faBehance,
  faDribbble
} from "@fortawesome/fontawesome-free-brands";
import Contactform from "../Component/smallComponent/contactform";
//Authentication
import * as auth0Client from "../auth";
import { pause, makeRequestUrl } from "../utils.js";
const makeUrl = (path, params) =>makeRequestUrl(`http://localhost:8080/${path}`, params);
//Authentication
class SingleAuction extends Component {
  state = {
    AuctionEstate_estate_id: "",
    auction_list: [],
    isTop: true,
    color: "",
    currentPrice: "",
    Users_user_id: "2",
    inauction_date: "",
    time: "",
    price: "",
    token:null,
    nickname:null
  
  };
  
  getProperty = async id => {
    try {
      const response = await fetch(`http://localhost:8080/auction/get/${id}`);
      const answer = await response.json();
      if (answer.success) {
        // add the user to the current list of contacts
        const auction = answer.result;
        const auction_list = [...this.state.auction_list, auction];
        this.setState({ auction_list });
      } else {
        this.setState({ error_message: answer.message });
      }
    } catch (err) {
      this.setState({ error_message: err.message });
    }
  };
  getCurrentPrice = async id => {
    try {
      const response = await fetch(
        `http://localhost:8080/currentprice/get/${id}`
      );
      const answer = await response.json();
      if (answer.success) {
        const currentPrice = answer.result.price;

        this.setState({ currentPrice });
      } else {
        this.setState({ error_message: answer.message });
      }
    } catch (err) {
      this.setState({ error_message: err.message });
    }
  };
  createBidding = async props => {
    const {
      AuctionEstate_estate_id,
      Users_user_id,
      inauction_date,
      time,
      price
    } = props;
    const url = makeUrl(`inauction/new?AuctionEstate_estate_id=${AuctionEstate_estate_id}&Users_user_id=${Users_user_id}&inauction_date=${inauction_date}&time=${time}&price=${price}`)
  //  const url = `http://localhost:8080/inauction/new?AuctionEstate_estate_id=${AuctionEstate_estate_id}&Users_user_id=${Users_user_id}&inauction_date=${inauction_date}&time=${time}&price=${price}`;
    try {
      if (
        !props ||
        !(
          props.AuctionEstate_estate_id &&
          props.Users_user_id &&
          props.inauction_date &&
          props.time &&
          props.price
        )
      ) {
    
        throw new Error(`you need all propertis`);
      
      }
      console.log("hereeeeeeeeee")
        const response = await fetch(url,{ headers: { Authorization: `Bearer ${auth0Client.getIdToken()}` }});
         console.log(response)
      const answer = await response.json();
      if (answer.success) {
        // we reproduce the user that was created in the database, locally
        const id = answer.result;
        window.alert("inserted");
      } else {
        this.setState({ error_message: answer.message });
        console.log("not create");
      }
     }
      catch (err) {
      this.setState({ error_message: err.message });
      console.log(err.message);
    }
  };
  timeanddate = () => {
    var that = this;
    var date = new Date().getDate(); //Current Date
    var month = new Date().getMonth() + 1; //Current Month
    var year = new Date().getFullYear(); //Current Year
    var hours = new Date().getHours(); //Current Hours
    var min = new Date().getMinutes(); //Current Minutes
    var sec = new Date().getSeconds(); //Current Seconds
    that.setState({
      //Setting the value of the date time
      inauction_date: date + "/" + month + "/" + year
    });
    that.setState({
      time: hours + "/" + min + "/" + sec
    });
  };
  async componentDidMount() {
    if (this.props.location.pathname === '/callback'){
      return
    }
    try {
      await auth0Client.silentAuth();
      await this.getPersonalPageData(); // get the data from our server
      this.forceUpdate();
    } catch (err) {
      if (err.error !== 'login_required'){
        console.log(err.error);
      }
    }
    const { id } = this.props.match.params;
    this.setState({ AuctionEstate_estate_id: id });
    this.getProperty(id);
    this.getCurrentPrice(id);
    this.timeanddate();

    document.addEventListener("scroll", () => {
      const isTop = window.scrollY < 100;
      if (isTop !== this.state.isTop) {
        this.setState({ isTop });
        this.setState({ color: "#0e1d41" });
      }
    });
  }
  Bidnow = evt => {
    evt.preventDefault();
    const {
      AuctionEstate_estate_id,
      Users_user_id,
      inauction_date,
      time,
      price
    } = this.state;
    console.log(auth0Client.isAuthenticated())
        if(auth0Client.isAuthenticated()){
        
    this.createBidding({
      AuctionEstate_estate_id,
      Users_user_id,
      inauction_date,
      time,
      price
    });
    this.setState({
      AuctionEstate_estate_id,
      Users_user_id,
      inauction_date,
      time,
      price
    });
        }
         else{
       console.log("auth")
       auth0Client.signIn()
     }
  };

  render() {
    return (
      <div className="SingleAuction ">
        <div className="super_container">
          {/* <!-- Home --> */}
          <div className="home">
            {/* <!-- Image by: https://unsplash.com/@jbriscoe --> */}
            <div
              className="home_background"
              style={{
                backgroundImage: `url(${require("./images/listings_single.jpg")})`
              }}
            />

            <div className="container">
              <div className="row">
                <div className="col">
                  <div className="home_content">
                    <div className="home_title">
                      <h2>single listings</h2>
                    </div>
                    <div className="breadcrumbs">
                      <span>
                        <Link to="/">Home</Link>
                      </span>
                      <span>
                        <Link to="/"> Search</Link>
                      </span>
                      <span>
                        <Link to="/auction"> Listings</Link>
                      </span>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

          {/* <!-- Header --> */}

          <header
            className="header trans_300"
            style={{ backgroundColor: this.state.color }}
          >
            <div className="container">
              <div className="row">
                <div className="col">
                  <div className="header_container d-flex flex-row align-items-center trans_300">
                    {/* <!-- Logo --> */}

                    <div className="logo_container">
                      <a href="#">
                        <div className="logo">
                          <img src={require("./images/logo.png")} alt="" />
                          <span>mazad</span>
                        </div>
                      </a>
                    </div>

                    {/* <!-- Main Navigation --> */}

                    <Mainnav />

                    {/* <!-- Phone Home --> */}

                    <Signin/>

                    {/* <!-- Hamburger --> */}

                    <div className="hamburger_container menu_mm">
                      <div className="hamburger menu_mm">
                        <i className="fas fa-bars trans_200 menu_mm" />
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            {/* <!-- Menu --> */}
            <ToastContainer />
            <div className="menu menu_mm">
              <ul className="menu_list">
                <li className="menu_item">
                  <div className="container">
                    <div className="row">
                      <div className="col">
                        <Link to="/">home</Link>
                      </div>
                    </div>
                  </div>
                </li>
                <li className="menu_item">
                  <div className="container">
                    <div className="row">
                      <div className="col">
                        <Link to="/about">about us</Link>
                      </div>
                    </div>
                  </div>
                </li>
                <li className="menu_item">
                  <div className="container">
                    <div className="row">
                      <div className="col">
                        <Link to="/list">listings</Link>
                      </div>
                    </div>
                  </div>
                </li>
                <li className="menu_item">
                  <div className="container">
                    <div className="row">
                      <div className="col">
                        <Link to="/news">news</Link>
                      </div>
                    </div>
                  </div>
                </li>
                <li className="menu_item">
                  <div className="container">
                    <div className="row">
                      <div className="col">
                        <Link to="/contact">contact</Link>
                      </div>
                    </div>
                  </div>
                </li>
              </ul>
            </div>
          </header>

          {/* <!-- Listing --> */}

          <div className="listing">
            {this.state.auction_list.map(auction => (
              <div key={auction.id}>
                <div className="container">
                  <div className="row">
                    <div className="col-lg-8">
                      {/* <!-- Listing Title --> */}
                      <div className="listing_title_container">
                        <div className="listing_title">
                          House in {auction.estate_location}
                        </div>
                        <p className="listing_text">
                          {auction.estate_description}
                        </p>
                        <div className="room_tags">
                          <span className="room_tag">
                            <a href="#">Hottub</a>
                          </span>
                          <span className="room_tag">
                            <a href="#">Swimming Pool</a>
                          </span>
                          <span className="room_tag">
                            <a href="#">Garden</a>
                          </span>
                          <span className="room_tag">
                            <a href="#">Patio</a>
                          </span>
                          <span className="room_tag">
                            <a href="#">Hard Wood Floor</a>
                          </span>
                        </div>
                      </div>
                    </div>

                    {/* <!-- Listing Price --> */}
                    <div className="col-lg-4 listing_price_col clearfix">
                      <div className="featured_card_box d-flex flex-row align-items-center trans_300 float-lg-right">
                        <img
                          src={require("./images/tag.svg")}
                          alt="https://www.flaticon.com/authors/lucy-g"
                        />
                        <div className="featured_card_box_content">
                          <div className="featured_card_price_title trans_300">
                            Start Bidding
                          </div>
                          <div className="featured_card_price trans_300">
                            {auction.estate_start_price}
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="row">
                    <div className="col">
                      {/* <!-- Listing Image Slider --> */}

                      <div className="listing_slider_container">
                        <div className="owl-carousel owl-theme listing_slider">
                          {/* <!-- Listing Slider Item --> */}
                          <div className="owl-item listing_slider_item">
                            <img
                              src={require("./images/listing_slider_1.jpg")}
                              alt="https://unsplash.com/@astute"
                            />
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div className="row listing_content_row">
                    {/* 				
				<!-- Search Sidebar --> */}

                    <div className="col-lg-4 sidebar_col">
                      {/* <!-- Search Box --> */}

                      <div className="search_box">
                        <div className="search_box_content">
                          {/* <!-- Search Box Title --> */}

                          {/* <!-- Search Box Title --> */}
                          <div className="search_box_title text-center">
                            <div className="search_box_title_inner">
                              <div className="search_box_title_icon d-flex flex-column align-items-center justify-content-center" />
                              <span>Go into Auction</span>
                            </div>
                          </div>

                          {/* <!-- Bidding Form --> */}
                          <form className="search_form" action="#">
                            <div className="search_box_container" />
                            <br />
                            <br />
                            <h2>Current Bid:</h2>
                            <span className="price">
                              {this.state.currentPrice != null
                                ? `${this.state.currentPrice}$`
                                : `${auction.estate_start_price}`}
                            </span>

                            <span className="bidder"> to Harg</span>
                            <div className="clear-floats">
                              <span className="icon reserve">
                                Reserve not met
                              </span>
                              <br />
                              <strong>Reserve:</strong> $
                              <input
                                onChange={evt => {
                                  this.setState({ price: evt.target.value });
                                }}
                                type="number"
                                id="tentacles"
                                name="tentacles"
                                min={this.state.currentPrice + 100}
                              />
                              <br />
                              <br />
                              <br />
                              <a
                                href="/login.cfm?r=1"
                                className="button bid d-flex flex-column align-items-center justify-content-center"
                              >
                                <button onClick={this.Bidnow}>
                                 
                                    Bid Now
                                
                                </button>
                              </a>
                            </div>
                          </form>
                        </div>
                      </div>

                      <div className="hello">
                        <div className="footer_col_title">say hello</div>
                        <div className="footer_contact_form_container">
                          <form
                            id="hello_contact_form"
                            className="footer_contact_form"
                            action="post"
                          >
                            <input
                              id="hello_contact_form_name"
                              className="input_field contact_form_name"
                              type="text"
                              placeholder="Name"
                              required="required"
                              data-error="Name is required."
                            />
                            <input
                              id="hello_contact_form_email"
                              className="input_field contact_form_email"
                              type="email"
                              placeholder="E-mail"
                              required="required"
                              data-error="Valid email is required."
                            />
                            <textarea
                              id="hello_contact_form_message"
                              className="text_field contact_form_message"
                              name="message"
                              placeholder="Message"
                              required="required"
                              data-error="Please, write us a message."
                            />
                            <button
                              id="hello_contact_send_btn"
                              type="submit"
                              className="contact_send_btn trans_200"
                              value="Submit"
                            >
                              send
                            </button>
                          </form>
                        </div>
                      </div>
                    </div>

                    {/* <!-- Listing --> */}

                    <div className="col-lg-8 listing_col">
                      <div className="listing_details">
                        <div className="listing_subtitle">Extra Facilities</div>
                        <p className="listing_details_text">
                          {auction.estate_description}
                        </p>
                        <div className="rooms">
                          <div className="room">
                            <span className="room_title">Bedrooms</span>
                            <div className="room_content">
                              <div className="room_image">
                                <img
                                  src={require("./images/bedroom.png")}
                                  alt=""
                                />
                              </div>
                              <span className="room_number">
                                {auction.estate_room}
                              </span>
                            </div>
                          </div>

                          <div className="room">
                            <span className="room_title">Bathrooms</span>
                            <div className="room_content">
                              <div className="room_image">
                                <img
                                  src={require("./images/shower.png")}
                                  alt=""
                                />
                              </div>
                              <span className="room_number">
                                {auction.estate_bathroom}
                              </span>
                            </div>
                          </div>

                          <div className="room">
                            <span className="room_title">Area</span>
                            <div className="room_content">
                              <div className="room_image">
                                <img
                                  src={require("./images/area.png")}
                                  alt=""
                                />
                              </div>
                              <span className="room_number">
                                {auction.estate_height} Sq Ft
                              </span>
                            </div>
                          </div>

                          <div className="room">
                            <span className="room_title">Patio</span>
                            <div className="room_content">
                              <div className="room_image">
                                <img
                                  src={require("./images/patio.png")}
                                  alt=""
                                />
                              </div>
                              <span className="room_number">1</span>
                            </div>
                          </div>

                          <div className="room">
                            <span className="room_title">Garage</span>
                            <div className="room_content">
                              <div className="room_image">
                                <img
                                  src={require("./images/garage.png")}
                                  alt=""
                                />
                              </div>
                              <span className="room_number">
                                {auction.estate_garage}
                              </span>
                            </div>
                          </div>
                        </div>
                      </div>

                      {/* <!-- Listing Description --> */}
                      <div className="listing_description">
                        <div className="listing_subtitle">Description</div>
                        <p className="listing_description_text">
                          {auction.estate_description}
                        </p>
                      </div>
                      {/* <div><Bidform/></div> */}
                      {/* <!-- Listing Additional Details --> */}
                      <div className="listing_additional_details">
                        <div className="listing_subtitle">
                          Additional Details
                        </div>
                        <ul className="additional_details_list">
                          <li className="additional_detail">
                            <span>bedroom features:</span> Main Floor Master
                            Bedroom, Walk-In Closet
                          </li>
                          <li className="additional_detail">
                            <span>dining area:</span> Breakfast Counter/Bar,
                            Living/Dining Combo
                          </li>
                          <li className="additional_detail">
                            <span>doors & windows:</span> Bay Window
                          </li>
                          <li className="additional_detail">
                            <span>entry location:</span> Mid Level
                          </li>
                          <li className="additional_detail">
                            <span>floors:</span> Raised Foundation, Vinyl Tile,
                            Wall-to-Wall Carpet, Wood
                          </li>
                        </ul>
                      </div>

                      {/* <!-- Listing Video --> */}
                      <div className="listing_video">
                        <div className="listing_subtitle">Property Video</div>
                        <div className="listing_video_link">
                          <a
                            className="video"
                            href="https://vimeo.com/99340873"
                            title=""
                          >
                            <img
                              src={require("./images/listing_video.jpg")}
                              alt="https://www.pexels.com/u/binyaminmellish/"
                            />
                          </a>
                          <div className="video_play">
                            <img src={require("./images/play.svg")} alt="" />
                          </div>
                        </div>
                      </div>

                      {/* <!-- Listing Map --> */}
                      <div className="listing_map">
                        <div className="listing_subtitle">Property on map</div>
                        <div id="google_map">
                          <div className="map_container">
                            <div id="map">
                              <iframe
                                src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1696164.8008068155!2d34.726213568721214!3d33.86848112590474!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x151f17028422aaad%3A0xcc7d34096c00f970!2sLebanon!5e0!3m2!1sen!2slb!4v1549527135111"
                                frameborder="0"
                                allowfullscreen
                              />
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            ))}
          </div>

          {/* <!-- Newsletter --> */}

          <div className="newsletter">
            <div className="container">
              <div className="row row-equal-height">
                <div className="col-lg-6">
                  <div className="newsletter_title">
                    <h3>subscribe to our newsletter</h3>
                    <span className="newsletter_subtitle">
                      Get the latest offers
                    </span>
                  </div>
                  <div className="newsletter_form_container">
                    <form action="#">
                      <div className="newsletter_form_content d-flex flex-row">
                        <input
                          id="newsletter_email"
                          className="newsletter_email"
                          type="email"
                          placeholder="Your email here"
                          required="required"
                          data-error="Valid email is required."
                        />
                        <button
                          id="newsletter_submit"
                          type="submit"
                          className="newsletter_submit_btn trans_200"
                          value="Submit"
                        >
                          subscribe
                        </button>
                      </div>
                    </form>
                  </div>
                </div>

                <div className="col-lg-6">
                  <a href="#">
                    <div className="weekly_offer">
                      <div className="weekly_offer_content d-flex flex-row">
                        <div className="weekly_offer_icon d-flex flex-column align-items-center justify-content-center">
                          <img src={require("./images/prize.svg")} alt="" />
                        </div>
                        <div className="weekly_offer_text text-center">
                          weekly offer
                        </div>
                      </div>
                      <div
                        className="weekly_offer_image"
                        style={{
                          backgroundImage: `url(${require("./images/weekly.jpg")})`
                        }}
                      />
                    </div>
                  </a>
                </div>
              </div>
            </div>
          </div>

          {/* <!-- Footer --> */}

          <footer className="footer">
            <div className="container">
              <div className="row">
                {/* <!-- Footer About --> */}

                <div className="col-lg-3 footer_col">
                  <div className="footer_col_title">
                    <div className="logo_container">
                      <a href="#">
                        <div className="logo">
                          <img src={require("./images/logo.png")} alt="" />
                          <span>mazad</span>
                        </div>
                      </a>
                    </div>
                  </div>
                  <div className="footer_social">
                    <ul className="footer_social_list">
                      <li className="footer_social_item">
                        <a href="#">
                          <i className="fab fa-pinterest">
                            <FontAwesomeIcon
                              className="font-awesome"
                              icon={faGithub}
                            />
                          </i>
                        </a>
                      </li>
                      <li className="footer_social_item">
                        <a href="#">
                          <i className="fab fa-facebook-f">
                            <FontAwesomeIcon
                              className="font-awesome"
                              icon={faTwitter}
                            />
                          </i>
                        </a>
                      </li>
                      <li className="footer_social_item">
                        <a href="#">
                          <i className="fab fa-twitter">
                            <FontAwesomeIcon
                              className="font-awesome"
                              icon={faTwitter}
                            />
                          </i>
                        </a>
                      </li>
                      <li className="footer_social_item">
                        <a href="#">
                          <i className="fab fa-dribbble">
                            <FontAwesomeIcon
                              className="font-awesome"
                              icon={faDribbble}
                            />
                          </i>
                        </a>
                      </li>
                      <li className="footer_social_item">
                        <a href="#">
                          <i className="fab fa-behance">
                            <FontAwesomeIcon
                              className="font-awesome"
                              icon={faBehance}
                            />
                          </i>
                        </a>
                      </li>
                    </ul>
                  </div>
                  <div className="footer_about">
                    <p>
                      Lorem ipsum dolor sit amet, cons ectetur quis ferme
                      adipiscing elit. Suspen dis se tellus eros, placerat quis
                      ferme ntum et, viverra sit amet lacus. Nam gravida quis
                      ferme semper augue.
                    </p>
                  </div>
                </div>

                {/* <!-- Footer Useful Links --> */}

                <div className="col-lg-3 footer_col">
                  <div className="footer_col_title">useful links</div>
                  <ul className="footer_useful_links">
                    <li className="useful_links_item">
                      <a href="#">Listings</a>
                    </li>
                    <li className="useful_links_item">
                      <a href="#">Favorite Cities</a>
                    </li>
                    <li className="useful_links_item">
                      <a href="#">Clients Testimonials</a>
                    </li>
                    <li className="useful_links_item">
                      <a href="#">Featured Listings</a>
                    </li>
                    <li className="useful_links_item">
                      <a href="#">Properties on Offer</a>
                    </li>
                    <li className="useful_links_item">
                      <a href="#">Services</a>
                    </li>
                    <li className="useful_links_item">
                      <a href="#">News</a>
                    </li>
                    <li className="useful_links_item">
                      <a href="#">Our Agents</a>
                    </li>
                  </ul>
                </div>

                {/* <!-- Footer Contact Form --> */}
                <div className="col-lg-3 footer_col">
                  <Contactform />
                </div>

                {/* <!-- Footer Contact Info --> */}

                <div className="col-lg-3 footer_col">
                  <div className="footer_col_title">contact info</div>
                  <ul className="contact_info_list">
                    <li className="contact_info_item d-flex flex-row">
                      <div>
                        <div className="contact_info_icon">
                          <img
                            src={require("./images/placeholder.svg")}
                            alt=""
                          />
                        </div>
                      </div>
                      <div className="contact_info_text">
                        4127 Raoul Wallenber 45b-c Gibraltar
                      </div>
                    </li>
                    <li className="contact_info_item d-flex flex-row">
                      <div>
                        <div className="contact_info_icon">
                          <img
                            src={require("./images/phone-call.svg")}
                            alt=""
                          />
                        </div>
                      </div>
                      <div className="contact_info_text">2556-808-8613</div>
                    </li>
                    <li className="contact_info_item d-flex flex-row">
                      <div>
                        <div className="contact_info_icon">
                          <img src={require("./images/message.svg")} alt="" />
                        </div>
                      </div>
                      <div className="contact_info_text">
                        <a
                          href="mailto:contactme@gmail.com?Subject=Hello"
                          target="_top"
                        >
                          mazadm009@gmail.com
                        </a>
                      </div>
                    </li>
                    <li className="contact_info_item d-flex flex-row">
                      <div>
                        <div className="contact_info_icon">
                          <img
                            src={require("./images/planet-earth.svg")}
                            alt=""
                          />
                        </div>
                      </div>
                      <div className="contact_info_text">
                        <a href="https://colorlib.com">mazadm009@gmail.com</a>
                      </div>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
          </footer>
        </div>
      </div>
    );
  }
}

export default SingleAuction;
