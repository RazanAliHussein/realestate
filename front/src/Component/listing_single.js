import React, { Component } from 'react';
//Components
import contactform from '../Component/smallComponent/contactform'
import Mainnav from './smallComponent/mainnav'
//Style
import {BrowserRouter as Router,Route,Link} from "react-router-dom";

//fa icon
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {faTwitter, faLinkedin, faGithub, faFacebook,faBehance,faDribbble} from '@fortawesome/fontawesome-free-brands';
import Contactform from '../Component/smallComponent/contactform';
class Single extends Component {
	state={
	
		estate_list:[],
		isTop: true,
    color:""
	}
	getProperty = async id => {
	
		try {
		  const response = await fetch(`http://localhost:8080/estate/get/${id}`);
		  const answer = await response.json();
		  if (answer.success) {
			// add the user to the current list of contacts
			const auction = answer.result;
			const estate_list = [...this.state.estate_list, auction];
			this.setState({ estate_list });
		  } else {
			this.setState({ error_message: answer.message });
		  }
		} catch (err) {
		  this.setState({ error_message: err.message });
		}
	  };
	componentDidMount(){
		const {id} = this.props.match.params
	
		this.getProperty(id);
		document.addEventListener('scroll', () => {
      const isTop = window.scrollY < 100;
      if (isTop !== this.state.isTop) {
          this.setState({ isTop })
          this.setState({color:"#0e1d41"})
      }
     
    });
	}
  render() {
	
	// this.setState({id:this.props.match.params})

    return (
      <div className="Single">
   <div class="super_container">
	
	{/* <!-- Home --> */}
	<div class="home">
		{/* <!-- Image by: https://unsplash.com/@jbriscoe --> */}
		<div class="home_background" style={ { backgroundImage: `url(${require('./images/listings_single.jpg')})` } }></div>
		
		<div class="container">
			<div class="row">
				<div class="col">
					<div class="home_content">
						<div class="home_title">
							<h2>single listings</h2>
						</div>
						<div class="breadcrumbs">
							<span><Link to="/">Home</Link></span>
							<span><Link to="/"> Search</Link></span>
							<span><Link to="/list"> Listings</Link></span>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	{/* <!-- Header --> */}

	<header class="header trans_300" style={{backgroundColor:this.state.color}}>

		<div class="container">
			<div class="row">
				<div class="col">
					<div class="header_container d-flex flex-row align-items-center trans_300">

						{/* <!-- Logo --> */}

						<div class="logo_container">
							<a href="#">
								<div class="logo">
									<img src={require('./images/logo.png')} alt=""/>
									<span>mazad</span>
								</div>
							</a>
						</div>
						
						{/* <!-- Main Navigation --> */}

						<Mainnav/>
						
						{/* <!-- Phone Home --> */}

						<div class="phone_home text-center">
							<span>+0080 234 567 84441</span>
						</div>
						
						{/* <!-- Hamburger --> */}

						<div class="hamburger_container menu_mm">
							<div class="hamburger menu_mm">
								<i class="fas fa-bars trans_200 menu_mm"></i>
							</div>
						</div>

					</div>
				</div>
			</div>
		</div>

		{/* <!-- Menu --> */}

		<div class="menu menu_mm">
			<ul class="menu_list">
				<li class="menu_item">
					<div class="container">
						<div class="row">
							<div class="col">
								<Link to="/">home</Link>
							</div>
						</div>
					</div>
				</li>
				<li class="menu_item">
					<div class="container">
						<div class="row">
							<div class="col">
								<Link to="/about">about us</Link>
							</div>
						</div>
					</div>
				</li>
				<li class="menu_item">
					<div class="container">
						<div class="row">
							<div class="col">
								<Link to="/list">listings</Link>
							</div>
						</div>
					</div>
				</li>
				<li class="menu_item">
					<div class="container">
						<div class="row">
							<div class="col">
								<Link to="/news">news</Link>
							</div>
						</div>
					</div>
				</li>
				<li class="menu_item">
					<div class="container">
						<div class="row">
							<div class="col">
								<Link to="/contact">contact</Link>
							</div>
						</div>
					</div>
				</li>
			</ul>
		</div>

	</header>

	{/* <!-- Listing --> */}

	<div class="listing">
	{this.state.estate_list.map(estate => (
              <div key={estate.id}>
		<div class="container">
			<div class="row">
				<div class="col-lg-8">
					
					{/* <!-- Listing Title --> */}
					<div class="listing_title_container">
						<div class="listing_title">House in {estate.estate_location}</div>
						<p class="listing_text">{estate.estate_description}</p>
						<div class="room_tags">
							<span class="room_tag"><a href="#">Hottub</a></span>
							<span class="room_tag"><a href="#">Swimming Pool</a></span>
							<span class="room_tag"><a href="#">Garden</a></span>
							<span class="room_tag"><a href="#">Patio</a></span>
							<span class="room_tag"><a href="#">Hard Wood Floor</a></span>
						</div>
					</div>
				</div>
				
				{/* <!-- Listing Price --> */}
				<div class="col-lg-4 listing_price_col clearfix">
					<div class="featured_card_box d-flex flex-row align-items-center trans_300 float-lg-right">
						<img src={require('./images/tag.svg')} alt="https://www.flaticon.com/authors/lucy-g"/>
						<div class="featured_card_box_content">
							<div class="featured_card_price_title trans_300">For Sale</div>
							<div class="featured_card_price trans_300">${estate.estate_price}</div>
						</div>
					</div>
				</div>

			</div>
			<div class="row">
				<div class="col">
					
					{/* <!-- Listing Image Slider --> */}

					<div class="listing_slider_container">
						<div class="owl-carousel owl-theme listing_slider">
							
							{/* <!-- Listing Slider Item --> */}
							<div class="owl-item listing_slider_item">
								<img src={require('./images/listing_slider_1.jpg')} alt="https://unsplash.com/@astute"/>
							</div>

							{/* <!-- Listing Slider Item --> */}
							{/* <div class="owl-item listing_slider_item">
								<img src={require('./images/listing_slider_1.jpg')} alt="https://unsplash.com/@astute"/>
							</div> */}

							{/* <!-- Listing Slider Item --> */}
							{/* <div class="owl-item listing_slider_item">
								<img src={require('./images/listing_slider_1.jpg')} alt="https://unsplash.com/@astute"/>
							</div> */}

							{/* <!-- Listing Slider Item --> */}
							{/* <div class="owl-item listing_slider_item">
								<img src={require('./images/listing_slider_1.jpg')} alt="https://unsplash.com/@astute"/>
							</div> */}

							{/* <!-- Listing Slider Item --> */}
							{/* <div class="owl-item listing_slider_item">
								<img src={require('./images/listing_slider_1.jpg')} alt="https://unsplash.com/@astute"/>
							</div>
							 */}
						</div>

						{/* <div class="listing_slider_nav listing_slider_prev d-flex flex-row align-items-center justify-content-center trans_200">
							<img src={require('./images/nav_left.png')} alt=""/>
						</div> */}

						{/* <div class="listing_slider_nav listing_slider_next d-flex flex-row align-items-center justify-content-center trans_200">
							<img src={require('./images/nav_right.png')} alt=""/>
						</div> */}

					</div>

				</div>
			</div>
					

			<div class="row listing_content_row">
{/* 				
				<!-- Search Sidebar --> */}

				<div class="col-lg-4 sidebar_col">
					{/* <!-- Search Box --> */}

					<div class="search_box">

						<div class="search_box_content">

							{/* <!-- Search Box Title --> */}
							<div class="search_box_title text-center">
								<div class="search_box_title_inner">
									<div class="search_box_title_icon d-flex flex-column align-items-center justify-content-center"><img src={require('./images/search.png')} alt=""/></div>
									<span>search your home</span>
								</div>
							</div>

							{/* <!-- Search Form --> */}
							<form class="search_form" action="#">
								<div class="search_box_container">
									<ul class="dropdown_row clearfix">
										<li class="dropdown_item">
											<div class="dropdown_item_title">Keywords</div>
											<select name="keywords" id="keywords" class="dropdown_item_select">
												<option>Any</option>
												<option>Keyword 1</option>
												<option>Keyword 2</option>
											</select>
										</li>
										<li class="dropdown_item">
											<div class="dropdown_item_title">Property ID</div>
											<select name="property_ID" id="property_ID" class="dropdown_item_select">
												<option>Any</option>
												<option>ID 1</option>
												<option>ID 2</option>
											</select>
										</li>
										<li class="dropdown_item">
											<div class="dropdown_item_title">Property Status</div>
											<select name="property_status" id="property_status" class="dropdown_item_select">
												<option>Any</option>
												<option>Status 1</option>
												<option>Status 2</option>
											</select>
										</li>
										<li class="dropdown_item">
											<div class="dropdown_item_title">Location</div>
											<select name="property_location" id="property_location" class="dropdown_item_select">
												<option>Any</option>
												<option>Location 1</option>
												<option>Location 2</option>
											</select>
										</li>
										<li class="dropdown_item">
											<div class="dropdown_item_title">Property Type</div>
											<select name="property_type" id="property_type" class="dropdown_item_select">
												<option>Any</option>
												<option>Type 1</option>
												<option>Type 2</option>
											</select>
										</li>
										<li class="dropdown_item dropdown_item_half">
											<div class="dropdown_item_title">Bedrooms no</div>
											<select name="bedrooms_no" id="bedrooms_no" class="dropdown_item_select">
												<option>Any</option>
												<option>1</option>
												<option>2</option>
											</select>
										</li>
										<li class="dropdown_item dropdown_item_half">
											<div class="dropdown_item_title">Bathrooms no</div>
											<select name="bathrooms_no" id="bathrooms_no" class="dropdown_item_select">
												<option>Any</option>
												<option>1</option>
												<option>2</option>
											</select>
										</li>
										<li class="dropdown_item dropdown_item_half">
											<div class="dropdown_item_title">Min Price</div>
											<select name="min_price" id="min_price" class="dropdown_item_select">
												<option>Any</option>
												<option>$10000</option>
												<option>$20000</option>
											</select>
										</li>
										<li class="dropdown_item dropdown_item_half">
											<div class="dropdown_item_title">Max Price</div>
											<select name="max_price" id="max_price" class="dropdown_item_select">
												<option>Any</option>
												<option>$1000000</option>
												<option>$2000000</option>
											</select>
										</li>
										<li class="dropdown_item dropdown_item_half">
											<div class="dropdown_item_title">Min Sq Ft</div>
											<select name="min_sq_ft" id="min_sq_ft" class="dropdown_item_select">
												<option>Any</option>
												<option>Any</option>
												<option>Any</option>
											</select>
										</li>
										<li class="dropdown_item dropdown_item_half">
											<div class="dropdown_item_title">Max Sq Ft</div>
											<select name="max_sq_ft" id="max_sq_ft" class="dropdown_item_select">
												<option>Any</option>
												<option>Any</option>
												<option>Any</option>
											</select>
										</li>
									</ul>
								</div>

								<div class="search_features_container">
									<div class="search_features_trigger">
										<a href="#">Specific features</a>
									</div>
									<ul class="search_features clearfix">
										<li class="search_features_item">
											<div>
												<input type="checkbox" id="search_features_1" class="search_features_cb"/>
												<label for="search_features_1">Feature 1</label>
											</div>	
										</li>
										<li class="search_features_item">
											<div>
												<input type="checkbox" id="search_features_2" class="search_features_cb"/>
												<label for="search_features_2">Feature 2</label>
											</div>
										</li>
										<li class="search_features_item">
											<div>
												<input type="checkbox" id="search_features_3" class="search_features_cb"/>
												<label for="search_features_3">Feature 3</label>
											</div>
										</li>
										<li class="search_features_item">
											<div>
												<input type="checkbox" id="search_features_4" class="search_features_cb"/>
												<label for="search_features_4">Feature 4</label>
											</div>
										</li>
										<li class="search_features_item">
											<div>
												<input type="checkbox" id="search_features_5" class="search_features_cb"/>
												<label for="search_features_5">Feature 5</label>
											</div>
										</li>
										<li class="search_features_item">
											<div>
												<input type="checkbox" id="search_features_6" class="search_features_cb"/>
												<label for="search_features_6">Feature 6</label>
											</div>
										</li>
										<li class="search_features_item">
											<div>
												<input type="checkbox" id="search_features_7" class="search_features_cb"/>
												<label for="search_features_7">Feature 7</label>
											</div>
										</li>
										<li class="search_features_item">
											<div>
												<input type="checkbox" id="search_features_8" class="search_features_cb"/>
												<label for="search_features_8">Feature 8</label>
											</div>
										</li>
										<li class="search_features_item">
											<div>
												<input type="checkbox" id="search_features_9" class="search_features_cb"/>
												<label for="search_features_9">Feature 9</label>
											</div>
										</li>
										<li class="search_features_item">
											<div>
												<input type="checkbox" id="search_features_10" class="search_features_cb"/>
												<label for="search_features_10">Feature 10</label>
											</div>
										</li>
									</ul>

									<div class="search_button">
										<input value="search" type="submit" class="search_submit_button"/>
									</div>
								</div>
							</form>
						</div>	
					</div>

					<div class="hello">
						<div class="footer_col_title">say hello</div>
						<div class="footer_contact_form_container">
							<form id="hello_contact_form" class="footer_contact_form" action="post">
								<input id="hello_contact_form_name" class="input_field contact_form_name" type="text" placeholder="Name" required="required" data-error="Name is required."/>
								<input id="hello_contact_form_email" class="input_field contact_form_email" type="email" placeholder="E-mail" required="required" data-error="Valid email is required."/>
								<textarea id="hello_contact_form_message" class="text_field contact_form_message" name="message" placeholder="Message" required="required" data-error="Please, write us a message."></textarea>
								<button id="hello_contact_send_btn" type="submit" class="contact_send_btn trans_200" value="Submit">send</button>
							</form>
						</div>
					</div>
				</div>

				{/* <!-- Listing --> */}

				<div class="col-lg-8 listing_col">

					<div class="listing_details">
						<div class="listing_subtitle">Extra Facilities</div>
						<p class="listing_details_text">{estate.estate_description}</p>
						<div class="rooms">

							<div class="room">
								<span class="room_title">Bedrooms</span>
								<div class="room_content">
									<div class="room_image"><img src={require('./images/bedroom.png')} alt=""/></div>
									<span class="room_number">{estate.estate_room}</span>
								</div>
							</div>

							<div class="room">
								<span class="room_title">Bathrooms</span>
								<div class="room_content">
									<div class="room_image"><img src={require('./images/shower.png')} alt=""/></div>
									<span class="room_number">{estate.estate_bathroom}</span>
								</div>
							</div>

							<div class="room">
								<span class="room_title">Area</span>
								<div class="room_content">
									<div class="room_image"><img src={require('./images/area.png')} alt=""/></div>
									<span class="room_number">{estate.estate_height} Sq Ft</span>
								</div>
							</div>

							<div class="room">
								<span class="room_title">Patio</span>
								<div class="room_content">
									<div class="room_image"><img src={require('./images/patio.png')} alt=""/></div>
									<span class="room_number">1</span>
								</div>
							</div>

							<div class="room">
								<span class="room_title">Garage</span>
								<div class="room_content">
									<div class="room_image"><img src={require('./images/garage.png')} alt=""/></div>
									<span class="room_number">{estate.estate_garage}</span>
								</div>
							</div>

						</div>
						
					</div>
					
					{/* <!-- Listing Description --> */}
					<div class="listing_description">
						<div class="listing_subtitle">Description</div>
						<p class="listing_description_text">{estate.estate_description}</p>
					</div>
					
					{/* <!-- Listing Additional Details --> */}
					<div class="listing_additional_details">
						<div class="listing_subtitle">Additional Details</div>
						<ul class="additional_details_list">
							<li class="additional_detail"><span>bedroom features:</span> Main Floor Master Bedroom, Walk-In Closet</li>
							<li class="additional_detail"><span>dining area:</span> Breakfast Counter/Bar, Living/Dining Combo</li>
							<li class="additional_detail"><span>doors & windows:</span> Bay Window</li>
							<li class="additional_detail"><span>entry location:</span> Mid Level</li>
							<li class="additional_detail"><span>floors:</span> Raised Foundation, Vinyl Tile, Wall-to-Wall Carpet, Wood</li>
						</ul>
					</div>
					
					{/* <!-- Listing Video --> */}
					<div class="listing_video">
						<div class="listing_subtitle">Property Video</div>
						<div class="listing_video_link">
							<a class="video" href="https://vimeo.com/99340873" title=""><img src={require('./images/listing_video.jpg')} alt="https://www.pexels.com/u/binyaminmellish/"/></a>
							<div class="video_play"><img src={require('./images/play.svg')} alt=""/></div>
						</div>
					</div>

					{/* <!-- Listing Map --> */}
					<div class="listing_map">
						<div class="listing_subtitle">Property on map</div>
						<div id="google_map">
							<div class="map_container">
								<div id="map"><iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1696164.8008068155!2d34.726213568721214!3d33.86848112590474!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x151f17028422aaad%3A0xcc7d34096c00f970!2sLebanon!5e0!3m2!1sen!2slb!4v1549527135111" frameborder="0" allowfullscreen></iframe></div>
							</div>
						</div>
					</div>

				</div>

			</div>
		</div>
		</div>
            ))}
	</div>

	{/* <!-- Newsletter --> */}

	<div class="newsletter">
		<div class="container">
			<div class="row row-equal-height">

				<div class="col-lg-6">
					<div class="newsletter_title">
						<h3>subscribe to our newsletter</h3>
						<span class="newsletter_subtitle">Get the latest offers</span>
					</div>
					<div class="newsletter_form_container">
						<form action="#">
							<div class="newsletter_form_content d-flex flex-row">
								<input id="newsletter_email" class="newsletter_email" type="email" placeholder="Your email here" required="required" data-error="Valid email is required."/>
								<button id="newsletter_submit" type="submit" class="newsletter_submit_btn trans_200" value="Submit">subscribe</button>
							</div>
						</form>
					</div>
				</div>

				<div class="col-lg-6">
					<a href="#">
						<div class="weekly_offer">
							<div class="weekly_offer_content d-flex flex-row">
								<div class="weekly_offer_icon d-flex flex-column align-items-center justify-content-center">
									<img src={require('./images/prize.svg')} alt=""/>
								</div>
								<div class="weekly_offer_text text-center">weekly offer</div>
							</div>
							<div class="weekly_offer_image" style={{backgroundImage:`url(${require("./images/weekly.jpg")})`}}></div>
						</div>
					</a>
				</div>

			</div>
		</div>
	</div>

	{/* <!-- Footer --> */}

	<footer class="footer">
		<div class="container">
			<div class="row">
				
				{/* <!-- Footer About --> */}

				<div class="col-lg-3 footer_col">
					<div class="footer_col_title">
						<div class="logo_container">
							<a href="#">
								<div class="logo">
									<img src={require('./images/logo.png')} alt=""/>
									<span>mazad</span>
								</div>
							</a>
						</div>
					</div>
					<div class="footer_social">
						<ul class="footer_social_list">
							<li class="footer_social_item"><a href="#"><i class="fab fa-pinterest"><FontAwesomeIcon className ='font-awesome' icon={faGithub} /></i></a></li>
							<li class="footer_social_item"><a href="#"><i class="fab fa-facebook-f"><FontAwesomeIcon className ='font-awesome' icon={faTwitter} /></i></a></li>
							<li class="footer_social_item"><a href="#"><i class="fab fa-twitter"><FontAwesomeIcon className ='font-awesome' icon={faTwitter} /></i></a></li>
							<li class="footer_social_item"><a href="#"><i class="fab fa-dribbble"><FontAwesomeIcon className ='font-awesome' icon={faDribbble} /></i></a></li>
							<li class="footer_social_item"><a href="#"><i class="fab fa-behance"><FontAwesomeIcon className ='font-awesome' icon={faBehance} /></i></a></li>
						</ul>
					</div>
					<div class="footer_about">
						<p>Lorem ipsum dolor sit amet, cons ectetur  quis ferme adipiscing elit. Suspen dis se tellus eros, placerat quis ferme ntum et, viverra sit amet lacus. Nam gravida  quis ferme semper augue.</p>
					</div>
				</div>
				
				{/* <!-- Footer Useful Links --> */}

				<div class="col-lg-3 footer_col">
					<div class="footer_col_title">useful links</div>
					<ul class="footer_useful_links">
						<li class="useful_links_item"><a href="#">Listings</a></li>
						<li class="useful_links_item"><a href="#">Favorite Cities</a></li>
						<li class="useful_links_item"><a href="#">Clients Testimonials</a></li>
						<li class="useful_links_item"><a href="#">Featured Listings</a></li>
						<li class="useful_links_item"><a href="#">Properties on Offer</a></li>
						<li class="useful_links_item"><a href="#">Services</a></li>
						<li class="useful_links_item"><a href="#">News</a></li>
						<li class="useful_links_item"><a href="#">Our Agents</a></li>
					</ul>
				</div>

				{/* <!-- Footer Contact Form --> */}
				<div class="col-lg-3 footer_col">
				<Contactform/>
				</div>

				{/* <!-- Footer Contact Info --> */}

				<div class="col-lg-3 footer_col">
					<div class="footer_col_title">contact info</div>
					<ul class="contact_info_list">
						<li class="contact_info_item d-flex flex-row">
							<div><div class="contact_info_icon"><img src={require('./images/placeholder.svg')} alt=""/></div></div>
							<div class="contact_info_text">4127 Raoul Wallenber 45b-c Gibraltar</div>
						</li>
						<li class="contact_info_item d-flex flex-row">
							<div><div class="contact_info_icon"><img src={require('./images/phone-call.svg')} alt=""/></div></div>
							<div class="contact_info_text">2556-808-8613</div>
						</li>
						<li class="contact_info_item d-flex flex-row">
							<div><div class="contact_info_icon"><img src={require('./images/message.svg')} alt=""/></div></div>
							<div class="contact_info_text"><a href="mailto:contactme@gmail.com?Subject=Hello" target="_top">mazadm009@gmail.com</a></div>
						</li>
						<li class="contact_info_item d-flex flex-row">
							<div><div class="contact_info_icon"><img src={require('./images/planet-earth.svg')} alt=""/></div></div>
							<div class="contact_info_text"><a href="https://colorlib.com">mazadm009@gmail.com</a></div>
						</li>
					</ul>
				</div>

			</div>
		</div>
	</footer>
      </div>
    </div>
    
      
    );
  }
}

export default Single;
