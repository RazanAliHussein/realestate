import React, { Component } from "react";
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import Mainnav from "./smallComponent/mainnav";
import "bootstrap/dist/css/bootstrap.min.css";
import { text } from "@fortawesome/fontawesome-svg-core";
import Signin from './LoginButton'
class Terms extends Component {
  state = {
	
		isTop: true,
    color: ""
  }
  createBidding = async props => {
    const {
      AuctionEstate_estate_id,
      Users_user_id,
      inauction_date,
      time,
      price
    } = props;
    const url = `http://localhost:8080/inauction/new?AuctionEstate_estate_id=${AuctionEstate_estate_id}&Users_user_id=${Users_user_id}&inauction_date=${inauction_date}&time=${time}&price=${price}`;
    try {
      if (
        !props ||
        !(
          props.AuctionEstate_estate_id &&
          props.Users_user_id &&
          props.inauction_date &&
          props.time &&
          props.price
        )
      ) {
        throw new Error(`you need all propertis`);
      }

      const response = await fetch(url);
      const answer = await response.json();
      if (answer.success) {
        // we reproduce the user that was created in the database, locally
        const id = answer.result;
        // const inauction = {AuctionEstate_estate_id,Users_user_id,inauction_date,time,price,id};
        // const blog_list = [...this.state.blog_list, inauction];
        // this.setState({ blog_list });
        window.alert("inserted");
      } else {
        this.setState({ error_message: answer.message });
        console.log("not create");
      }
    } catch (err) {
      this.setState({ error_message: err.message });
      console.log("error");
    }
  };
  Bidnow = evt => {
    evt.preventDefault();
    const {
      AuctionEstate_estate_id,
      Users_user_id,
      inauction_date,
      time,
      price
    } = this.state;
    this.createBidding({
      AuctionEstate_estate_id,
      Users_user_id,
      inauction_date,
      time,
      price
    });
    this.setState({
      AuctionEstate_estate_id,
      Users_user_id,
      inauction_date,
      time,
      price
    });
  };
 

  componentDidMount() {
    document.addEventListener("scroll", () => {
      const isTop = window.scrollY < 100;
      if (isTop !== this.state.isTop) {
        this.setState({ isTop });
        this.setState({ color: "#0e1d41" });
      }
    });
  }
  render() {
		
    return (
      <div className="Terms">
        <div
          class="home_background"
          style={{ backgroundImage: `url(${require("./images/news.jpg")})` }}
        />

        <div class="container">
          <div class="row">
            <div class="col">
              <div class="home_content" />
            </div>
          </div>
        </div>

        <header
          class="header trans_300"
          style={{ backgroundColor: this.state.color }}
        >
          <div class="container">
            <div class="row">
              <div class="col">
                <div class="header_container d-flex flex-row align-items-center trans_300">
                  {/* <!-- Logo --> */}

                  <div class="logo_container">
                    <a href="#">
                      <div class="logo">
                        <img src={require("./images/logo.png")} alt="" />
                        <span>mazad</span>
                      </div>
                    </a>
                  </div>

                  {/* <!-- Main Navigation --> */}
                  <Mainnav />

                  {/* <!-- Phone Home --> */}

                  <Signin/>
                  {/* <!-- Hamburger --> */}

                  <div class="hamburger_container menu_mm">
                    <div class="hamburger menu_mm">
                      <i class="fas fa-bars trans_200 menu_mm" />
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

          {/* <!-- Menu --> */}

          <div class="menu menu_mm">
            <ul class="menu_list">
              <li class="menu_item">
                <div class="container">
                  <div class="row">
                    <div class="col">
                      <Link to="/">home</Link>
                    </div>
                  </div>
                </div>
              </li>
              <li class="menu_item">
                <div class="container">
                  <div class="row">
                    <div class="col">
                      <Link to="/">>about us</Link>
                    </div>
                  </div>
                </div>
              </li>
              <li class="menu_item">
                <div class="container">
                  <div class="row">
                    <div class="col">
                      <Link to="/list">>listings</Link>
                    </div>
                  </div>
                </div>
              </li>
              <li class="menu_item">
                <div class="container">
                  <div class="row">
                    <div class="col">
                      <Link to="/news">news</Link>
                    </div>
                  </div>
                </div>
              </li>
              <li class="menu_item">
                <div class="container">
                  <div class="row">
                    <div class="col">
                      <Link to="/contact">>contact</Link>
                    </div>
                  </div>
                </div>
              </li>
            </ul>
          </div>
        </header>
        <div className="textTerms" style={{ backgroundColor: "white" }}>
          <h1>Bidder Terms</h1>

          <p>
            RealtyBid International, LLC, ("RBI") has agreements with various
            individuals and corporations, etc. ("Sellers") to market and
            advertise certain properties and/or financial instruments
            ("Property" or "Properties") through its online auction website,
            RealtyBid.com. All properties offered on RBI's website shall be
            offered subject to the Sellers' approval of the price and final
            terms and conditions. The Properties shall be offered through an
            Internet auction process with a specified opening and closing date.{" "}
          </p>
          <p align="center">
            <strong>
              NOTE: If you are a Real Estate Agent wanting to bid for yourself
              or a customer, please{" "}
              <a href="/agent/agentterms.cfm">click here to register</a>.
            </strong>
          </p>
          <h3>&nbsp;</h3>
          <h3>All USERS MUST AGREE TO THE BIDDER TERMS BEFORE BIDDING.</h3>

          <h4>Site Usage</h4>

          <p>
            The first step to using this site effectively is Registration.
            Registration is currently free and is necessary to unlock many of
            the site's more useful features; please review{" "}
            <a href="/subdata/how_to_bid_individual.cfm" class="orangelink">
              How to Bid
            </a>{" "}
            and{" "}
            <a href="/subdata/sitefeatures.cfm" class="orangelink">
              {" "}
              Helpful Site Features
            </a>
            . <strong>BidAssist</strong>, as explained in the Helpful Site
            Features, is "highly" recommended to help you at the conclusion of
            bidding. This feature allows the computer to bid for you up to your
            maximum bid. It is protection against a last minute bid preventing
            you from winning a property. Also, because different computers have
            different connection speeds, waiting until the last 5 or 10 seconds
            to place your bid is not recommended. The countdown clock on
            RealtyBid.com should be considered accurate but not exact. Many
            processes can affect what actual time a user sees on their screen
            such as computer processor speeds, internet connection speeds, etc.,
            so please do not wait until the last few minutes of an auction to
            place a bid. It is highly recommended that if you intend to place a
            bid, you should do so early to get a feel for the bidding process,
            and you should also use BidAssist, a feature designed to allow the
            computer to bid for you at the end so you are assured of getting
            your bids placed. If you choose to wait until the last few minutes
            or seconds of the countdown clock to place your bid and your system
            lags or your bid does not get placed, RealtyBid bears no
            responsibility for re-offering the property or causing your bid to
            be placed for you. If it is determined that there was a system issue
            that may have prevented a bid from being placed, RealtyBid reserves
            the right to review all bids and possibly re-open the bidding on a
            property. This determination will be made on a case by case basis
            and is at the sole discretion of the company. The intent of
            RealtyBid.com is to always provide a fair, level and transparent
            bidding environment for our consumer audience.
          </p>

          <h4>Property View &amp; Property Information Packages</h4>

          <p>
            Prospective bidders and interested parties may view general
            information about a property just by clicking the links to the
            property's Bid Page. Additional property information may also be
            available by clicking additional links located on a particular
            property's Bid Page.
          </p>

          <h4>Property Bidding</h4>

          <p>
            If you elect to bid on a property, you must be registered to do so;
            however, you do not have to be registered in order to view bids for
            a property. All bids are open and displayed for public viewing
            unless otherwise noted. You can bid on properties either from a
            property's Bid Page or from your{" "}
            <a href="/subdata/sitefeatures.cfm">My Properties</a> listing page,
            which can display multiple properties for a bidder to monitor or
            place bids on. The My Properties area only has abbreviated
            information about each property but is a great tool if multiple
            properties are being monitored.
          </p>

          <h4>Terms of Use</h4>

          <p>
            Each site user shall act with honesty and integrity while on the
            site; otherwise, user privileges shall be revoked. Each bidder shall
            review and adhere to the Terms of Sale for each individual property
            listed on the site. RealtyBid.com may suspend your account and seek
            legal remedies if the site's staff at any time suspects that either
            as a buyer or seller, you have committed or engaged in fraud or
            fraudulent activities relating to transactions on the site.{" "}
            <strong>
              {" "}
              Any bidder making fraudulent bids on this site or defaulting on
              properties won shall be banned from the site indefinitely without
              any recourse from the bidder.
            </strong>
          </p>

          <h4>User Eligibility</h4>

          <p>
            RBI's services are available only to individuals who can form
            legally binding contracts under applicable law. RBI's services are
            not available to minors or to previously suspended RBI users. If you
            register as a business entity, by placing a bid, you represent that
            you have the authority to bind the entity to this Agreement and
            other real estate related agreements.
          </p>
          <h4>RealtyBid Fairness Doctrine</h4>
          <p>
            RealtyBid prides itself with offering properties on a fair and level
            playing field allowing bidders an equal and fair opportunity to
            compete.&nbsp; If it is determined by RealtyBid that a property is
            won unfairly, for any reason, RealtyBid reserves the right to 1)
            re-offer the property in a new round of bidding, 2) request a
            highest and best offer among the bidders or 3) return the property
            to the Seller and not sell it to any bidder. &nbsp;
          </p>
          <h4>Email Notifications &amp; Bidding Requirements</h4>

          <p>
            <strong>Property Notifications</strong>: To register to receive
            "email notifications" about properties in your specified areas of
            interest, you must fill out a simple email registration form by
            clicking the{" "}
            <a href="/bidder/emailnotification.cfm">Property Notifications</a>{" "}
            link.
            <br />
            <br />
            <strong>Bidder Registration</strong>: Bidding on this site is a very
            simple but sophisticated process. If you intend to bid on a
            property, in most cases, registration is all that is required. In
            some cases, you will need to follow the additional Specific Bid
            Instructions to bid on that property. Bidder information and
            password information is confidential and may not be shared or
            transferred to another party.
            <strong>
              <br />
              <br />
              If you intend to bid on a property, it is highly recommended that
              you register at least 72 hours prior to the close of the bidding,
              just as a precaution against any potential registration problems.
            </strong>
          </p>

          <p>
            There are no fees required to bid; however, you will need a valid
            credit card in order to register and bid. If you win a property and
            default on the contract or do not return the contract, your credit
            card will be charged a default penalty fee of at least $500, and you
            may be banned from future use of the site.
          </p>

          <h4>Fees &amp; Services</h4>

          <p>
            Currently, registration for all RBI websites is free; however, we
            reserve the right to change our Site Terms at any time relating to
            fees and other items mentioned or not mentioned in this published
            document. Changes to the policy are effective after we have posted
            the new Site Terms for at least five (5) consecutive days. We may in
            our sole discretion change any or all of our services at any time.
            If a change occurs in a particular service, the fees for that
            service are effective at the introduction of the service. Each
            Property will be subject to an Internet Transaction Fee (a.k.a.
            Buyer's Premium) added to it as defined in the description box for
            each property, to be paid by the BUYER at the closing. The bidding
            will not reflect the buyer's premium, but the Purchase Agreement
            shall include the buyer's premium through a RealtyBid Addendum,
            which shall be attached to the Purchase Agreement and signed by both
            Buyer and Seller.
          </p>

          <h4>Discrepancies in Advertisements</h4>

          <p>
            If there are any discrepancies between the terms and conditions
            outlined on this site and the terms outlined in the Purchase
            Agreement, the terms in the Purchase Agreement shall prevail, so
            read the Purchase Agreement carefully. All dates and times listed on
            this site supercede any advertisements or printed statements.
          </p>

          <h4>Bid Presentation to the Seller</h4>
          <p>
            If a property meets or exceeds it reserve price during the Online
            Bidding process, it will be coded as Pending until the buyer
            executed Purchase Agreement and a deposit check is received from the
            Bidder. At that point, the bid will be presented to the Seller and
            if it is accepted in their system, and the Purchase Agreement is
            fully executed, it will be coded as Sold and the closing process
            will begin. While the property is coded as Pending, and up until the
            property is accepted in the Seller’s internal system, additional
            higher bids may be presented.{" "}
          </p>
          <p>
            <strong>Pre-Sale Bids:</strong> If a Pre-Sale period is allowed, the
            property will be available on a "first come, first served" basis.
            Only bids entered that meet or exceed the Seller's undisclosed price
            range will be allowed by our system. When a Pre-Sale Bid is
            received, the property is coded Pre-Sale Pending in our system. Bids
            will be under RealtyBid.com internal review, a process that may take
            up to 3 business days. Bidders will be notified via email with the
            results as soon as this process has concluded. At that point, the
            paperwork process will begin for that bidder; however, that bid will
            not be presented to the Seller until the buyer-signed Purchase
            Agreement and earnest money are returned to RealtyBid.com.{" "}
            <em>
              <u>
                Until the bid is accepted and coded as Sold in the Seller’s
                Internal Tracking System, additional bids may be placed by other
                bidders.
              </u>
            </em>{" "}
            If multiple bids are received, the Seller reserves the right to call
            for a Highest and Best Bid Request among the parties. When this
            occurs, competing bidders will have typically one day to submit
            their highest bid. More than one Highest and Best Bid Request may
            occur should other bids be placed on RealtyBid.com before a bid is
            accepted and coded as Sold in the Seller’s Internal Tracking System.{" "}
            <b>
              For more details on the Pre-Sale Bid Process,{" "}
              <a href="../../subdata/prebidding_overview.cfm">click here</a>.
            </b>
          </p>
          <p>
            <strong>Note:</strong> Bid History for a Pre-Sale property is not
            disclosed to other bidders and is NOT available by calling or
            emailing RealtyBid.com at any time. No bidder shall receive
            preferential treatment over another bidder. No bidder will receive
            any type of first right of refusal on any property. The Seller will
            decide which conforming bid to accept in their sole and absolute
            discretion.
          </p>
          <p>
            <strong>Post Bids:</strong> If a property does not meet its reserve
            price during the PreSale period, or the Online Bidding Process, it
            will move to a Post Bidding status, which means that additional bids
            can be submitted up until an acceptable bid is achieved and accepted
            in the Seller’s internal system.
          </p>
          <p>
            <strong>Highest and Best Offer Process:</strong> If more than one
            bid is submitted in what is considered an acceptable price range by
            RealtyBid, a highest and best offer round may be held to make sure
            each bidder received a fair opportunity to place their highest
            possible bid. This process may be utilized even after a property is
            coded as Pending. The highest and best offer process will be
            utilized at Seller and RealtyBid’s sole discretion.
          </p>

          <p>
            <strong>Notice to All Bidders:</strong> RealtyBid is charged by its
            Sellers with selling properties in a time efficient manner at the
            highest possible price. In all cases, it operates its business
            process with those two ideas in mind.{" "}
          </p>
          <h4>Negotiations &amp; Post Event Activities</h4>
          <p>
            Once an online bidding event concludes, neither the Seller nor RBI
            is under any obligation to negotiate with any bidder. All properties
            will be sold on a "first come, first served" basis by the Seller.
          </p>

          <h4>Final Approval by Seller</h4>

          <p>
            Each Seller on this site reserves the right to have final approval
            of any and all Purchase Contracts submitted by any broker or bidder,
            regardless of information provided by this site, any broker, or any
            RealtyBid.com employee or affiliate to the contrary.
          </p>

          <h4>Online Transaction Venue</h4>

          <p>
            RBI provides an online real estate transaction venue that allows for
            the bidding and transfer of properties via a public forum. RBI does
            not hold itself out to be a real estate broker or agent, nor does it
            claim to act on behalf of buyers or sellers, even though it does
            hold real estate and auctioneer licenses in numerous states. RBI
            does claim to provide an efficient and sophisticated platform that
            allows sellers to advertise their properties to willing and able
            buyers. All Listing Brokers identified with a property on this site
            are agents of the Seller, not the Buyer. If a disruption of Internet
            bidding service (such as a power outage or other technical
            occurrence of any kind) occurs at the end of an event, RBI reserves
            the right to extend that particular event for a reasonable period of
            time as decided by RBI in order to provide bidders a fair chance to
            compete for the purchase of a property.
          </p>

          <h3>All USERS MUST AGREE TO THESE BIDDER TERMS BEFORE BIDDING.</h3>
          <form 
            name="Bidder_terms"
           
				
          >
            <center>
              {" "}
              <p>
                <input
                  type="submit"
                  name="Submit"
                  className="termsbutton"
								
									onSubmit={this.Bidnow}
                />
                <Link to="/">
                  {" "}
                  <input
                    type="Button"
                    name="I Disagree"
                    className="termsbutton"
                    value="I Disagree"
                  />{" "}
                </Link>
              </p>
            </center>
          </form>
        </div>
      </div>
    );
  }
}

export default Terms;
