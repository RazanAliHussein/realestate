import React, { Component } from "react";
//toast
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
//toast
//Authentication
import * as auth0Client from "../auth";
import { makeRequestUrl } from "../utils.js";
const makeUrl = (path, params) =>makeRequestUrl(`http://localhost:8080/${path}`, params);
//Authentication

class LoginButton extends Component {

  state={
    toggle:"sign in",
     token: null,
    nick: null,
    checkingSession:true,
    isLoggedIn:false
  }
  //authentication
  getPersonalPageDatas = async () => {
    try {
      const url = makeUrl(`mypage`);
      const response = await fetch(url, {
        headers: { Authorization: `Bearer ${auth0Client.getIdToken()}` }
      });
      const answer = await response.json();
      if (answer.success) {
        const message = answer.result;
        // we should see: "received from the server: 'ok, user <username> has access to this page'"
   
        toast(`received from the server: '${message}'`);
      } else {
        this.setState({ error_message: answer.message });
        toast.error(
          `error message received from the server: ${answer.message}`
        );
      }
    } catch (err) {
      this.setState({ error_message: err.message });
      toast.error(err.message);
    }
  };
  renderProfilePages = () => {
    if(this.state.checkingSession){
      return <p>validating session...</p>
    }
  }
  renderUsers() {
    
    const isLoggedIn = auth0Client.isAuthenticated();
    console.log(auth0Client.isAuthenticated())
    if (isLoggedIn) {
    
     // console.log("render user login")
      // user is logged in
      return this.renderUserLoggedIns();
      
    } else {
     
     // console.log("render user out")
      return this.renderUserLoggedOuts();
     
    }
  }
  renderUserLoggedOuts() {
    return <button onClick={auth0Client.signIn}>Sign In</button>;
  }
  renderUserLoggedIns() {
    const nick = auth0Client.getProfile().name;
    return (
      <div>
        Hello, {nick}! <button onClick={auth0Client.signOut}>logout</button>
      </div>
    );
  }

  isLogging = false;
  logins = async () => {
    if (this.isLogging === true) {
      return;
    }
    this.isLogging = true;
    try{
      await auth0Client.handleAuthentication();
      const name = auth0Client.getProfile().name
       // get the data from Auth0
       
      await this.getPersonalPageDatas() // get the data from our server
      toast(`${name} is logged in`)
     
       
    }catch(err){
      this.isLogging = false
      toast.error(err.message);
    }
  }
  handleAuthentications = ({history}) => {
    this.login(history)
    return <p>wait...</p>
  }
test=()=>{
 if(auth0Client.isAuthenticated()){
 {auth0Client.signOut()}
 }
 else{
  {auth0Client.signIn()}
 }
}
async componentDidMount(){
  // if (pathname === '/callback2'){
  //   return
  // }
  try {
    await auth0Client.silentAuth();
    await this.getPersonalPageDatas(); // get the data from our server
    this.forceUpdate();
  } catch (err) {
    if (err.error !== 'login_required'){
      console.log(err.error);
    }
  }

}
  //authentication
  render() {
    return (
      <div className="phone_home text-center">
        <span onClick={this.test}>Sign IN</span>
      </div>
    );
  }
}
export default LoginButton;
