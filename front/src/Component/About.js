import React, { Component } from "react";
//component
import contactform from '../Component/smallComponent/contactform'
import Mainnav from './smallComponent/mainnav'
import Signin from './LoginButton'
//Router
import {BrowserRouter as Router,Route,Link} from "react-router-dom";
//fa icon
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {faTwitter, faLinkedin, faGithub, faFacebook,faBehance,faDribbble} from '@fortawesome/fontawesome-free-brands';
import Contactform from "../Component/smallComponent/contactform";
class About extends Component {
  state={
		isTop: true,
    color:""
	}
	componentDidMount() {
    document.addEventListener('scroll', () => {
      const isTop = window.scrollY < 100;
      if (isTop !== this.state.isTop) {
          this.setState({ isTop })
          this.setState({color:"#0e1d41"})
      }
     
    });
 
  }
  render() {
    return (
      <div className="About">
        <div className="super_container">
          {/* Home  */}
          <div className="home">
            {/* Image by: https://unsplash.com/@jbriscoe */}
            <div
              className="home_background"
              style={ { backgroundImage: `url(${require('./images/home_background.jpg')})` } }
              // style="background-image:url(images/home_background.jpg)"
            />

            <div className="container">
              <div className="row">
                <div className="col">
                  <div className="home_content">
                    <div className="home_title">
                      <h2>about us</h2>
                    </div>
                    <div className="breadcrumbs">
                      <span>
                        <Link to="/">Home</Link>
                        
                      </span>
                      <span>
                      <Link to="/about">About Us</Link>
                      </span>
                      <span>
                      <Link to="/about">Our Agents</Link>
                      </span>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

          {/* Header  */}

          <header className="header trans_300" style={{backgroundColor:this.state.color}}>
            <div className="container">
              <div className="row">
                <div className="col">
                  <div className="header_container d-flex flex-row align-items-center trans_300">
                    {/* Logo  */}

                    <div className="logo_container">
                      <a href="#">
                        <div className="logo">
                          <img src={require("./images/logo.png")} alt="" />
                          <span>mazad</span>
                        </div>
                      </a>
                    </div>

                    {/* Main Navigation */}

                    <Mainnav/>

                    {/* Phone Home  */}

                   <Signin currentPage={'/about'}/>

                    {/* Hamburger  */}

                    <div className="hamburger_container menu_mm">
                      <div className="hamburger menu_mm">
                        <i className="fas fa-bars trans_200 menu_mm" />
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            {/* Menu  */}

            <div className="menu menu_mm">
              <ul className="menu_list">
                <li className="menu_item">
                  <div className="container">
                    <div className="row">
                      <div className="col">
                        <a href="index.html">home</a>
                      </div>
                    </div>
                  </div>
                </li>
                <li className="menu_item">
                  <div className="container">
                    <div className="row">
                      <div className="col">
                        <a href="#">about us</a>
                      </div>
                    </div>
                  </div>
                </li>
                <li className="menu_item">
                  <div className="container">
                    <div className="row">
                      <div className="col">
                        <a href="listings.html">listings</a>
                      </div>
                    </div>
                  </div>
                </li>
                <li className="menu_item">
                  <div className="container">
                    <div className="row">
                      <div className="col">
                        <a href="news.html">news</a>
                      </div>
                    </div>
                  </div>
                </li>
                <li className="menu_item">
                  <div className="container">
                    <div className="row">
                      <div className="col">
                        <a href="contact.html">contact</a>
                      </div>
                    </div>
                  </div>
                </li>
              </ul>
            </div>
          </header>

          {/* Intro  */}

          <div className="intro">
            <div className="container">
              <div className="row">
                <div className="col-lg-7 order-lg-1 order-2">
                  <h2 className="intro_title">The Estate Promise</h2>
                  <div className="intro_subtitle">
                    Sed vestibulum lectus ut leo molestie, id suscipit magna
                  </div>
                  <p className="intro_text">
                    Donec ullamcorper nulla non metus auctor fringi lla.
                    Curabitur blandit tempus porttitor. Lorem ipsum dolor sit
                    amet, consectetur adipiscing elit. Suspendisse tellus eros,
                    placerat quis fermentum et, viverra sit amet lacus. Nam
                    gravida semper augue id sagittis. Cras nec arcu quis velit
                    tempor porttitor sit amet vel risus. Sed vestibulum lectus
                    ut leo molestie, id suscipit magna mattis. Vivamus nisl
                    ligula, varius congue dui sit amet, vestibulum sollicitudin
                    mauris. Vestibulum quis ligula in nunc varius maximus ac et
                    nunc. Nulla sed magna turpis.
                  </p>
                  <div className="button intro_button trans_200">
                    <a className="trans_200" href="#">
                      read more
                    </a>
                  </div>
                </div>
                <div className="col-lg-5 order-lg-2 order-1">
                  <div className="intro_image">
                  
                    <img src={require('./images/intro.png')} alt="" />
                  </div>
                </div>
              </div>
            </div>
          </div>

          {/* Milestones  */}

          <div className="milestones">
            <div
              className="milestones_background parallax-window"
              data-parallax="scroll"
              data-image-src="images/milestones.jpg"
            />
            <div className="container">
              <div className="row">
                {/* Milestone  */}
                <div className="col-lg-2 milestone_col">
                  <div className="milestone text-center d-flex flex-column align-items-center justify-content-start">
                    <div className="milestone_icon d-flex flex-column justify-content-end">
                      <img src={require("./images/milestone_1.svg")} alt="" />
                    </div>
                    <div className="milestone_counter" data-end-value="310">
                      0
                    </div>
                    <div className="milestone_text">houses sold</div>
                  </div>
                </div>

                {/* Milestone */}
                <div className="col-lg-2 milestone_col">
                  <div className="milestone text-center d-flex flex-column align-items-center justify-content-start">
                    <div className="milestone_icon d-flex flex-column justify-content-end">
                      <img src={require("./images/milestone_2.svg")} alt="" />
                    </div>
                    <div className="milestone_counter" data-end-value="129">
                      0
                    </div>
                    <div className="milestone_text">clients</div>
                  </div>
                </div>

                {/* Milestone  */}
                <div className="col-lg-2 milestone_col">
                  <div className="milestone text-center d-flex flex-column align-items-center justify-content-start">
                    <div className="milestone_icon d-flex flex-column justify-content-end">
                      <img src={require("./images/milestone_3.svg")} alt="" />
                    </div>
                    <div className="milestone_counter" data-end-value="14">
                      0
                    </div>
                    <div className="milestone_text">agents</div>
                  </div>
                </div>

                {/* Milestone  */}
                <div className="col-lg-2 milestone_col">
                  <div className="milestone text-center d-flex flex-column align-items-center justify-content-start">
                    <div className="milestone_icon d-flex flex-column justify-content-end">
                      <img src={require("./images/milestone_4.svg")} alt="" />
                    </div>
                    <div className="milestone_counter" data-end-value="521">
                      0
                    </div>
                    <div className="milestone_text">rents</div>
                  </div>
                </div>

                {/* //  Milestone  */}
                <div className="col-lg-2 milestone_col">
                  <div className="milestone text-center d-flex flex-column align-items-center justify-content-start">
                    <div className="milestone_icon d-flex flex-column justify-content-end">
                      <img src={require("./images/milestone_5.svg")} alt="" />
                    </div>
                    <div className="milestone_counter" data-end-value="1107">
                      0
                    </div>
                    <div className="milestone_text">contracts</div>
                  </div>
                </div>

                {/* Milestone  */}
                <div className="col-lg-2 milestone_col">
                  <div className="milestone text-center d-flex flex-column align-items-center justify-content-start">
                    <div className="milestone_icon d-flex flex-column justify-content-end">
                      <img src={require("./images/milestone_6.svg")} alt="" />
                    </div>
                    <div className="milestone_counter" data-end-value="39">
                      0
                    </div>
                    <div className="milestone_text">investments</div>
                  </div>
                </div>
              </div>
            </div>
          </div>

          {/* Agents  */}

          <div className="agents">
            <div className="container">
              <div className="row">
                <div className="col">
                  <div className="section_title text-center">
                    <h3>out agents</h3>
                    <span className="section_subtitle">The best out there</span>
                  </div>
                </div>
              </div>

              <div className="row agents_row">
                {/*  Agent */}
                <div className="col-lg-3 agent_col text-center">
                  <div className="agent_image mx-auto">
                    <img
                      src={require("./images/agent_1.jpg")}
                      alt="image by Andrew Robles"
                    />
                  </div>
                  <div className="agent_content">
                    <div className="agent_name">michael williams</div>
                    <div className="agent_role">Real Estate Agent</div>
                    <div className="agent_social">
                      <ul className="agent_social_list">
                        <li className="agent_social_item">
                          <a href="#">
                            <i className="fab fa-pinterest" ><FontAwesomeIcon className ='font-awesome' icon={faGithub} /></i>
                          </a>
                        </li>
                        <li className="agent_social_item">
                          <a href="#">
                            <i className="fab fa-facebook-f" ><FontAwesomeIcon className ='font-awesome' icon={faFacebook} /></i>
                          </a>
                        </li>
                        <li className="agent_social_item">
                          <a href="#">
                            <i className="fab fa-twitter" ><FontAwesomeIcon className ='font-awesome' icon={faTwitter} /></i>
                          </a>
                        </li>
                      </ul>
                    </div>
                  </div>
                </div>

                {/*  Agent */}
                <div className="col-lg-3 agent_col text-center">
                  <div className="agent_image mx-auto">
                    <img
                      src={require("./images/agent_2.jpg")}
                      alt="https://unsplash.com/@gabrielsilverio"
                    />
                  </div>
                  <div className="agent_content">
                    <div className="agent_name">michael williams</div>
                    <div className="agent_role">Real Estate Agent</div>
                    <div className="agent_social">
                      <ul className="agent_social_list">
                        <li className="agent_social_item">
                          <a href="#">
                            <i className="fab fa-pinterest" ><FontAwesomeIcon className ='font-awesome' icon={faGithub} /></i>
                          </a>
                        </li>
                        <li className="agent_social_item">
                          <a href="#">
                            <i className="fab fa-facebook-f" ><FontAwesomeIcon className ='font-awesome' icon={faFacebook} /></i>
                          </a>
                        </li>
                        <li className="agent_social_item">
                          <a href="#">
                            <i className="fab fa-twitter" ><FontAwesomeIcon className ='font-awesome' icon={faTwitter} /></i>
                          </a>
                        </li>
                      </ul>
                    </div>
                  </div>
                </div>

                {/*  Agent */}
                <div className="col-lg-3 agent_col text-center">
                  <div className="agent_image mx-auto">
                    <img
                      src={require("./images/agent_3.jpg")}
                      alt="https://unsplash.com/@mehdizadeh"
                    />
                  </div>
                  <div className="agent_content">
                    <div className="agent_name">michael williams</div>
                    <div className="agent_role">Real Estate Agent</div>
                    <div className="agent_social">
                      <ul className="agent_social_list">
                        <li className="agent_social_item">
                          <a href="#">

                            <i className="fab fa-pinterest" ><FontAwesomeIcon className ='font-awesome' icon={faBehance} /></i>
                          </a>
                        </li>
                        <li className="agent_social_item">
                          <a href="#">
                            <i className="fab fa-facebook-f" ><FontAwesomeIcon className ='font-awesome' icon={faFacebook} /></i>
                          </a>
                        </li>
                        <li className="agent_social_item">
                          <a href="#">
                            <i className="fab fa-twitter" ><FontAwesomeIcon className ='font-awesome' icon={faTwitter} /></i>
                          </a>
                        </li>
                      </ul>
                    </div>
                  </div>
                </div>

                {/*  Agent */}
                <div className="col-lg-3 agent_col text-center">
                  <div className="agent_image mx-auto">
                    <img
                      src={require("./images/agent_4.jpg")}
                      alt="https://unsplash.com/@michaeldam"
                    />
                  </div>
                  <div className="agent_content">
                    <div className="agent_name">michael williams</div>
                    <div className="agent_role">Real Estate Agent</div>
                    <div className="agent_social">
                      <ul className="agent_social_list">
                        <li className="agent_social_item">
                          <a href="#">
                            <i className="fab fa-pinterest" ><FontAwesomeIcon className ='font-awesome' icon={faTwitter} /></i>
                          </a>
                        </li>
                        <li className="agent_social_item">
                          <a href="#">
                            <i className="fab fa-facebook-f" ><FontAwesomeIcon className ='font-awesome' icon={faFacebook} /></i>
                          </a>
                        </li>
                        <li className="agent_social_item">
                          <a href="#">
                            <i className="fab fa-twitter" ><FontAwesomeIcon className ='font-awesome' icon={faGithub} /></i>
                          </a>
                        </li>
                      </ul>
                    </div>
                  </div>
                </div>
              </div>

              <div className="row">
                <div className="col-lg-12 text-center">
                  <div className="agents_more">
                    <div className="button agents_more_button trans_200">
                      <a className="trans_200" href="#">
                        read more
                      </a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

          {/* Newsletter  */}

          <div className="newsletter">
            <div className="container">
              <div className="row row-equal-height">
                <div className="col-lg-6">
                  <div className="newsletter_title">
                    <h3>subscribe to our newsletter</h3>
                    <span className="newsletter_subtitle">
                      Get the latest offers
                    </span>
                  </div>
                  <div className="newsletter_form_container">
                    <form action="#">
                      <div className="newsletter_form_content d-flex flex-row">
                        <input
                          id="newsletter_email"
                          className="newsletter_email"
                          type="email"
                          placeholder="Your email here"
                          required="required"
                          data-error="Valid email is required."
                        />
                        <button
                          id="newsletter_submit"
                          type="submit"
                          className="newsletter_submit_btn trans_200"
                          value="Submit"
                        >
                          subscribe
                        </button>
                      </div>
                    </form>
                  </div>
                </div>

                <div className="col-lg-6">
                  <a href="#">
                    <div className="weekly_offer">
                      <div className="weekly_offer_content d-flex flex-row">
                        <div className="weekly_offer_icon d-flex flex-column align-items-center justify-content-center">
                          <img src={require("./images/prize.svg")} alt="" />
                        </div>
                        <div className="weekly_offer_text text-center">
                          weekly offer
                        </div>
                      </div>
                      <div
                        className="weekly_offer_image"
                        style={ { backgroundImage: `url(${require('./images/weekly.jpg')})` } }
                        //style="background-image:url(images/weekly.jpg)"
                      />
                    </div>
                  </a>
                </div>
              </div>
            </div>
          </div>

          {/* Footer  */}

          <footer className="footer">
            <div className="container">
              <div className="row">
                {/* Footer About  */}

                <div className="col-lg-3 footer_col">
                  <div className="footer_col_title">
                    <div className="logo_container">
                      <a href="#">
                        <div className="logo">
                          <img src={require("./images/logo.png")} alt="" />
                          <span>mazad</span>
                        </div>
                      </a>
                    </div>
                  </div>
                  <div className="footer_social">
                    <ul className="footer_social_list">
                      <li className="footer_social_item">
                        <a href="#">
                          <i className="fab fa-pinterest" ><FontAwesomeIcon className ='font-awesome' icon={faBehance} /></i>
                        </a>
                      </li>
                      <li className="footer_social_item">
                        <a href="#">
                          <i className="fab fa-facebook-f" ><FontAwesomeIcon className ='font-awesome' icon={faGithub} /></i>
                        </a>
                      </li>
                      <li className="footer_social_item">
                        <a href="#">
                          <i className="fab fa-twitter" ><FontAwesomeIcon className ='font-awesome' icon={faTwitter} /></i>
                        </a>
                      </li>
                      <li className="footer_social_item">
                        <a href="#">
                          <i className="fab fa-dribbble" ><FontAwesomeIcon className ='font-awesome' icon={faFacebook} /></i>
                        </a>
                      </li>
                      <li className="footer_social_item">
                        <a href="#">
                          <i className="fab fa-behance" ><FontAwesomeIcon className ='font-awesome' icon={faDribbble} /></i>
                        </a>
                      </li>
                    </ul>
                  </div>
                  <div className="footer_about">
                    <p>
                      Lorem ipsum dolor sit amet, cons ectetur quis ferme
                      adipiscing elit. Suspen dis se tellus eros, placerat quis
                      ferme ntum et, viverra sit amet lacus. Nam gravida quis
                      ferme semper augue.
                    </p>
                  </div>
                </div>

                {/* Footer Useful Links  */}

                <div className="col-lg-3 footer_col">
                  <div className="footer_col_title">useful links</div>
                  <ul className="footer_useful_links">
                    <li className="useful_links_item">
                      <a href="#">Listings</a>
                    </li>
                    <li className="useful_links_item">
                      <a href="#">Favorite Cities</a>
                    </li>
                    <li className="useful_links_item">
                      <a href="#">Clients Testimonials</a>
                    </li>
                    <li className="useful_links_item">
                      <a href="#">Featured Listings</a>
                    </li>
                    <li className="useful_links_item">
                      <a href="#">Properties on Offer</a>
                    </li>
                    <li className="useful_links_item">
                      <a href="#">Services</a>
                    </li>
                    <li className="useful_links_item">
                      <a href="#">News</a>
                    </li>
                    <li className="useful_links_item">
                      <a href="#">Our Agents</a>
                    </li>
                  </ul>
                </div>

                {/* Footer Contact Form  */}
                <div className="col-lg-3 footer_col">
                 <Contactform/>
                </div>

                {/* Footer Contact Info  */}

                <div className="col-lg-3 footer_col">
                  <div className="footer_col_title">contact info</div>
                  <ul className="contact_info_list">
                    <li className="contact_info_item d-flex flex-row">
                      <div>
                        <div className="contact_info_icon">
                          <img
                            src={require("./images/placeholder.svg")}
                            alt=""
                          />
                        </div>
                      </div>
                      <div className="contact_info_text">
                        4127 Raoul Wallenber 45b-c Gibraltar
                      </div>
                    </li>
                    <li className="contact_info_item d-flex flex-row">
                      <div>
                        <div className="contact_info_icon">
                          <img
                            src={require("./images/phone-call.svg")}
                            alt=""
                          />
                        </div>
                      </div>
                      <div className="contact_info_text">2556-808-8613</div>
                    </li>
                    <li className="contact_info_item d-flex flex-row">
                      <div>
                        <div className="contact_info_icon">
                          <img src={require("./images/message.svg")} alt="" />
                        </div>
                      </div>
                      <div className="contact_info_text">
                        <a
                          href="mailto:contactme@gmail.com?Subject=Hello"
                          target="_top"
                        >
                         mazadm009@gmail.com
                        </a>
                      </div>
                    </li>
                    <li className="contact_info_item d-flex flex-row">
                      <div>
                        <div className="contact_info_icon">
                          <img
                            src={require("./images/planet-earth.svg")}
                            alt=""
                          />
                        </div>
                      </div>
                      <div className="contact_info_text">
                        <a href="https://colorlib.com">www.colorlib.com</a>
                      </div>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
          </footer>

          {/* Credits  */}

         
        </div>
      </div>
    );
  }
}
export default About;
