import React, { Component } from "react";
//Components
import contactform from "../Component/smallComponent/contactform";
import Mainnav from "./smallComponent/mainnav";
import Search from "./smallComponent/filtration";
import Signin from './LoginButton'
//sttyle
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
//fa icon
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faTwitter,
  faLinkedin,
  faGithub,
  faFacebook,
  faBehance,
  faDribbble
} from "@fortawesome/fontawesome-free-brands";
import Contactform from "../Component/smallComponent/contactform";

const ListItem = ({
  estate_price,
  id,
  estate_location,
  estate_description,
  estate_room,
  estate_bathroom,
  estate_height
}) => {
  return (
 
        <div className="listing_item_inner d-flex flex-md-row flex-column trans_300">
          <div className="listing_image_container">
            <div className="listing_image">
              {/* <!-- Image by: https://unsplash.com/@breather --> */}
              <div
                className="listing_background"
                style={{
                  backgroundImage: `url(${require("./images/listing_1.jpg")})`
                }}
              />
            </div>
            <div className="featured_card_box d-flex flex-row align-items-center trans_300">
              <img
                src={require("./images/tag.svg")}
                alt="https://www.flaticon.com/authors/lucy-g"
              />
              <div className="featured_card_box_content">
                <div className="featured_card_price_title trans_300">For Sale</div>
                <div className="featured_card_price trans_300">{estate_price}</div>
              </div>
            </div>
          </div>
          <div className="listing_content">
            <div className="listing_title">
              <Link to={`/list/item/${id}`}>House in {estate_location}</Link>
            </div>
            <div className="listing_text">{estate_description}</div>
            <div className="rooms">
              <div className="room">
                <span className="room_title">Bedrooms</span>
                <div className="room_content">
                  <div className="room_image">
                    <img src={require("./images/bedroom.png")} alt="" />
                  </div>
                  <span className="room_number">{estate_room}</span>
                </div>
              </div>

              <div className="room">
                <span className="room_title">Bathrooms</span>
                <div className="room_content">
                  <div className="room_image">
                    <img src={require("./images/shower.png")} alt="" />
                  </div>
                  <span className="room_number">{estate_bathroom}</span>
                </div>
              </div>

              <div className="room">
                <span className="room_title">Area</span>
                <div className="room_content">
                  <div className="room_image">
                    <img src={require("./images/area.png")} alt="" />
                  </div>
                  <span className="room_number">{estate_height} Sq Ft</span>
                </div>
              </div>
            </div>

            <div className="room_tags">
              <span className="room_tag">
                <a href="#">Hottub</a>
              </span>
              <span className="room_tag">
                <a href="#">Swimming Pool</a>
              </span>
              <span className="room_tag">
                <a href="#">Garden</a>
              </span>
              <span className="room_tag">
                <a href="#">Patio</a>
              </span>
              <span className="room_tag">
                <a href="#">Hard Wood Floor</a>
              </span>
            </div>
          </div>
        </div>
    
	
  );
};
class Listing extends Component {
  state = {
    estate_list: [],
    currentPage: 1,
    todosPerPage: 3,
    isTop: true,
    color:""
  };
  handleClick = event => {
    this.setState({
      currentPage: Number(event.target.id)
    });
  };

  handlePageChange(pageNumber) {
    console.log(`active page is ${pageNumber}`);
    this.setState({ activePage: pageNumber });
  }
  getEstateList = async () => {
    try {
      const response = await fetch("http://localhost:8080/estate/list");
      const data = await response.json();
      this.setState({ estate_list: data.result });
    } catch (err) {
      console.log(err);
    }
  };
  componentDidMount() {
    this.getEstateList();
    document.addEventListener('scroll', () => {
      const isTop = window.scrollY < 100;
      if (isTop !== this.state.isTop) {
          this.setState({ isTop })
          this.setState({color:"#0e1d41"})
      }
     
    });
  }
  render() {
    const { estate_list, currentPage, todosPerPage } = this.state;
    const indexOfLastTodo = currentPage * todosPerPage;
    const indexOfFirstTodo = indexOfLastTodo - todosPerPage;
    const currentTodos = estate_list.slice(indexOfFirstTodo, indexOfLastTodo);
    const renderTodos = currentTodos.map(estate => {
      return (
				<div className="listing_item">

          <ListItem
					key={estate.id}
            estate_price={estate.estate_price}
            id={estate.id}
            estate_location={estate.estate_location}
            estate_description={estate.estate_description}
            estate_room={estate.estate_room}
            estate_bathroom={estate.estate_bathroom}
            estate_height={estate.estate_height}
          />
        </div>
      );
    });
    const pageNumbers = [];
    for (let i = 1; i <= Math.ceil(estate_list.length / todosPerPage); i++) {
      pageNumbers.push(i);
    }

    const renderPageNumbers = pageNumbers.map(number => {
      return (
        <li key={number} id={number} onClick={this.handleClick}>
          {number}
        </li>
      );
    });
    return (
      <div className="Listing">
        <div className="super_container">
          {/* <!-- Home --> */}
          <div className="home">
            {/* <!-- Image by: https://unsplash.com/@jbriscoe --> */}

            <div
              className="home_background"
              style={{
                backgroundImage: `url(${require("./images/listings.jpg")})`
              }}
            />

            <div className="container">
              <div className="row">
                <div className="col">
                  <div className="home_content">
                    <div className="home_title">
                      <h2>listings</h2>
                    </div>
                    <div className="breadcrumbs">
                      <span>
                        <Link to="/">Home</Link>
                      </span>
                      <span>
                        <Link to="/list"> Search</Link>
                      </span>
                      <span>
                        <Link to="/list"> Listings</Link>
                      </span>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

          {/* <!-- Header --> */}

          <header className="header trans_300" style={{backgroundColor:this.state.color}} >
            <div className="container">
              <div className="row">
                <div className="col">
                  <div className="header_container d-flex flex-row align-items-center trans_300">
                    {/* <!-- Logo --> */}

                    <div className="logo_container">
                      <a href="#">
                        <div className="logo">
                          <img src={require("./images/logo.png")} alt="" />
                          <span>mazad</span>
                        </div>
                      </a>
                    </div>

                    {/* <!-- Main Navigation --> */}

                    <Mainnav />
                    {/* <!-- Phone Home --> */}

                    <Signin/>
                    {/* <!-- Hamburger --> */}

                    <div className="hamburger_container menu_mm">
                      <div className="hamburger menu_mm">
                        <i className="fas fa-bars trans_200 menu_mm" />
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            {/* <!-- Menu --> */}

            <div className="menu menu_mm">
              <ul className="menu_list">
                <li className="menu_item">
                  <div className="container">
                    <div className="row">
                      <div className="col">
                        <Link to="/">home</Link>
                      </div>
                    </div>
                  </div>
                </li>
                <li className="menu_item">
                  <div className="container">
                    <div className="row">
                      <div className="col">
                        <Link to="/about ">about us</Link>
                      </div>
                    </div>
                  </div>
                </li>
                <li className="menu_item">
                  <div className="container">
                    <div className="row">
                      <div className="col">
                        <Link to="/list">listings</Link>
                      </div>
                    </div>
                  </div>
                </li>
                <li className="menu_item">
                  <div className="container">
                    <div className="row">
                      <div className="col">
                        <Link to="/news">news</Link>
                      </div>
                    </div>
                  </div>
                </li>
                <li className="menu_item">
                  <div className="container">
                    <div className="row">
                      <div className="col">
                        <Link to="/contact">contact</Link>
                      </div>
                    </div>
                  </div>
                </li>
              </ul>
            </div>
          </header>

          {/* <!-- Listings --> */}

          <div className="listings">
            <div className="container">
              <div className="row">
                {/* <!-- Search Sidebar --> */}

                <div className="col-lg-4 sidebar_col">
                  {/* <!-- Search Box --> */}

                  <div className="search_box">
                    <Search />
                  </div>
                </div>

                {/* <!-- Listings --> */}
								<div className="col-lg-8 listings_col">
                {renderTodos}
								</div>
              </div>

              <div className="row">
                <div className="col clearfix">
                  <div className="listings_nav">
                    <ul className="listings_nav_item active">
                      <li className="listings_nav_item active">
											{renderPageNumbers}
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
          </div>

          {/* <!-- Newsletter --> */}

          <div className="newsletter">
            <div className="container">
              <div className="row row-equal-height">
                <div className="col-lg-6">
                  <div className="newsletter_title">
                    <h3>subscribe to our newsletter</h3>
                    <span className="newsletter_subtitle">
                      Get the latest offers
                    </span>
                  </div>
                  <div className="newsletter_form_container">
                    <form action="#">
                      <div className="newsletter_form_content d-flex flex-row">
                        <input
                          id="newsletter_email"
                          className="newsletter_email"
                          type="email"
                          placeholder="Your email here"
                          required="required"
                          data-error="Valid email is required."
                        />
                        <button
                          id="newsletter_submit"
                          type="submit"
                          className="newsletter_submit_btn trans_200"
                          value="Submit"
                        >
                          subscribe
                        </button>
                      </div>
                    </form>
                  </div>
                </div>

                <div className="col-lg-6">
                  <a href="#">
                    <div className="weekly_offer">
                      <div className="weekly_offer_content d-flex flex-row">
                        <div className="weekly_offer_icon d-flex flex-column align-items-center justify-content-center">
                          <img src={require("./images/prize.svg")} alt="" />
                        </div>
                        <div className="weekly_offer_text text-center">
                          weekly offer
                        </div>
                      </div>
                      <div
                        className="weekly_offer_image"
                        style={{
                          backgroundImage: `url(${require("./images/weekly.jpg")})`
                        }}
                      />
                    </div>
                  </a>
                </div>
              </div>
            </div>
          </div>

          {/* <!-- Footer --> */}

          <footer className="footer">
            <div className="container">
              <div className="row">
                {/* <!-- Footer About --> */}

                <div className="col-lg-3 footer_col">
                  <div className="footer_col_title">
                    <div className="logo_container">
                      <a href="#">
                        <div className="logo">
                          <img src={require("./images/logo.png")} alt="" />
                          <span>mazad</span>
                        </div>
                      </a>
                    </div>
                  </div>
                  <div className="footer_social">
                    <ul className="footer_social_list">
                      <li className="footer_social_item">
                        <a href="#">
                          <i className="fab fa-pinterest">
                            <FontAwesomeIcon
                              className="font-awesome"
                              icon={faBehance}
                            />
                          </i>
                        </a>
                      </li>
                      <li className="footer_social_item">
                        <a href="#">
                          <i className="fab fa-facebook-f">
                            <FontAwesomeIcon
                              className="font-awesome"
                              icon={faFacebook}
                            />
                          </i>
                        </a>
                      </li>
                      <li className="footer_social_item">
                        <a href="#">
                          <i className="fab fa-twitter">
                            <FontAwesomeIcon
                              className="font-awesome"
                              icon={faTwitter}
                            />
                          </i>
                        </a>
                      </li>
                      <li className="footer_social_item">
                        <a href="#">
                          <i className="fab fa-dribbble">
                            <FontAwesomeIcon
                              className="font-awesome"
                              icon={faDribbble}
                            />
                          </i>
                        </a>
                      </li>
                      <li className="footer_social_item">
                        <a href="#">
                          <i className="fab fa-behance">
                            <FontAwesomeIcon
                              className="font-awesome"
                              icon={faBehance}
                            />
                          </i>
                        </a>
                      </li>
                    </ul>
                  </div>
                  <div className="footer_about">
                    <p>
                      Lorem ipsum dolor sit amet, cons ectetur quis ferme
                      adipiscing elit. Suspen dis se tellus eros, placerat quis
                      ferme ntum et, viverra sit amet lacus. Nam gravida quis
                      ferme semper augue.
                    </p>
                  </div>
                </div>

                {/* <!-- Footer Useful Links --> */}

                <div className="col-lg-3 footer_col">
                  <div className="footer_col_title">useful links</div>
                  <ul className="footer_useful_links">
                    <li className="useful_links_item">
                      <a href="#">Listings</a>
                    </li>
                    <li className="useful_links_item">
                      <a href="#">Favorite Cities</a>
                    </li>
                    <li className="useful_links_item">
                      <a href="#">Clients Testimonials</a>
                    </li>
                    <li className="useful_links_item">
                      <a href="#">Featured Listings</a>
                    </li>
                    <li className="useful_links_item">
                      <a href="#">Properties on Offer</a>
                    </li>
                    <li className="useful_links_item">
                      <a href="#">Services</a>
                    </li>
                    <li className="useful_links_item">
                      <a href="#">News</a>
                    </li>
                    <li className="useful_links_item">
                      <a href="#">Our Agents</a>
                    </li>
                  </ul>
                </div>
                {/* 
				<!-- Footer Contact Form --> */}
                <div className="col-lg-3 footer_col">
                  <Contactform />
                </div>

                {/* <!-- Footer Contact Info --> */}

                <div className="col-lg-3 footer_col">
                  <div className="footer_col_title">contact info</div>
                  <ul className="contact_info_list">
                    <li className="contact_info_item d-flex flex-row">
                      <div>
                        <div className="contact_info_icon">
                          <img
                            src={require("./images/placeholder.svg")}
                            alt=""
                          />
                        </div>
                      </div>
                      <div className="contact_info_text">
                        4127 Raoul Wallenber 45b-c Gibraltar
                      </div>
                    </li>
                    <li className="contact_info_item d-flex flex-row">
                      <div>
                        <div className="contact_info_icon">
                          <img
                            src={require("./images/phone-call.svg")}
                            alt=""
                          />
                        </div>
                      </div>
                      <div className="contact_info_text">2556-808-8613</div>
                    </li>
                    <li className="contact_info_item d-flex flex-row">
                      <div>
                        <div className="contact_info_icon">
                          <img src={require("./images/message.svg")} alt="" />
                        </div>
                      </div>
                      <div className="contact_info_text">
                        <a
                          href="mailto:contactme@gmail.com?Subject=Hello"
                          target="_top"
                        >
                          mazadm009@gmail.com
                        </a>
                      </div>
                    </li>
                    <li className="contact_info_item d-flex flex-row">
                      <div>
                        <div className="contact_info_icon">
                          <img
                            src={require("./images/planet-earth.svg")}
                            alt=""
                          />
                        </div>
                      </div>
                      <div className="contact_info_text">
                        <a href="https://colorlib.com">mazadm009@gmail.com</a>
                      </div>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
          </footer>
        </div>
      </div>
    );
  }
}

export default Listing;
