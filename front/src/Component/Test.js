import React, { Component } from "react";
import { LinkContainer } from "react-router-bootstrap";
//Components
import Mainnav from "./smallComponent/mainnav";
import contactform from "./smallComponent/contactform";
import Heeader from "./header.js";
import Searchbox from "./smallComponent/searchbox.js";
import Signin from './LoginButton'
//CSS
import "./CSS/background.css";


import Image from "./images/home_slider_bcg.jpg";

//route
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
//fa icon
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faTwitter,
  faLinkedin,
  faGithub,
  faFacebook,
  faBehance,
  faDribbble
} from "@fortawesome/fontawesome-free-brands";
import Contactform from "./smallComponent/contactform";
let image = "home_slider_bcg.jpg";
let styles = {
  backgroundImage: `url(${Image})`
};
class test extends Component {
  state = {
    testimonial_list: [],
    three_list:[],
    isTop: true,
    color:""
  };
  onChange = e => {
    this.props.history.push(`/${e.target.value}`);
  };

  componentDidMount() {
    this.getTestimonialList();
    this.getAuctionList();
    document.addEventListener('scroll', () => {
      const isTop = window.scrollY < 100;
      if (isTop !== this.state.isTop) {
          this.setState({ isTop })
          this.setState({color:"#0e1d41"})
      }
     
    });
  }
  getTestimonialList = async () => {
    try {
      const response = await fetch("//localhost:8080/testimonial/list");
      const data = await response.json();
      this.setState({ testimonial_list: data.result });
    } catch (err) {
      console.log(err);
    }
  };
	getAuctionList=async()=>{
		try{
			const response=await fetch('http://localhost:8080/auction/list')
			const data = await response.json();
			this.setState({three_list:data.result.slice(-3)})
	
		}
		catch(err){
			console.log(err)
		}
	}

  render() {
   
    return (
      <div className="Index">
        <div className="super_container">
          {/* Home  */}
          <div className="homes">
            {/* Home Slider */}
            <div className="home_slider_container">
              <div className="owl-carousel owl-theme home_slider">
                {/* Home Slider Item  */}
                <div className="owl-item home_slider_item">
                  {/* Image by https://unsplash.com/@aahubs  */}
                  <div
                    className="home_slider_background"
                    //  style="background-image:url(images/home_slider_bcg.jpg)"
                    style={styles}
                  />
                  <div className="home_slider_content_container text-center">
                    <div className="home_slider_content">
                      <h1
                        data-animation-in="flipInX"
                        data-animation-out="animate-out fadeOut"
                      >
                        find your home
                      </h1>
                    </div>
                  </div>
                </div>

                {/* Home Slider Item  */}
               

                {/* Home Slider Item  */}
                </div>

              {/* Home Slider Nav */}
              <div className="home_slider_nav_left home_slider_nav d-flex flex-row align-items-center justify-content-end">
                <img src={require("./images/nav_left.png")} alt="" />
              </div>
            </div>
          </div>
          {/* // Header  */}
          <header className="header trans_300 " style={{backgroundColor:this.state.color}} >
            <div className="container">
              <div className="row">
                <div className="col">
                  <div className="header_container d-flex flex-row align-items-center trans_300">
                    {/* Logo  */}

                    <div className="logo_container">
                      <a href="#">
                        <div className="logo">
                          <img src={require("./images/logo.png")} alt="" />
                          <span>mazad</span>
                        </div>
                      </a>
                    </div>

                    {/* Main Navigation */}

                    <Mainnav />

                    {/* Phone Home  */}

                    <Signin/>

                    {/* Hamburger  */}

                    <div className="hamburger_container menu_mm">
                      <div className="hamburger menu_mm">
                        <i className="fas fa-bars trans_200 menu_mm" />
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            {/* Menu */}

            <div className="menu menu_mm">
              <ul className="menu_list">
                <li className="menu_item">
                  <div className="container">
                    <div className="row">
                      <div className="col">
                        <Link to="/">home</Link>
                      </div>
                    </div>
                  </div>
                </li>
                <li className="menu_item">
                  <div className="container">
                    <div className="row">
                      <div className="col">
                        <Link to="/about">about us</Link>
                      </div>
                    </div>
                  </div>
                </li>
                <li className="menu_item">
                  <div className="container">
                    <div className="row">
                      <div className="col">
                        <Link to="/list">listings</Link>
                      </div>
                    </div>
                  </div>
                </li>
                <li className="menu_item">
                  <div className="container">
                    <div className="row">
                      <div className="col">
                        <Link to="/news">news</Link>
                      </div>
                    </div>
                  </div>
                </li>
                <li className="menu_item">
                  <div className="container">
                    <div className="row">
                      <div className="col">
                        <Link to="/contact">contact</Link>
                      </div>
                    </div>
                  </div>
                </li>
              </ul>
            </div>
          </header>
          {/* Search Box  */}
          <Searchbox />
          {/* Featured properties */}
          <div className="featured">
            <div className="container">
              <div className="row">
                <div className="col">
                  <div className="section_title text-center">
                    <h3>featured properties</h3>
                    <span className="section_subtitle">
                      See our best offers
                    </span>
                  </div>
                </div>
              </div>

              <div className="row featured_row">
              {this.state.three_list.map(auction=>(
                <div className="col-lg-4 featured_card_col">
                  <div className="featured_card_container">
                  
                     
                
                    <div className="card featured_card trans_300" key={auction.id}>
                      <div className="featured_panel">featured</div>
                      <img
                        className="card-img-top"
                        src={require("./images/featured_1.jpg")}
                        alt="https://unsplash.com/@breather"
                      />
                      <div className="card-body">
                        <div className="card-title">
                          <Link to={`/auction/item/${auction.id}`}>
                            House in {auction.estate_location}
                          </Link>
                        </div>
                        <div className="card-text">
                          {auction.estate_description}
                        </div>
                        <div className="rooms">
                          <div className="room">
                            <span className="room_title">Bedrooms</span>
                            <div className="room_content">
                              <div className="room_image">
                                <img
                                  src={require("./images/bedroom.png")}
                                  alt=""
                                />
                              </div>
                              <span className="room_number">{auction.estate_room}</span>
                            </div>
                          </div>

                          <div className="room">
                            <span className="room_title">Bathrooms</span>
                            <div className="room_content">
                              <div className="room_image">
                                <img
                                  src={require("./images/shower.png")}
                                  alt=""
                                />
                              </div>
                              <span className="room_number">{auction.estate_bathroom}</span>
                            </div>
                          </div>

                          <div className="room">
                            <span className="room_title">Area</span>
                            <div className="room_content">
                              <div className="room_image">
                                <img
                                  src={require("./images/area.png")}
                                  alt=""
                                />
                              </div>
                              <span className="room_number">{auction.estate_height} Sq Ft</span>
                            </div>
                          </div>

                          <div className="room">
                            <span className="room_title">Patio</span>
                            <div className="room_content">
                              <div className="room_image">
                                <img
                                  src={require("./images/patio.png")}
                                  alt=""
                                />
                              </div>
                              <span className="room_number">1</span>
                            </div>
                          </div>

                          <div className="room">
                            <span className="room_title">Garage</span>
                            <div className="room_content">
                              <div className="room_image">
                                <img
                                  src={require("./images/garage.png")}
                                  alt=""
                                />
                              </div>
                              <span className="room_number">2</span>
                            </div>
                          </div>
                        </div>

                        <div className="room_tags">
                          <span className="room_tag">
                            <a href="#">Hottub</a>
                          </span>
                          <span className="room_tag">
                            <a href="#">Swimming Pool</a>
                          </span>
                          <span className="room_tag">
                            <a href="#">Garden</a>
                          </span>
                          <span className="room_tag">
                            <a href="#">Patio</a>
                          </span>
                          <span className="room_tag">
                            <a href="#">Hard Wood Floor</a>
                          </span>
                        </div>
                      </div>
                    </div> 

                    <div className="featured_card_box d-flex flex-row align-items-center trans_300">
                      <img
                        src={require("./images/tag.svg")}
                        alt="https://www.flaticon.com/authors/lucy-g"
                      />
                      <div className="featured_card_box_content">
                        <div className="featured_card_price_title">
                          For Sale
                        </div>
                        <div className="featured_card_price">${auction.estate_start_price}</div>
                      </div>
                    </div>
                  </div>
                 
                </div>
                   ))}
                </div>
                </div>
          </div>
          {/* // Testimonials  */}
          <div className="testimonials">
            <div className="testimonials_background_container prlx_parent">
              <div
                className="testimonials_background prlx"
                // style="background-image:url(images/testimonials_background.jpg)"
                style={{
                  backgroundImage: `url(${require("./images/testimonials_background.jpg")})`
                }}
              />
            </div>
            <div className="container">
              <div className="row">
                <div className="col">
                  <div className="section_title text-center">
                    <h3>clients testimonials</h3>
                    <span className="section_subtitle">
                      See our best offers
                    </span>
                  </div>
                </div>
              </div>

              <div className="row">
                <div className="col-lg-10 offset-lg-1">
                  <div className="testimonials_slider_container">
                    {/* Testimonials Slider  */}
                    <div className="owl-carousel owl-theme testimonials_slider">
                      {/* Testimonials Item  */}
                      <div className="owl-item">
                      {this.state.testimonial_list.map(testimonial=>(
                     
                    
                        <div className="testimonials_item text-center" key={testimonial.testimonial_id}>
                          <p className="testimonials_text">
                         {testimonial.testimonial_description}
                          </p>
                          <div className="testimonial_user">
                            <div className="testimonial_image mx-auto">
                              <img
                                src={require("./images/person.jpg")}
                                alt="https://unsplash.com/@remdesigns"
                              />
                            </div>
                            <div className="testimonial_name">
                             {testimonial.testimonial_author}
                            </div>
                            <div className="testimonial_title">
                              Client in Lebanon
                            </div>
                          </div>
                        </div>
                          ))}
                      </div>

                      {/* Testimonials Item  */}
                   
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          {/* Workflow */}
          <div className="workflow">
            <div className="container">
              <div className="row">
                <div className="col">
                  <div className="section_title text-center">
                    <h3>see how we operate</h3>
                    <span className="section_subtitle">
                      What you need to do
                    </span>
                  </div>
                </div>
              </div>

              <div className="row workflow_row">
                <div className="workflow_rocket">
                  <img src={require("./images/rocket.png")} alt="" />
                </div>

                {/* Workflow Item  */}
                <div className="col-lg-4 workflow_col">
                  <div className="workflow_item">
                    <div className="workflow_image_container d-flex flex-column align-items-center justify-content-center">
                      <div className="workflow_image_background">
                        <div className="workflow_circle_outer trans_200" />
                        <div className="workflow_circle_inner trans_200" />
                        <div className="workflow_num text-center trans_200">
                          <span>01.</span>
                        </div>
                      </div>
                      <div className="workflow_image">
                        <img src={require("./images/workflow_1.png")} alt="" />
                      </div>
                    </div>
                    <div className="workflow_item_content text-center">
                      <div className="workflow_title">Choose a Location</div>
                      <p className="workflow_text">
                        Donec ullamcorper nulla non metus auctor fringi lla.
                        Curabitur blandit tempus porttitor.
                      </p>
                    </div>
                  </div>
                </div>

                {/* Workflow Item */}
                <div className="col-lg-4 workflow_col">
                  <div className="workflow_item">
                    <div className="workflow_image_container d-flex flex-column align-items-center justify-content-center">
                      <div className="workflow_image_background">
                        <div className="workflow_circle_outer trans_200" />
                        <div className="workflow_circle_inner trans_200" />
                        <div className="workflow_num text-center trans_200">
                          <span>02.</span>
                        </div>
                      </div>
                      <div className="workflow_image">
                        <img src={require("./images/workflow_2.png")} alt="" />
                      </div>
                    </div>
                    <div className="workflow_item_content text-center">
                      <div className="workflow_title">
                        Find the Perfect Home
                      </div>
                      <p className="workflow_text">
                        Donec ullamcorper nulla non metus auctor fringi lla.
                        Curabitur blandit tempus porttitor.
                      </p>
                    </div>
                  </div>
                </div>

                {/* Workflow Item  */}
                <div className="col-lg-4 workflow_col">
                  <div className="workflow_item">
                    <div className="workflow_image_container d-flex flex-column align-items-center justify-content-center">
                      <div className="workflow_image_background">
                        <div className="workflow_circle_outer trans_200" />
                        <div className="workflow_circle_inner trans_200" />
                        <div className="workflow_num text-center trans_200">
                          <span>03.</span>
                        </div>
                      </div>
                      <div className="workflow_image">
                        <img src={require("./images/workflow_3.png")} alt="" />
                      </div>
                    </div>
                    <div className="workflow_item_content text-center">
                      <div className="workflow_title">
                        Move in your new life
                      </div>
                      <p className="workflow_text">
                        Donec ullamcorper nulla non metus auctor fringi lla.
                        Curabitur blandit tempus porttitor.
                      </p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          {/* Cities  */}
          <div className="cities">
            <div className="cities_background" />
            <div className="container">
              <div className="row">
                <div className="col">
                  <div className="section_title text-center">
                    <h3>cities clients prefer</h3>
                    <span className="section_subtitle">
                      What you need to do
                    </span>
                  </div>
                </div>
              </div>

             
            </div>
          </div>
          {/* Call to Action  */}
          <div className="cta_1">
            <div
              className="cta_1_background"
              //   style="background-image:url(images/cta_1.jpg)"
              style={{ backgroundImage: `url(require("images/cta_1.jpg"))` }}
            />
            <div className="container">
              <div className="row">
                <div className="col">
                  <div className="cta_1_content d-flex flex-lg-row flex-column align-items-center justify-content-start">
                    <h3 className="cta_1_text text-lg-left text-center">
                      Do you want to talk with one of our{" "}
                      <span>real estate experts?</span>
                    </h3>
                    <div className="cta_1_phone">
                      Call now: +0080 234 567 84441
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          {/* Newsletter */}
          <div className="newsletter">
            <div className="container">
              <div className="row row-equal-height">
                <div className="col-lg-6">
                  <div className="newsletter_title">
                    <h3>subscribe to our newsletter</h3>
                    <span className="newsletter_subtitle">
                      Get the latest offers
                    </span>
                  </div>
                  <div className="newsletter_form_container">
                    <form action="#">
                      <div className="newsletter_form_content d-flex flex-row">
                        <input
                          id="newsletter_email"
                          className="newsletter_email"
                          type="email"
                          placeholder="Your email here"
                          required="required"
                          data-error="Valid email is required."
                        />
                        <button
                          id="newsletter_submit"
                          type="submit"
                          className="newsletter_submit_btn trans_200"
                          value="Submit"
                        >
                          subscribe
                        </button>
                      </div>
                    </form>
                  </div>
                </div>

                <div className="col-lg-6">
                  <a href="#">
                    <div className="weekly_offer">
                      <div className="weekly_offer_content d-flex flex-row">
                        <div className="weekly_offer_icon d-flex flex-column align-items-center justify-content-center">
                          <img src={require("./images/prize.svg")} alt="" />
                        </div>
                        <div className="weekly_offer_text text-center">
                          weekly offer
                        </div>
                      </div>
                      <div
                        className="weekly_offer_image"
                        // style="background-image:url(images/weekly.jpg)"
                        style={{
                          backgroundImage: `url(require("images/weekly.jpg"))`
                        }}
                      />
                    </div>
                  </a>
                </div>
              </div>
            </div>
          </div>
          {/* Footer  */}
          <footer className="footer">
            <div className="container">
              <div className="row">
                {/* Footer About  */}

                <div className="col-lg-3 footer_col">
                  <div className="footer_col_title">
                    <div className="logo_container">
                      <a href="#">
                        <div className="logo">
                          <img src={require("./images/logo.png")} alt="" />
                          <span>mazad</span>
                        </div>
                      </a>
                    </div>
                  </div>
                  <div className="footer_social">
                    <ul className="footer_social_list">
                      <li className="footer_social_item">
                        <a href="#">
                          <i className="fab fa-pinterest">
                            <FontAwesomeIcon
                              className="font-awesome"
                              icon={faGithub}
                            />
                          </i>
                        </a>
                      </li>
                      <li className="footer_social_item">
                        <a href="#">
                          <i className="fab fa-facebook-f">
                            <FontAwesomeIcon
                              className="font-awesome"
                              icon={faFacebook}
                            />
                          </i>
                        </a>
                      </li>
                      <li className="footer_social_item">
                        <a href="#">
                          <i className="fab fa-twitter">
                            {" "}
                            <FontAwesomeIcon
                              className="font-awesome"
                              icon={faTwitter}
                            />
                          </i>
                        </a>
                      </li>
                      <li className="footer_social_item">
                        <a href="#">
                          <i className="fab fa-dribbble">
                            <FontAwesomeIcon
                              className="font-awesome"
                              icon={faDribbble}
                            />
                          </i>
                        </a>
                      </li>
                      <li className="footer_social_item">
                        <a href="#">
                          <i className="fab fa-behance">
                            <FontAwesomeIcon
                              className="font-awesome"
                              icon={faBehance}
                            />
                          </i>
                        </a>
                      </li>
                    </ul>
                  </div>
                  <div className="footer_about">
                    <p>
                      Lorem ipsum dolor sit amet, cons ectetur quis ferme
                      adipiscing elit. Suspen dis se tellus eros, placerat quis
                      ferme ntum et, viverra sit amet lacus. Nam gravida quis
                      ferme semper augue.
                    </p>
                  </div>
                </div>

                {/* Footer Useful Links  */}

                <div className="col-lg-3 footer_col">
                  <div className="footer_col_title">useful links</div>
                  <ul className="footer_useful_links">
                    <li className="useful_links_item">
                      <a href="#">Listings</a>
                    </li>
                    <li className="useful_links_item">
                      <a href="#">Favorite Cities</a>
                    </li>
                    <li className="useful_links_item">
                      <a href="#">Clients Testimonials</a>
                    </li>
                    <li className="useful_links_item">
                      <a href="#">Featured Listings</a>
                    </li>
                    <li className="useful_links_item">
                      <a href="#">Properties on Offer</a>
                    </li>
                    <li className="useful_links_item">
                      <a href="#">Services</a>
                    </li>
                    <li className="useful_links_item">
                      <a href="#">News</a>
                    </li>
                    <li className="useful_links_item">
                      <a href="#">Our Agents</a>
                    </li>
                  </ul>
                </div>

                {/* Footer Contact Form  */}
                <div className="col-lg-3 footer_col">
                  <Contactform />
                </div>

                {/* Footer Contact Info  */}

                <div className="col-lg-3 footer_col">
                  <div className="footer_col_title">contact info</div>
                  <ul className="contact_info_list">
                    <li className="contact_info_item d-flex flex-row">
                      <div>
                        <div className="contact_info_icon">
                          <img
                            src={require("./images/placeholder.svg")}
                            alt=""
                          />
                        </div>
                      </div>
                      <div className="contact_info_text">
                        4127 Raoul Wallenber 45b-c Gibraltar
                      </div>
                    </li>
                    <li className="contact_info_item d-flex flex-row">
                      <div>
                        <div className="contact_info_icon">
                          <img
                            src={require("./images/phone-call.svg")}
                            alt=""
                          />
                        </div>
                      </div>
                      <div className="contact_info_text">2556-808-8613</div>
                    </li>
                    <li className="contact_info_item d-flex flex-row">
                      <div>
                        <div className="contact_info_icon">
                          <img src={require("./images/message.svg")} alt="" />
                        </div>
                      </div>
                      <div className="contact_info_text">
                        <a
                          href="mailto:contactme@gmail.com?Subject=Hello"
                          target="_top"
                        >
                          mazadm009@gmail.com
                        </a>
                      </div>
                    </li>
                    <li className="contact_info_item d-flex flex-row">
                      <div>
                        <div className="contact_info_icon">
                          <img
                            src={require("./images/planet-earth.svg")}
                            alt=""
                          />
                        </div>
                      </div>
                      <div className="contact_info_text">
                        <a href="https://colorlib.com">www.colorlib.com</a>
                      </div>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
          </footer>
    
        </div>
      </div>
    );
  }
}

export default test;
