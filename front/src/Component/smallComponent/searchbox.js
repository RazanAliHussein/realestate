import React, { Component } from "react";
//toast
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { Link } from "react-router-dom";

class Searchbox extends Component {
  state = {
    estate_location: "any",
    estate_floor: "any",
    estate_room: "any",
    estate_bathroom: "any",
    estate_garage: "any",
    estate_type:"any"
  };
  handleChangeRoom = e => {
    this.setState({ estate_room: e.target.value });
  };
  handleChangeBathroom = e => {
    this.setState({ estate_bathroom: e.target.value });
  };
  handleChangeFloor = e => {
    this.setState({ estate_floor: e.target.value });
  };
  handleChangeLocation = e => {
    this.setState({ estate_location: e.target.value });
  };
  handleChangeGarage = e => {
    this.setState({ estate_garage: e.target.value });
  };
  handleChangeType = e => {
    this.setState({ estate_type: e.target.value });
  };
  onSubmit = evt => {
    evt.preventDefault();
    console.log(this.state);
  };
  render() {
    return (
      <div className="search_box">
        <div className="container">
          <div className="row">
            <div className="col">
              <div className="search_box_outer">
                <div className="search_box_inner">
                  {/* Search Box Title */}
                  <div className="search_box_title text-center">
                    <div className="search_box_title_inner">
                      <div className="search_box_title_icon d-flex flex-column align-items-center justify-content-center">
                        <img src={require("../images/search.png")} alt="" />
                      </div>
                      <span>search your home</span>
                    </div>
                  </div>

                  {/* Search Arrow */}
                  <div className="search_arrow_box">
                    <div className="search_arrow_box_inner">
                      <div className="search_arrow_circle d-flex flex-column align-items-center justify-content-center">
                        <span>Search it here</span>
                      </div>
                      <img src={require("../images/search_arrow.png")} alt="" />
                    </div>
                  </div>

                  {/* Search Form  */}
                  <form className="search_form" onSubmit={this.onSubmit}>
                    <div className="search_box_container">
                      <ul className="dropdown_row clearfix">
                        <li className="dropdown_item dropdown_item_5">
                          <div className="dropdown_item_title">Keywords</div>
                          <select
                            name="keywords"
                            id="keywords"
                            className="dropdown_item_select"
                          >
                            <option>Any</option>
                          </select>
                        </li>
                        <li className="dropdown_item dropdown_item_5">
                          <div className="dropdown_item_title">Property ID</div>
                          <select
                            name="property_ID"
                            id="property_ID"
                            className="dropdown_item_select"
                          >
                            <option>Any</option>
                          </select>
                        </li>
                        <li className="dropdown_item dropdown_item_5">
                          <div className="dropdown_item_title">
                            Property Status
                          </div>
                          <select
                            name="property_status"
                            id="property_status"
                            className="dropdown_item_select"
                          >
                            <option>Any</option>
                          </select>
                        </li>
                        <li className="dropdown_item dropdown_item_5">
                          <div className="dropdown_item_title">Location</div>
                          <select
                            name="property_location"
                            id="property_location"
                            className="dropdown_item_select"
                            onChange={this.handleChangeLocation}
                          >
                            <option>Any</option>
                            <option>Bekaa</option>
                            <option>Beirut</option>
                          </select>
                        </li>
                        <li className="dropdown_item dropdown_item_5">
                          <div className="dropdown_item_title">
                            Property floor
                          </div>
                          <select
                            name="property_type"
                            id="property_type"
                            className="dropdown_item_select"
                            onChange={this.handleChangeFloor}
                          >
                            <option>Any</option>
                            <option>0</option>
                            <option>1</option>
                            <option>2</option>
                            <option>3</option>
                            <option>4</option>
                          </select>
                        </li>
                      </ul>
                    </div>

                    <div className="search_box_container">
                      <ul className="dropdown_row clearfix">
                        <li className="dropdown_item dropdown_item_6">
                          <div className="dropdown_item_title">Rooms no</div>
                          <select
                            name="bedrooms_no"
                            id="bedrooms_no"
                            className="dropdown_item_select"
                            onChange={this.handleChangeRoom}
                          >
                            <option>Any</option>
                            <option>1</option>
                            <option>2</option>
                            <option>3</option>
                            <option>4</option>
                            <option>5</option>
                          </select>
                        </li>
                        <li className="dropdown_item dropdown_item_6">
                          <div className="dropdown_item_title">Type</div>
                          <select
                            name="bedrooms_no"
                            id="bedrooms_no"
                            className="dropdown_item_select"
                            onChange={this.handleChangeType}
                          >
                            <option>Any</option>
                            <option>Auction </option>
                            <option>Direct</option>
                           
                          </select>
                        </li>
                        <li className="dropdown_item dropdown_item_6">
                          <div className="dropdown_item_title">
                            Bathrooms no
                          </div>
                          <select
                            name="bathrooms_no"
                            id="bathrooms_no"
                            className="dropdown_item_select"
                            onChange={this.handleChangeBathroom}
                          >
                            <option>Any</option>
                            <option>1</option>
                            <option>2</option>
                            <option>3</option>
                            <option>4</option>
                          </select>
                        </li>
                        <li className="dropdown_item dropdown_item_6">
                          <div className="dropdown_item_title">
                            Property garage
                          </div>
                          <select
                            name="min_price"
                            id="min_price"
                            className="dropdown_item_select"
                            onChange={this.handleChangeGarage}
                          >
                            <option>Any</option>
                            <option>0</option>
                            <option>1</option>
                          </select>
                        </li>

                        <li className="dropdown_item">
                          <div className="search_button">
                            <Link
                              to={{
                                pathname: "/search",
                                state: {
                                  estate_floor: this.state.estate_floor,
                                  estate_bathroom: this.state.estate_bathroom,
                                  estate_garage: this.state.estate_garage,
                                  estate_room: this.state.estate_room,
                                  estate_location: this.state.estate_location,
                                  estate_type:this.state.estate_type
                                }
                              }}
                            >
                              <input
                                value="search"
                                type="submit"
                                className="search_submit_button"
                              />
                            </Link>
                          </div>
                        </li>
                      </ul>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Searchbox;
