import React, { Component } from 'react';
//toast
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';



class Contactform extends Component {
  state = {
    name: "",
    email: "",
    message: "",
    subject: ""
  };
   ///Submit function
   sendEmail(name, email, message) {
    try{
      toast("message sent");
    fetch(`//localhost:8080/send`, {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json"
      },
      body: JSON.stringify({
        name: this.state.name,
        email: this.state.email,
        message: this.state.message
      })
    })
    
      .then(res => res.json())
      .then(res => {
        console.log("here is the response: ", res);
       
      })
      
      .catch(err => {
        console.error("here is the error: ", err);
      });
    }
    catch(err){
      toast.error(err.message);
    }
  }
  //submit function end
  //handle the change
  handleChangeName=(e)=> {
    this.setState({name: e.target.value});
  }
  handleChangeEmail=(e)=>{
    this.setState({email: e.target.value});
  }
  handleChangeSubject=(e)=>{
    this.setState({subject: e.target.value});
  }
  handleChangeMessage=(e)=>{
    this.setState({message: e.target.value});
  }
  //handle the change end
  render() {
    return (
     
      <div className="Contactform">
      <ToastContainer/>
     <div className="footer_col_title">say hello</div>
					<div className="footer_contact_form_container">
						<div id="footer_contact_form" className="footer_contact_form" action="post" >
							<input id="contact_form_name" className="input_field contact_form_name" type="text" placeholder="Name" required="required" data-error="Name is required." onChange={this.handleChangeName}/>
							<input id="contact_form_email" className="input_field contact_form_email" type="email" placeholder="E-mail" required="required" data-error="Valid email is required." onChange={this.handleChangeEmail}/>
							<textarea id="contact_form_message" className="text_field contact_form_message" name="message" placeholder="Message" required="required" data-error="Please, write us a message." onChange={this.handleChangeMessage}></textarea>
							<button id="contact_send_btn" type="submit" className="contact_send_btn trans_200" value="Submit" onClick={() => this.sendEmail()}>send</button>
						</div>
					</div>
      </div>
    
    );
  }
}

export default Contactform;
