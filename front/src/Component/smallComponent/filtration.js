import React, { Component } from 'react';
//toast
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import {Link } from 'react-router-dom'


class Filter extends Component {
  state = {
    estate_location: "any",
    estate_floor: "any",
    estate_room: "any",
    estate_bathroom: "any",
    estate_garage: "any",
    estate_type:"any"
  };
  handleChangeRoom = e => {
    this.setState({ estate_room: e.target.value });
  };
  handleChangeBathroom = e => {
    this.setState({ estate_bathroom: e.target.value });
  };
  handleChangeFloor = e => {
    this.setState({ estate_floor: e.target.value });
  };
  handleChangeLocation = e => {
    this.setState({ estate_location: e.target.value });
  };
  handleChangeGarage = e => {
    this.setState({ estate_garage: e.target.value });
  };
  handleChangeType = e => {
    this.setState({ estate_type: e.target.value });
  };
  onSubmit = evt => {
    evt.preventDefault();
  
  };
  render() {
    return (
     
    
      	<div class="search_box_content">

{/* <!-- Search Box Title --> */}
<div class="search_box_title text-center">
    <div class="search_box_title_inner">
        <div class="search_box_title_icon d-flex flex-column align-items-center justify-content-center"><img src={require('../images/search.png')} alt=""/></div>
        <span>search your home</span>
    </div>
</div>
     <form class="search_form" onSubmit={this.onSubmit}>
								<div class="search_box_container">
									<ul class="dropdown_row clearfix">
										<li class="dropdown_item">
											<div class="dropdown_item_title">Keywords</div>
											<select name="keywords" id="keywords" class="dropdown_item_select">
												<option>Any</option>
											
											</select>
										</li>
										<li class="dropdown_item">
											<div class="dropdown_item_title">Property ID</div>
											<select name="property_ID" id="property_ID" class="dropdown_item_select">
												<option>Any</option>
												
											</select>
										</li>
										<li class="dropdown_item">
											<div class="dropdown_item_title">Property Status</div>
											<select name="property_status" id="property_status" class="dropdown_item_select">
												<option>Available</option>
												
											</select>
										</li>
										<li class="dropdown_item">
											<div class="dropdown_item_title">Location</div>
											<select name="property_location" id="property_location" class="dropdown_item_select" 
											onChange={this.handleChangeLocation}>
												<option>Any</option>
												<option>Beirut</option>
												<option>Bekaa</option>
											</select>
										</li>
										<li class="dropdown_item">
											<div class="dropdown_item_title">Property Type</div>
											<select name="property_type" id="property_type" class="dropdown_item_select" onChange={this.handleChangeType}>
												<option>Any</option>
												<option>Auction</option>
												<option>Direct</option>
											</select>
										</li>
										<li class="dropdown_item dropdown_item_half">
											<div class="dropdown_item_title">Rooms no</div>
											<select name="bedrooms_no" id="bedrooms_no" class="dropdown_item_select" onChange={this.handleChangeRoom}>
												<option>Any</option>
												<option>1</option>
												<option>2</option>
												<option>3</option>
												<option>4</option>
												<option>5</option>
											</select>
										</li>
										<li class="dropdown_item dropdown_item_half">
											<div class="dropdown_item_title">Bathrooms no</div>
											<select name="bathrooms_no" id="bathrooms_no" class="dropdown_item_select" onChange={this.handleChangeBathroom}>
												<option>Any</option>
												<option>1</option>
												<option>2</option>
												<option>3</option>
												<option>4</option>
												<option>5</option>
											</select>
										</li>
										<li class="dropdown_item dropdown_item_half">
											<div class="dropdown_item_title">Garage no</div>
											<select name="bathrooms_no" id="bathrooms_no" class="dropdown_item_select" onChange={this.handleChangeGarage}>
												<option>Any</option>
												<option>1</option>
												<option>2</option>
												<option>3</option>
												<option>4</option>
												<option>5</option>
											</select>
										</li>
										<li class="dropdown_item dropdown_item_half">
											<div class="dropdown_item_title">Property floor</div>
											<select name="bathrooms_no" id="bathrooms_no" class="dropdown_item_select" onChange={this.handleChangeFloor}>
												<option>Any</option>
												<option>1</option>
												<option>2</option>
												<option>3</option>
												<option>4</option>
												<option>5</option>
											</select>
										</li>
										
									
									</ul>
								</div>

								<div class="search_features_container">
									<div class="search_features_trigger">
										
									</div>

									<div class="search_button">
									<Link
                              to={{
                                pathname: "/search",
                                state: {
                                  estate_floor: this.state.estate_floor,
                                  estate_bathroom: this.state.estate_bathroom,
                                  estate_garage: this.state.estate_garage,
                                  estate_room: this.state.estate_room,
                                  estate_location: this.state.estate_location,
                                  estate_type:this.state.estate_type
                                }
                              }}
                            >
										<input value="search" type="submit" class="search_submit_button"/>
										</Link>
									</div>
								</div>
							</form>
      </div>
    
    );
  }
}

export default Filter;
