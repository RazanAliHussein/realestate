import React, { Component } from 'react';
//toast
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';



class Bidform extends Component {
 
  render() {
    return (
     
      <div className="Bidform">
      <ToastContainer/>
     <div class="footer_col_title">say hello</div>
					<div class="footer_contact_form_container">
						<div id="footer_contact_form" class="footer_contact_form" action="post" >
							<label id="contact_form_name" class="input_field contact_form_name" type="text" placeholder="Name" required="required" data-error="Name is required." >Current Bid</label>
							<label id="contact_form_email" class="input_field contact_form_email" type="email" placeholder="E-mail" required="required" data-error="Valid email is required.">$157,000</label>
							<input id="contact_form_message" class="text_field contact_form_message" name="message" placeholder="Your Bid" required="required" data-error="Please, write us a message." ></input>
							<button id="contact_send_btn" type="submit" class="contact_send_btn trans_200" value="Submit" >bid now</button>
						</div>
					</div>
      </div>
    
    );
  }
}

export default Bidform;
