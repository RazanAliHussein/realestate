import React, { Component } from 'react';

//Route
import {BrowserRouter as Router,Route,Link} from "react-router-dom";
import { withRouter } from "react-router";
//CSS


class Nav extends Component {
  onChange = (e) => {
    this.props.history.push(`/${e.target.value}`);
  }
  render() {
    return (
   
     
      <nav className="main_nav" >
      <ul className="main_nav_list">
        <li className="main_nav_item">
        <Link to="/" >home</Link>
         
        </li>
        <li className="main_nav_item">
        <Link to="/about">about us</Link>
         
        </li>
        <li className="main_nav_item">
        <select name="listing" className="main_nav_select" onChange={this.onChange}>
          <option className="main_nav_item main_nav_select_item" value="/" >Buy</option>
          <option className="main_nav_item main_nav_select_item" value="auction"> Live Auction</option>
         <option className="main_nav_item main_nav_select_item" value="list">Listings for sale</option>

        </select>
        </li>
        <li className="main_nav_item">
        <Link to="/sell">Sell</Link>
        </li>
        <li className="main_nav_item">
        <Link to="/news">news</Link>
          
        </li>
        <li className="main_nav_item">
        <Link to="/contact">contact</Link>
        </li>
       
      </ul>
    </nav>
   
    );
  }
}

export default withRouter(Nav);
