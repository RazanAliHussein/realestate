import React, { Component } from 'react';
//component
import Mainnav from './smallComponent/mainnav'
import Signin from './LoginButton'
//router
import {BrowserRouter as Router,Route,Link} from "react-router-dom";
//fa icon
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {faTwitter, faLinkedin, faGithub, faFacebook,faBehance,faDribbble} from '@fortawesome/fontawesome-free-brands';
class Contact extends Component {
	state={
		isTop: true,
    color:""
	}
	componentDidMount() {
    document.addEventListener('scroll', () => {
      const isTop = window.scrollY < 100;
      if (isTop !== this.state.isTop) {
          this.setState({ isTop })
          this.setState({color:"#0e1d41"})
      }
     
    });
  }
  render() {
    return (
      <div className="Contact">
      <div className="super_container">
	
	{/* Home  */}
	<div className="home">
		{/* Image by: https://unsplash.com/@breather  */}
		<div className="home_background"   style={ { backgroundImage: `url(${require('./images/contact.jpg')})` } }></div>
		
		<div className="container">
			<div className="row">
				<div className="col">
					<div className="home_content">
						<div className="home_title">
							<h2>single listings</h2>
						</div>
						<div className="breadcrumbs">
							<span><Link to="/">Home</Link></span>

							<span><Link to="/contact">Contact</Link></span>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	{/* Header  */}

	<header className="header trans_300" style={{backgroundColor:this.state.color}}>
		<div className="container">
			<div className="row">
				<div className="col">
					<div className="header_container d-flex flex-row align-items-center trans_300">

					 {/* Logo  */}

						<div className="logo_container">
							<a href="#">
								<div className="logo">
									<img src={require('./images/logo.png')} alt=""/>
									<span>mazad</span>
								</div>
							</a>
						</div>
						
						 {/* Main Navigation  */}

						<Mainnav/>
						{/* Phone Home  */}

					<Signin/>
						
						 {/* Hamburger  */}

						<div className="hamburger_container menu_mm">
							<div className="hamburger menu_mm">
								<i className="fas fa-bars trans_200 menu_mm"></i>
							</div>
						</div>

					</div>
				</div>
			</div>
		</div>

		 {/* Menu  */}

		<div className="menu menu_mm">
			<ul className="menu_list">
				<li className="menu_item">
					<div className="container">
						<div className="row">
							<div className="col">
								<a href="index.html">home</a>
							</div>
						</div>
					</div>
				</li>
				<li className="menu_item">
					<div className="container">
						<div className="row">
							<div className="col">
								<a href="about.html">about us</a>
							</div>
						</div>
					</div>
				</li>
				<li className="menu_item">
					<div className="container">
						<div className="row">
							<div className="col">
								<a href="listings.html">listings</a>
							</div>
						</div>
					</div>
				</li>
				<li className="menu_item">
					<div className="container">
						<div className="row">
							<div className="col">
								<a href="news.html">news</a>
							</div>
						</div>
					</div>
				</li>
				<li className="menu_item">
					<div className="container">
						<div className="row">
							<div className="col">
								<a href="#">contact</a>
							</div>
						</div>
					</div>
				</li>
			</ul>
		</div>

	</header>

	 {/* Contact  */}

	<div className="contact">
		<div className="container">
			<div className="row">
				
				<div className="col-lg-6 contact_col">
					<div className="estate_contact_form">
						<div className="contact_title">say hello</div>
						<div className="estate_contact_form_container">
							<form id="estate_contact_form" className="estate_contact_form" action="post">
								<input id="estate_contact_form_name" className="estate_input_field estate_contact_form_name" type="text" placeholder="Name" required="required" data-error="Name is required."/>
								<input id="estate_contact_form_email" className="estate_input_field estate_contact_form_email" type="email" placeholder="E-mail" required="required" data-error="Valid email is required."/>
								<input id="estate_contact_form_subject" className="estate_input_field estate_contact_form_subject" type="email" placeholder="Subject" required="required" data-error="Subject is required."/>
								<textarea id="estate_contact_form_message" className="estate_text_field estate_contact_form_message" name="message" placeholder="Message" required="required" data-error="Please, write us a message."></textarea>
								<button id="estate_contact_send_btn" type="submit" className="estate_contact_send_btn trans_200" value="Submit">send</button>
							</form>
						</div>
					</div>
				</div>

				<div className="col-lg-3 contact_col">
					<div className="contact_title">contact info</div>
					<ul className="contact_info_list estate_contact">
						<li className="contact_info_item d-flex flex-row">
							<div><div className="contact_info_icon"><img src={require('./images/placeholder.svg')} alt=""/></div></div>
							<div className="contact_info_text">4127 Raoul Wallenber 45b-c Gibraltar</div>
						</li>
						<li className="contact_info_item d-flex flex-row">
							<div><div className="contact_info_icon"><img src={require('./images/phone-call.svg')} alt=""/></div></div>
							<div className="contact_info_text">2556-808-8613</div>
						</li>
						<li className="contact_info_item d-flex flex-row">
							<div><div className="contact_info_icon"><img src={require('./images/message.svg')} alt=""/></div></div>
							<div className="contact_info_text"><a href="mailto:contactme@gmail.com?Subject=Hello" target="_top">mazadm009@gmail.com</a></div>
						</li>
						<li className="contact_info_item d-flex flex-row">
							<div><div className="contact_info_icon"><img src={require('./images/planet-earth.svg')} alt=""/></div></div>
							<div className="contact_info_text"><a href="https://colorlib.com">www.colorlib.com</a></div>
						</li>
					</ul>
					<div className="estate_social">
						<ul className="estate_social_list">
							<li className="estate_social_item"><a href="#"><i className="fab fa-pinterest"><FontAwesomeIcon className ='font-awesome' icon={faGithub} /></i></a></li>
							<li className="estate_social_item"><a href="#"><i className="fab fa-facebook-f"><FontAwesomeIcon className ='font-awesome' icon={faFacebook} /></i></a></li>
							<li className="estate_social_item"><a href="#"><i className="fab fa-twitter"><FontAwesomeIcon className ='font-awesome' icon={faTwitter} /></i></a></li>
							<li className="estate_social_item"><a href="#"><i className="fab fa-dribbble"><FontAwesomeIcon className ='font-awesome' icon={faDribbble} /></i></a></li>
							<li className="estate_social_item"><a href="#"><i className="fab fa-behance"><FontAwesomeIcon className ='font-awesome' icon={faBehance} /></i></a></li>
						</ul>
					</div>
				</div>

				<div className="col-lg-3 contact_col">
					<div className="contact_title">about</div>
					<div className="estate_about_text">
						<p>Lorem ipsum dolor sit amet, cons ectetur  quis ferme adipiscing elit. Suspen dis se tellus eros, placerat quis ferme ntum et, adipiscingvive rra sit ipsum amet lacus. </p>
						<p>Nam gravida quis placerat quis fe rme ntum et ferme sadipiscinge te llus semper augue.</p>
					</div>
				</div>

			</div>

		</div>

		 {/* Google Map  */}
		
		<div className="estate_map">
			<div id="google_map" className="google_map">
				<div className="map_container">
					<div id="map"><iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1696164.8008068155!2d34.726213568721214!3d33.86848112590474!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x151f17028422aaad%3A0xcc7d34096c00f970!2sLebanon!5e0!3m2!1sen!2slb!4v1549527135111" width="1200" height="450" frameborder="0" allowfullscreen></iframe></div>
				</div>
			</div>
		</div>

	</div>

	 {/* Newsletter  */}

	<div className="newsletter">
		<div className="container">
			<div className="row row-equal-height">

				<div className="col-lg-6">
					<div className="newsletter_title">
						<h3>subscribe to our newsletter</h3>
						<span className="newsletter_subtitle">Get the latest offers</span>
					</div>
					<div className="newsletter_form_container">
						<form action="#">
							<div className="newsletter_form_content d-flex flex-row">
								<input id="newsletter_email" className="newsletter_email" type="email" placeholder="Your email here" required="required" data-error="Valid email is required."/>
								<button id="newsletter_submit" type="submit" className="newsletter_submit_btn trans_200" value="Submit">subscribe</button>
							</div>
						</form>
					</div>
				</div>

				<div className="col-lg-6">
					<a href="#">
						<div className="weekly_offer">
							<div className="weekly_offer_content d-flex flex-row">
								<div className="weekly_offer_icon d-flex flex-column align-items-center justify-content-center">
									<img src={require('./images/prize.svg')} alt=""/>
								</div>
								<div className="weekly_offer_text text-center">weekly offer</div>
							</div>
							<div className="weekly_offer_image"   style={ { backgroundImage: `url(${require('./images/weekly.jpg')})` } }></div>
						</div>
					</a>
				</div>

			</div>
		</div>
	</div>

	 {/* Footer */}

	<footer className="footer">
		<div className="container">
			<div className="row">
				
				 {/* Footer About  */}

				<div className="col-lg-3 footer_col">
					<div className="footer_col_title">
						<div className="logo_container">
							<a href="#">
								<div className="logo">
									<img src={require('./images/logo.png')} alt=""/>
									<span>mazad</span>
								</div>
							</a>
						</div>
					</div>
					<div className="footer_social">
						<ul className="footer_social_list">
							<li className="footer_social_item"><a href="#"><i className="fab fa-pinterest"><FontAwesomeIcon className ='font-awesome' icon={faGithub} /></i></a></li>
							<li className="footer_social_item"><a href="#"><i className="fab fa-facebook-f"><FontAwesomeIcon className ='font-awesome' icon={faFacebook} /></i></a></li>
							<li className="footer_social_item"><a href="#"><i className="fab fa-twitter"><FontAwesomeIcon className ='font-awesome' icon={faTwitter} /></i></a></li>
							<li className="footer_social_item"><a href="#"><i className="fab fa-dribbble"><FontAwesomeIcon className ='font-awesome' icon={faDribbble} /></i></a></li>
							<li className="footer_social_item"><a href="#"><i className="fab fa-behance"><FontAwesomeIcon className ='font-awesome' icon={faBehance} /></i></a></li>
						</ul>
					</div>
					<div className="footer_about">
						<p>Lorem ipsum dolor sit amet, cons ectetur  quis ferme adipiscing elit. Suspen dis se tellus eros, placerat quis ferme ntum et, viverra sit amet lacus. Nam gravida  quis ferme semper augue.</p>
					</div>
				</div>
				
				{/* Footer Useful Links  */}

				<div className="col-lg-3 footer_col">
					<div className="footer_col_title">useful links</div>
					<ul className="footer_useful_links">
						<li className="useful_links_item"><a href="#">Listings</a></li>
						<li className="useful_links_item"><a href="#">Favorite Cities</a></li>
						<li className="useful_links_item"><a href="#">Clients Testimonials</a></li>
						<li className="useful_links_item"><a href="#">Featured Listings</a></li>
						<li className="useful_links_item"><a href="#">Properties on Offer</a></li>
						<li className="useful_links_item"><a href="#">Services</a></li>
						<li className="useful_links_item"><a href="#">News</a></li>
						<li className="useful_links_item"><a href="#">Our Agents</a></li>
					</ul>
				</div>

				{/* Footer Contact Form  */}
				<div className="col-lg-3 footer_col">
					<div className="footer_col_title">say hello</div>
					<div className="footer_contact_form_container">
						<form id="footer_contact_form" className="footer_contact_form" action="post">
							<input id="contact_form_name" className="input_field contact_form_name" type="text" placeholder="Name" required="required" data-error="Name is required."/>
							<input id="contact_form_email" className="input_field contact_form_email" type="email" placeholder="E-mail" required="required" data-error="Valid email is required."/>
							<textarea id="contact_form_message" className="text_field contact_form_message" name="message" placeholder="Message" required="required" data-error="Please, write us a message."></textarea>
							<button id="contact_send_btn" type="submit" className="contact_send_btn trans_200" value="Submit">send</button>
						</form>
					</div>
				</div>

				 {/* Footer Contact Info */}

				<div className="col-lg-3 footer_col">
					<div className="footer_col_title">contact info</div><input/>
					<ul className="contact_info_list">
						<li className="contact_info_item d-flex flex-row">
							<div><div className="contact_info_icon"><img src={require('./images/placeholder.svg')} alt=""/></div></div>
							<div className="contact_info_text">4127 Raoul Wallenber 45b-c Gibraltar</div>
						</li>
						<li className="contact_info_item d-flex flex-row">
							<div><div className="contact_info_icon"><img src={require('./images/phone-call.svg')} alt=""/></div></div>
							<div className="contact_info_text">2556-808-8613</div>
						</li>
						<li className="contact_info_item d-flex flex-row">
							<div><div className="contact_info_icon"><img src={require('./images/message.svg')}alt=""/></div></div>
							<div className="contact_info_text"><a href="mailto:contactme@gmail.com?Subject=Hello" target="_top">mazadm009@gmail.com</a></div>
						</li>
						<li className="contact_info_item d-flex flex-row">
							<div><div className="contact_info_icon"><img src={require('./images/planet-earth.svg')} alt=""/></div></div>
							<div className="contact_info_text"><a href="https://colorlib.com">www.colorlib.com</a></div>
						</li>
					</ul>
				</div>

			</div>
		</div>
	</footer>

	{/* //  Credits  */}

	

</div>

      </div>
    );
  }
}

export default Contact;
