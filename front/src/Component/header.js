import React, { Component } from 'react';

//Route
import {BrowserRouter as Router,Route,Link} from "react-router-dom";
//component
import Mainnav from './smallComponent/mainnav'

import Signin from './LoginButton'
import Image from './images/home_slider_bcg.jpg'
//CSS
let image='home_slider_bcg.jpg'
let styles = {
  
      backgroundImage: `url(${Image})`
   
  }
class Header extends Component {
  render() {
    return (
     
      <div className="Header">
       {/* Home  */}
       <div className="home">
            {/* Home Slider */}
            <div className="home_slider_container">
              <div className="owl-carousel owl-theme home_slider">
                {/* Home Slider Item  */}
                <div className="owl-item home_slider_item">
                  {/* Image by https://unsplash.com/@aahubs  */}
                  <div
                    className="home_slider_background"
                    //  style="background-image:url(images/home_slider_bcg.jpg)"
                    style={ styles}
                  />
                  <div className="home_slider_content_container text-center">
                    <div className="home_slider_content">
                      <h1
                        data-animation-in="flipInX"
                        data-animation-out="animate-out fadeOut"
                      >
                        find your home
                      </h1>
                    </div>
                  </div>
                </div>

                {/* Home Slider Item  */}
                <div className="owl-item home_slider_item">
                  {/* Image by https://unsplash.com/@aahubs  */}
                  <div
                    className="home_slider_background"
                    // style="background-image:url(images/home_slider_bcg.jpg)"
                    //style={ { backgroundImage: `url(${require('./images/home_slider_bcg.jpg')})` } }
                    style={ styles}
                  />
                  <div className="home_slider_content_container text-center">
                    <div className="home_slider_content">
                      <h1
                        data-animation-in="flipInX"
                        data-animation-out="animate-out fadeOut"
                      >
                        find your home
                      </h1>
                    </div>
                  </div>
                </div>

                {/* Home Slider Item  */}
                <div className="owl-item home_slider_item">
                  {/* Image by https://unsplash.com/@aahubs  */}
                  <div
                    className="home_slider_background"
                    // style="background-image:url(images/home_slider_bcg.jpg)"
                   // style={ { backgroundImage: `url(require('./images/home_slider_bcg.jpg'))` } }
                   style={ styles}
                  />
                  <div className="home_slider_content_container text-center">
                    <div className="home_slider_content">
                      <h1
                        data-animation-in="flipInX"
                        data-animation-out="animate-out fadeOut"
                      >
                        find your home
                      </h1>
                    </div>
                  </div>
                </div>
              </div>

              {/* Home Slider Nav */}
              <div className="home_slider_nav_left home_slider_nav d-flex flex-row align-items-center justify-content-end">
                <img src={require('./images/nav_left.png') }alt="" />
              </div>
            </div>
          </div>
          {/* // Header  */}
          <header className="header trans_300" id="navbars">
            <div className="container" >
              <div className="row">
                <div className="col">
                  <div className="header_container d-flex flex-row align-items-center trans_300">
                    {/* Logo  */}

                    <div className="logo_container">
                      <a href="#">
                        <div className="logo">
                          <img src={require('./images/logo.png')} alt="" />
                          <span>mazad</span>
                        </div>
                      </a>
                    </div>

                    {/* Main Navigation */}

                   <Mainnav/>

                    {/* Phone Home  */}

                    <Signin/>

                    {/* Hamburger  */}

                    <div className="hamburger_container menu_mm">
                      <div className="hamburger menu_mm">
                        <i className="fas fa-bars trans_200 menu_mm" />
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            {/* Menu */}

            <div className="menu menu_mm">
              <ul className="menu_list">
                <li className="menu_item">
                  <div className="container">
                    <div className="row">
                      <div className="col">
                        <a href="#">home</a>
                      </div>
                    </div>
                  </div>
                </li>
                <li className="menu_item">
                  <div className="container">
                    <div className="row">
                      <div className="col">
                        <a href="about.html">about us</a>
                      </div>
                    </div>
                  </div>
                </li>
                <li className="menu_item">
                  <div className="container">
                    <div className="row">
                      <div className="col">
                        <a href="listings.html">listings</a>
                      </div>
                    </div>
                  </div>
                </li>
                <li className="menu_item">
                  <div className="container">
                    <div className="row">
                      <div className="col">
                        <a href="news.html">news</a>
                      </div>
                    </div>
                  </div>
                </li>
                <li className="menu_item">
                  <div className="container">
                    <div className="row">
                      <div className="col">
                        <a href="contact.html">contact</a>
                      </div>
                    </div>
                  </div>
                </li>
              </ul>
            </div>
          </header>
      </div>
    
    );
  }
}

export default Header;
