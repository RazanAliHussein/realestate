import React, { Component } from "react";
//Components
import contactform from "../Component/smallComponent/contactform";
import Bidform from "../Component/smallComponent/bidform";
import Mainnav from "./smallComponent/mainnav";
import Signin from './LoginButton'
//Style
import { BrowserRouter as Router, Route, Link } from "react-router-dom";

import "./CSS/sell.css";

import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

//fa icon
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faTwitter,
  faLinkedin,
  faGithub,
  faFacebook,
  faBehance,
  faDribbble
} from "@fortawesome/fontawesome-free-brands";
import Contactform from "../Component/smallComponent/contactform";
class Sell extends Component {
  state = {
    name: "",
    phone: "",
    address: "",
    price: "",
    description: "",
    email: "",
    type: "direct",
    error_message: "",
   
  };
 
   ///Submit function
   sendEmail(name, email, message) {
    try{
      toast("your message was sent we will get back to you");
    fetch(`//localhost:8080/send`, {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json"
      },
      body: JSON.stringify({
        name: this.state.name,
        email: this.state.email,
        message: this.state.description
      })
    })
    
      .then(res => res.json())
      .then(res => {
        console.log("here is the response: ", res);
       
      })
      
      .catch(err => {
        console.error("here is the error: ", err);
      });
    }
    catch(err){
      toast.error(err.message);
    }
  }
  //submit function end
  createSell = async props => {
    const { name, phone, address, price, type, email, description } = props;
    const url = `http://localhost:8080/sell/new?name=${name}&phone=${phone}&address=${address}&price=${price}&type=${type}&email=${email}&description=${description}`;
    try {
      if (
        !props ||
        !(
          props.name &&
          props.phone &&
          props.address &&
          props.price &&
          props.type &&
          props.email &&
          props.description
        )
      ) {
        throw new Error(`you need all propertis`);
      }

      const response = await fetch(url);
      const answer = await response.json();
      if (answer.success) {
        // we reproduce the user that was created in the database, locally
        const id = answer.result;
        console.log(answer);
      } else {
        console.log("not create");
      }
    } catch (err) {
      this.setState({ error_message: err.message });
    }
  };
  handleChange = e => {
    this.setState({ type: e.target.value });
  };
  onSubmit = evt => {
    evt.preventDefault();
    // extract testimonial_description from state
    const {
      name,
      phone,
      address,
      price,
      type,
      email,
      description
    } = this.state;
    // create the blog
    this.createSell({ name, phone, address, price, type, email, description });
    this.sendEmail();
    // empty the text input fields are reset
    this.setState({
      name: "",
      phone: "",
      address: "",
      price: "",
      description: "",
      email: "",
      type: ""
    });
  };
  render() {
    return (
      <div className="Sell">
        <ToastContainer />
<div>
<header className="header trans_300"   style={ { backgroundImage: `url(${require('./images/home_background.jpg')})`} }>
            <div className="container">
              <div className="row">
                <div className="col">
                  <div className="header_container d-flex flex-row align-items-center trans_300">
                    {/* Logo  */}

                    <div className="logo_container">
                      <a href="#">
                        <div className="logo">
                          <img src={require("./images/logo.png")} alt="" />
                          <span>mazad</span>
                        </div>
                      </a>
                    </div>

                    {/* Main Navigation */}

                    <Mainnav/>

                    {/* Phone Home  */}

                    <Signin/>

                    {/* Hamburger  */}

                    <div className="hamburger_container menu_mm">
                      <div className="hamburger menu_mm">
                        <i className="fas fa-bars trans_200 menu_mm" />
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            {/* Menu  */}

            <div className="menu menu_mm">
              <ul className="menu_list">
                <li className="menu_item">
                  <div className="container">
                    <div className="row">
                      <div className="col">
                        <a href="index.html">home</a>
                      </div>
                    </div>
                  </div>
                </li>
                <li className="menu_item">
                  <div className="container">
                    <div className="row">
                      <div className="col">
                        <a href="#">about us</a>
                      </div>
                    </div>
                  </div>
                </li>
                <li className="menu_item">
                  <div className="container">
                    <div className="row">
                      <div className="col">
                        <a href="listings.html">listings</a>
                      </div>
                    </div>
                  </div>
                </li>
                <li className="menu_item">
                  <div className="container">
                    <div className="row">
                      <div className="col">
                        <a href="news.html">news</a>
                      </div>
                    </div>
                  </div>
                </li>
                <li className="menu_item">
                  <div className="container">
                    <div className="row">
                      <div className="col">
                        <a href="contact.html">contact</a>
                      </div>
                    </div>
                  </div>
                </li>
              </ul>
            </div>
          </header>
          </div>
          <br/>
          <br/>
          <br/>
          <br/>
          <br/>
          <br/>
          <br/>
          

        <div className="containers">
          <form onSubmit={this.onSubmit}>
            <div className="row">
              <div className="col-25">
                <label for="fname" className="selllabel">
                 
                  Name
                </label>
              </div>
              <div className="col-75">
                <input
                  type="text"
                  id="fname"
                  name="name"
                  onChange={evt => this.setState({ name: evt.target.value })}
                  placeholder="Your name.."
                />
              </div>
            </div>
            <div className="row">
              <div className="col-25">
                <label for="lname" className="selllabel">
                  Phone Number
                </label>
              </div>
              <div className="col-75">
                <input
                  type="text"
                  id="lname"
                  name="phone"
                  placeholder="Phone .."
                  onChange={evt => this.setState({ phone: evt.target.value })}
                />
              </div>
            </div>
            <div className="row">
              <div className="col-25">
                <label for="email" className="selllabel">
                  Email
                </label>
              </div>
              <div className="col-75">
                <input
                  type="text"
                  id="email"
                  name="email"
                  placeholder="Email.."
                  onChange={evt => this.setState({ email: evt.target.value })}
                />
              </div>
            </div>
            <div className="row">
              <div className="col-25">
                <label for="address" className="selllabel">
                  Address
                </label>
              </div>
              <div className="col-75">
                <input
                  type="text"
                  id="address"
                  name="address"
                  placeholder="Address.."
                  onChange={evt => this.setState({ address: evt.target.value })}
                />
              </div>
            </div>
            <div className="row">
              <div className="col-25">
                <label for="price" className="selllabel">
                  Price
                </label>
              </div>
              <div className="col-75">
                <input
                  type="text"
                  id="price"
                  name="price"
                  placeholder="Price.."
                  onChange={evt => this.setState({ price: evt.target.value })}
                />
              </div>
            </div>

            <div className="row">
              <div className="col-25">
                <label for="country" className="selllabel">
                  Type
                </label>
              </div>
              <div className="col-75">
                <select id="type" name="type" onChange={this.handleChange}>
                  <option value="direct">Direct</option>
                  <option value="auction">Auction</option>
                </select>
              </div>
            </div>
            <div className="row">
              <div className="col-25">
                <label for="subject">Description</label>
              </div>
              <div className="col-75">
                <textarea
                  id="subject"
                  name="description"
                  placeholder="Write something.."
                  style={{ height: "200px" }}
                  onChange={evt =>
                    this.setState({ description: evt.target.value })
                  }
                />
              </div>
            </div>
            <div className="row">
              <input className="sellsubmit" type="submit" value="Submit" />
            </div>
          </form>
        </div>
      </div>
    );
  }
}

export default Sell;
