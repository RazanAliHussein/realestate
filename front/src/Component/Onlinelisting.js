import React, { Component } from 'react';
//Components
import contactform from '../Component/smallComponent/contactform'
import Mainnav from './smallComponent/mainnav'
import Search from './smallComponent/filtration'
import Signin from './LoginButton'
//style
import {BrowserRouter as Router,Route,Link} from "react-router-dom";


//fa icon
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {faTwitter, faLinkedin, faGithub, faFacebook,faBehance,faDribbble} from '@fortawesome/fontawesome-free-brands';
import Contactform from '../Component/smallComponent/contactform';

const ListItem=( {estate_start_price,
	updatePage,
  id,
  estate_location,
  estate_description,
  estate_room,
  estate_bathroom,
	estate_height})=>{
	return(
		<div class="listing_item_inner d-flex flex-md-row flex-column trans_300">
		<div class="listing_image_container">
			<div class="listing_image">
				{/* <!-- Image by: https://unsplash.com/@breather --> */}
				<div class="listing_background" style={ { backgroundImage: `url(${require('./images/listing_1.jpg')})` } }></div>
			</div>
			<div class="featured_card_box d-flex flex-row align-items-center trans_300">
			
				<img src={require('./images/tag.svg')} alt="https://www.flaticon.com/authors/lucy-g"/>
				<div class="featured_card_box_content">
					<div class="featured_card_price_title trans_300">Start Bidding</div>
					<div class="featured_card_price trans_300">{estate_start_price}</div>
				</div>
			</div>
		</div>
		<div class="listing_content">
			<div class="listing_title"><Link to={`/auction/item/${id}`} onClick = {() => updatePage(id)}>House in {estate_location}</Link></div>
			<div class="listing_text">{estate_description}</div>
			<div class="rooms">

				<div class="room">
					<span class="room_title">Bedrooms</span>
					<div class="room_content">
						<div class="room_image"><img src={require('./images/bedroom.png')} alt=""/></div>
						<span class="room_number">{estate_room}</span>
					</div>
				</div>

				<div class="room">
					<span class="room_title">Bathrooms</span>
					<div class="room_content">
						<div class="room_image"><img src={require('./images/shower.png' )}alt=""/></div>
						<span class="room_number">{estate_bathroom}</span>
					</div>
				</div>

				<div class="room">
					<span class="room_title">Area</span>
					<div class="room_content">
						<div class="room_image"><img src={require('./images/area.png')} alt=""/></div>
						<span class="room_number">{estate_height} Sq Ft</span>
					</div>
				</div>

			</div>

			<div class="room_tags">
				<span class="room_tag"><a href="#">Hottub</a></span>
				<span class="room_tag"><a href="#">Swimming Pool</a></span>
				<span class="room_tag"><a href="#">Garden</a></span>
				<span class="room_tag"><a href="#">Patio</a></span>
				<span class="room_tag"><a href="#">Hard Wood Floor</a></span>
			</div>
		</div>
		</div>
	)
}
class Onlinelisting extends Component {
	state={
		auction_list:[],
		currentPage: 1,
		todosPerPage: 3,
		isTop: true,
    color:""
	}
	updatePage = (id) =>{
		this.props.updatePage("/auction/item" + id)
	}
	handleClick = event => {
    this.setState({
      currentPage: Number(event.target.id)
    });
  };

  handlePageChange(pageNumber) {
    console.log(`active page is ${pageNumber}`);
    this.setState({ activePage: pageNumber });
  }
	getAuctionList=async()=>{
		try{
			const response=await fetch('http://localhost:8080/auction/list')
			const data = await response.json();
			this.setState({ auction_list: data.result });
			
		}
		catch(err){
			console.log(err)
		}
	}
	componentDidMount(){
		this.getAuctionList();
		document.addEventListener('scroll', () => {
      const isTop = window.scrollY < 100;
      if (isTop !== this.state.isTop) {
          this.setState({ isTop })
          this.setState({color:"#0e1d41"})
      }
     
		});
	}
  render() {
		const { auction_list, currentPage, todosPerPage } = this.state;
    const indexOfLastTodo = currentPage * todosPerPage;
    const indexOfFirstTodo = indexOfLastTodo - todosPerPage;
    const currentTodos = auction_list.slice(indexOfFirstTodo, indexOfLastTodo);
    const renderTodos = currentTodos.map(estate => {
      return (
				<div className="listing_item">

          <ListItem
					key={estate.id}
					updatePage={this.updatePage}
          estate_start_price={estate.estate_start_price}
          id={estate.id}
            estate_location={estate.estate_location}
            estate_description={estate.estate_description}
            estate_room={estate.estate_room}
            estate_bathroom={estate.estate_bathroom}
            estate_height={estate.estate_height}
          />
        </div>
      );
    });
    const pageNumbers = [];
    for (let i = 1; i <= Math.ceil(auction_list.length / todosPerPage); i++) {
      pageNumbers.push(i);
    }

    const renderPageNumbers = pageNumbers.map(number => {
      return (
        <li key={number} id={number} onClick={this.handleClick}>
          {number}
        </li>
      );
    });
    return (
   
      <div className="Onlinelisting">
       <div class="super_container">
	
	{/* <!-- Home --> */}
	<div class="home">
		{/* <!-- Image by: https://unsplash.com/@jbriscoe --> */}
		
		<div class="home_background" style={ { backgroundImage: `url(${require('./images/listings.jpg')})` } }></div>
		
		<div class="container">
			<div class="row">
				<div class="col">
					<div class="home_content">
						<div class="home_title">
							<h2>listings</h2>
						</div>
						<div class="breadcrumbs">
							<span><Link to="/">Home</Link></span>
							<span><Link to="/list"> Search</Link></span>
							<span><Link to="/list"> Listings</Link></span>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	{/* <!-- Header --> */}

	<header class="header trans_300" style={{backgroundColor:this.state.color}} >
		<div class="container">
			<div class="row">
				<div class="col">
					<div class="header_container d-flex flex-row align-items-center trans_300">

						{/* <!-- Logo --> */}

						<div class="logo_container">
							<a href="#">
								<div class="logo">
									<img src={require('./images/logo.png')} alt=""/>
									<span>mazad</span>
								</div>
							</a>
						</div>
						
						{/* <!-- Main Navigation --> */}

					<Mainnav/>
						
						{/* <!-- Phone Home --> */}

						<Signin/>
						
						{/* <!-- Hamburger --> */}

						<div class="hamburger_container menu_mm">
							<div class="hamburger menu_mm">
								<i class="fas fa-bars trans_200 menu_mm"></i>
							</div>
						</div>

					</div>
				</div>
			</div>
		</div>

		{/* <!-- Menu --> */}

		<div class="menu menu_mm">
			<ul class="menu_list">
				<li class="menu_item">
					<div class="container">
						<div class="row">
							<div class="col">
							<Link to="/">home</Link>
								
							</div>
						</div>
					</div>
				</li>
				<li class="menu_item">
					<div class="container">
						<div class="row">
							<div class="col">
								<Link to="/about ">about us</Link>
							</div>
						</div>
					</div>
				</li>
				<li class="menu_item">
					<div class="container">
						<div class="row">
							<div class="col">
								<Link to="/list">listings</Link>
							</div>
						</div>
					</div>
				</li>
				<li class="menu_item">
					<div class="container">
						<div class="row">
							<div class="col">
								<Link to="/news">news</Link>
							</div>
						</div>
					</div>
				</li>
				<li class="menu_item">
					<div class="container">
						<div class="row">
							<div class="col">
								<Link to="/contact">contact</Link>
							</div>
						</div>
					</div>
				</li>
			</ul>
		</div>

	</header>

	{/* <!-- Listings --> */}

	<div class="listings">
		<div class="container">
			<div class="row">
				
				{/* <!-- Search Sidebar --> */}

				<div class="col-lg-4 sidebar_col">
					{/* <!-- Search Box --> */}

					<div class="search_box">

					<Search/>	
					
					</div>
				</div>

				{/* <!-- Listings --> */}

				<div class="col-lg-8 listings_col">

					{/* <!-- Listings Item --> */}
				
					{renderTodos}
						
					</div>
				
					{/* <!-- Listings Item --> */}

			
						

			</div>

			<div class="row">
				<div class="col clearfix">
					<div class="listings_nav">
						<ul>
						<li className="listings_nav_item active">
											{renderPageNumbers}
                      </li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div> 


	{/* <!-- Newsletter --> */}

	<div class="newsletter">
		<div class="container">
			<div class="row row-equal-height">

				<div class="col-lg-6">
					<div class="newsletter_title">
						<h3>subscribe to our newsletter</h3>
						<span class="newsletter_subtitle">Get the latest offers</span>
					</div>
					<div class="newsletter_form_container">
						<form action="#">
							<div class="newsletter_form_content d-flex flex-row">
								<input id="newsletter_email" class="newsletter_email" type="email" placeholder="Your email here" required="required" data-error="Valid email is required."/>
								<button id="newsletter_submit" type="submit" class="newsletter_submit_btn trans_200" value="Submit">subscribe</button>
							</div>
						</form>
					</div>
				</div>

				<div class="col-lg-6">
					<a href="#">
						<div class="weekly_offer">
							<div class="weekly_offer_content d-flex flex-row">
								<div class="weekly_offer_icon d-flex flex-column align-items-center justify-content-center">
									<img src={require('./images/prize.svg')} alt=""/>
								</div>
								<div class="weekly_offer_text text-center">weekly offer</div>
							</div>
							<div class="weekly_offer_image" style={ { backgroundImage: `url(${require('./images/weekly.jpg')})` } }></div>
						</div>
					</a>
				</div>

			</div>
		</div>
	</div>

	{/* <!-- Footer --> */}

	<footer class="footer">
		<div class="container">
			<div class="row">
				
				{/* <!-- Footer About --> */}

				<div class="col-lg-3 footer_col">
					<div class="footer_col_title">
						<div class="logo_container">
							<a href="#">
								<div class="logo">
									<img src={require('./images/logo.png')} alt=""/>
									<span>mazad</span>
								</div>
							</a>
						</div>
					</div>
					<div class="footer_social">
						<ul class="footer_social_list">
							<li class="footer_social_item"><a href="#"><i class="fab fa-pinterest"><FontAwesomeIcon className ='font-awesome' icon={faBehance} /></i></a></li>
							<li class="footer_social_item"><a href="#"><i class="fab fa-facebook-f"><FontAwesomeIcon className ='font-awesome' icon={faFacebook} /></i></a></li>
							<li class="footer_social_item"><a href="#"><i class="fab fa-twitter"><FontAwesomeIcon className ='font-awesome' icon={faTwitter} /></i></a></li>
							<li class="footer_social_item"><a href="#"><i class="fab fa-dribbble"><FontAwesomeIcon className ='font-awesome' icon={faDribbble} /></i></a></li>
							<li class="footer_social_item"><a href="#"><i class="fab fa-behance"><FontAwesomeIcon className ='font-awesome' icon={faBehance} /></i></a></li>
						</ul>
					</div>
					<div class="footer_about">
						<p>Lorem ipsum dolor sit amet, cons ectetur  quis ferme adipiscing elit. Suspen dis se tellus eros, placerat quis ferme ntum et, viverra sit amet lacus. Nam gravida  quis ferme semper augue.</p>
					</div>
				</div>
				
				{/* <!-- Footer Useful Links --> */}

				<div class="col-lg-3 footer_col">
					<div class="footer_col_title">useful links</div>
					<ul class="footer_useful_links">
						<li class="useful_links_item"><a href="#">Listings</a></li>
						<li class="useful_links_item"><a href="#">Favorite Cities</a></li>
						<li class="useful_links_item"><a href="#">Clients Testimonials</a></li>
						<li class="useful_links_item"><a href="#">Featured Listings</a></li>
						<li class="useful_links_item"><a href="#">Properties on Offer</a></li>
						<li class="useful_links_item"><a href="#">Services</a></li>
						<li class="useful_links_item"><a href="#">News</a></li>
						<li class="useful_links_item"><a href="#">Our Agents</a></li>
					</ul>
				</div>
{/* 
				<!-- Footer Contact Form --> */}
				<div class="col-lg-3 footer_col">
					<Contactform/>
				</div>

				{/* <!-- Footer Contact Info --> */}

				<div class="col-lg-3 footer_col">
					<div class="footer_col_title">contact info</div>
					<ul class="contact_info_list">
						<li class="contact_info_item d-flex flex-row">
							<div><div class="contact_info_icon"><img src={require('./images/placeholder.svg')} alt=""/></div></div>
							<div class="contact_info_text">4127 Raoul Wallenber 45b-c Gibraltar</div>
						</li>
						<li class="contact_info_item d-flex flex-row">
							<div><div class="contact_info_icon"><img src={require('./images/phone-call.svg')} alt=""/></div></div>
							<div class="contact_info_text">2556-808-8613</div>
						</li>
						<li class="contact_info_item d-flex flex-row">
							<div><div class="contact_info_icon"><img src={require('./images/message.svg')} alt=""/></div></div>
							<div class="contact_info_text"><a href="mailto:contactme@gmail.com?Subject=Hello" target="_top">mazadm009@gmail.com</a></div>
						</li>
						<li class="contact_info_item d-flex flex-row">
							<div><div class="contact_info_icon"><img src={require('./images/planet-earth.svg')} alt=""/></div></div>
							<div class="contact_info_text"><a href="https://colorlib.com">mazadm009@gmail.com</a></div>
						</li>
					</ul>
				</div>

			</div>
		</div>
	</footer>
    </div>
      </div>
     
    );
  }
}
export default Onlinelisting;

