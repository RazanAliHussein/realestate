import React, { Component } from 'react';
//component
import {BrowserRouter as Router,Route,Link} from "react-router-dom";
import Mainnav from './smallComponent/mainnav'
import Contactform from '../Component/smallComponent/contactform';
import Signin from './LoginButton'
//fa icon
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {faTwitter, faLinkedin, faGithub, faFacebook,faBehance,faDribbble} from '@fortawesome/fontawesome-free-brands';

class News extends Component {
	state={
		blog_list:[],
		isTop: true,
		color:""
	}
	componentDidMount(){
		this.getList();
		document.addEventListener('scroll', () => {
			const isTop = window.scrollY < 100;
			if (isTop !== this.state.isTop) {
				this.setState({ isTop })
				this.setState({color:"#0e1d41"})
			}
		   
		  });
	}
	getList = async () => {
		try{
		const response = await fetch("http://localhost:8080/blog/list");
		const data = await response.json();
		this.setState({ blog_list: data.result });
		}
		catch(err){
			console.log(err)
		}
	  };
  render() {
	  console.log(this.state.blog_list)
    return (
      <div className="News">
     <div class="super_container">
	
	{/* <!-- Home --> */}
	<div class="home">
		{/* <!-- Image by: https://unsplash.com/@jbriscoe --> */}
		<div class="home_background"  style={ { backgroundImage: `url(${require('./images/news.jpg')})` } } ></div>
		
		<div class="container">
			<div class="row">
				<div class="col">
					<div class="home_content">
						<div class="home_title">
							<h2>listings</h2>
						</div>
						<div class="breadcrumbs">
							<span><Link to="/">Home</Link></span>
							<span><Link to="/news"> News</Link></span>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	{/* <!-- Header --> */}

	<header class="header trans_300" style={{backgroundColor:this.state.color}}>
		<div class="container">
			<div class="row">
				<div class="col">
					<div class="header_container d-flex flex-row align-items-center trans_300">

						{/* <!-- Logo --> */}

						<div class="logo_container">
							<a href="#">
								<div class="logo">
									<img src={require('./images/logo.png')}alt=""/>
									<span>mazad</span>
								</div>
							</a>
						</div>
						
						{/* <!-- Main Navigation --> */}
<Mainnav/>
						
						{/* <!-- Phone Home --> */}

						<Signin/>
						
						{/* <!-- Hamburger --> */}

						<div class="hamburger_container menu_mm">
							<div class="hamburger menu_mm">
								<i class="fas fa-bars trans_200 menu_mm"></i>
							</div>
						</div>

					</div>
				</div>
			</div>
		</div>

		{/* <!-- Menu --> */}

		<div class="menu menu_mm">
			<ul class="menu_list">
				<li class="menu_item">
					<div class="container">
						<div class="row">
							<div class="col">
								<Link to="/">home</Link>
							</div>
						</div>
					</div>
				</li>
				<li class="menu_item">
					<div class="container">
						<div class="row">
							<div class="col">
								<Link to="/">>about us</Link>
							</div>
						</div>
					</div>
				</li>
				<li class="menu_item">
					<div class="container">
						<div class="row">
							<div class="col">
								<Link to="/list">>listings</Link>
							</div>
						</div>
					</div>
				</li>
				<li class="menu_item">
					<div class="container">
						<div class="row">
							<div class="col">
								<Link to="/news">>news</Link>
							</div>
						</div>
					</div>
				</li>
				<li class="menu_item">
					<div class="container">
						<div class="row">
							<div class="col">
								<Link to="/contact">>contact</Link>
							</div>
						</div>
					</div>
				</li>
			</ul>
		</div>

	</header>

	{/* <!-- News --> */}

	<div class="news">
		<div class="container">
			<div class="row">

				<div class="col-lg-8 news_content_col">
					
					{/* <!-- News Post --> */}
					{this.state.blog_list.map(blog =>
						(
					<div class="news_post" key={blog.blog_id}>
						<div class="news_post_date">{blog.blog_date}</div>
						<div class="news_post_title"><a href="#">{blog.blog_title}</a></div>
						<div class="news_post_meta">
							<ul>
								<li class="news_post_meta_item">
									<div class="news_post_meta_icon">
										<img src={require('./images/profile.svg')} alt="https://www.flaticon.com/authors/lucy-g"/>
									</div>
									<span><a href="#">By Lore Papp</a></span>
								</li>
								<li class="news_post_meta_item">
									<div class="news_post_meta_icon">
										<img src={require('./images/favorite.svg')} alt="https://www.flaticon.com/authors/lucy-g"/>
									</div>
									<span><a href="#">in Uncategorized</a></span>
								</li>
								<li class="news_post_meta_item">
									<div class="news_post_meta_icon">
										<img src={require('./images/speech-bubble.svg')} alt="https://www.flaticon.com/authors/lucy-g"/>
									</div>
									<span><a href="#">3 Comments</a></span>
								</li>
							</ul>
						</div>
						<div class="news_post_image">
							<img src={require('./images/news_1.jpg')} alt="https://unsplash.com/@alcasqui"/>
						</div>
						<div class="news_post_text">
							<p>{blog.blog_description} </p>
						</div>
						<div class="button news_post_button"><a href="#">read more</a></div>
					</div>
					))}
			
			</div>
				<div class="col-lg-4 news_sidebar_col">
					<div class="news_sidebar">
						<div class="sidebar_search">
							<form action="post">
								<div id="search_form" class="search_form">
									<input id="news_search" class="news_search" type="search" placeholder="Search" required="required" data-error="Valid keywords required."/>
									<button id="search_submit_btn" type="submit" class="search_submit_btn trans_300" value="Submit">
										<img src={require('./images/search.png')}alt=""/>
									</button>
								</div>
							</form>
						</div>

						{/* <!-- Archives --> */}
						<div class="sidebar_section">
							<div class="sidebar_title">archives</div>
							<ul class="sidebar_list">
								<li class="sidebar_list_item"><a href="#">March 2017</a></li>
								<li class="sidebar_list_item"><a href="#">April 2017</a></li>
								<li class="sidebar_list_item"><a href="#">May 2017</a></li>
								<li class="sidebar_list_item"><a href="#">June 2017</a></li>
							</ul>
						</div>

						{/* <!-- Categories --> */}
						<div class="sidebar_section">
							<div class="sidebar_title">categories</div>
							<ul class="sidebar_list">
								<li class="sidebar_list_item"><a href="#">Uncategorized</a></li>
								<li class="sidebar_list_item"><a href="#">Useful Information</a></li>
								<li class="sidebar_list_item"><a href="#">Events</a></li>
								<li class="sidebar_list_item"><a href="#">Real Estate Tips</a></li>
								<li class="sidebar_list_item"><a href="#">Home Owners</a></li>
								<li class="sidebar_list_item"><a href="#">Vacation Homes</a></li>
							</ul>
						</div>
						
						{/* <!-- Latest Posts --> */}
						<div class="sidebar_section">
							<div class="sidebar_title">latest posts</div>
							<div class="latest_posts_container">

								{/* <!-- Latest Post --> */}
								<div class="latest_post">
									<div class="latest_post_image"><img src={require('./images/latest_1.jpg')}alt=""/></div>
									<div class="latest_post_content">
										<div class="latest_post_title"><a href="#">A simple blog post</a></div>
										<div class="latest_post_meta">
											<span><a href="#">by Jane Smith</a> / Aug 25, 2016</span>
										</div>
									</div>
								</div>

								{/* <!-- Latest Post --> */}
								<div class="latest_post">
									<div class="latest_post_image"><img src={require('./images/latest_2.jpg')} alt=""/></div>
									<div class="latest_post_content">
										<div class="latest_post_title"><a href="#">Dream destination for you</a></div>
										<div class="latest_post_meta">
											<span><a href="#">by Jane Smith</a> / Aug 25, 2016</span>
										</div>
									</div>
								</div>

								{/* <!-- Latest Post --> */}
								<div class="latest_post">
									<div class="latest_post_image"><img src={require('./images/latest_3.jpg')}alt=""/></div>
									<div class="latest_post_content">
										<div class="latest_post_title"><a href="#">Tips to travel light</a></div>
										<div class="latest_post_meta">
											<span><a href="#">by Jane Smith</a> / Aug 25, 2016</span>
										</div>
									</div>
								</div>
{/* 
								<!-- Latest Post --> */}
								<div class="latest_post">
									<div class="latest_post_image"><img src={require('./images/latest_4.jpg')}alt=""/></div>
									<div class="latest_post_content">
										<div class="latest_post_title"><a href="#">How to pick your vacation</a></div>
										<div class="latest_post_meta">
											<span><a href="#">by Jane Smith</a> / Aug 25, 2016</span>
										</div>
									</div>
								</div>

							</div>
						</div>
						
						{/* <!-- Instagram --> */}
						<div class="sidebar_section">
							<div class="sidebar_title">instagram</div>
							<div class="gallery_container">
								<ul class="gallery_items d-flex flex-row align-items-start justify-content-between flex-wrap">
									<li class="gallery_item">
										<a class="colorbox" href="https://images.unsplash.com/photo-1503174971373-b1f69850bded?auto=format&fit=crop&w=720&q=80">
											<img src={require('./images/gallery_1.jpg')} alt="https://unsplash.com/@alcasqui"/>
										</a>
									</li>
									<li class="gallery_item">
										<a class="colorbox" href="https://images.unsplash.com/photo-1448630360428-65456885c650?auto=format&fit=crop&w=720&q=80">
											<img src={require('./images/gallery_2.jpg')} alt="https://unsplash.com/@etnbr"/>
										</a>
									</li>
									<li class="gallery_item">
										<a class="colorbox" href="https://images.unsplash.com/photo-1502005229762-cf1b2da7c5d6?auto=format&fit=crop&w=634&q=80">
											<img src={require('./images/gallery_3.jpg')} alt="https://unsplash.com/@jbriscoe"/>
										</a>
									</li>
									<li class="gallery_item">
										<a class="colorbox" href="https://images.unsplash.com/photo-1464375573282-035539096568?auto=format&fit=crop&w=720&q=80">
											<img src={require('./images/gallery_4.jpg')} alt="https://unsplash.com/@marcusneto"/>
										</a>
									</li>
									<li class="gallery_item">
										<a class="colorbox" href="https://images.unsplash.com/photo-1449844908441-8829872d2607?auto=format&fit=crop&w=720&q=80">
											<img src={require('./images/gallery_5.jpg')} alt="https://unsplash.com/@insolitus"/>
										</a>
									</li>
									<li class="gallery_item">
										<a class="colorbox" href="https://images.unsplash.com/photo-1502672260266-1c1ef2d93688?auto=format&fit=crop&w=720&q=80">
											<img src={require('./images/gallery_6.jpg')} alt="https://unsplash.com/@pperkins"/>
										</a>
									</li>
								</ul>
							</div>
						</div>

						{/* <!-- Hello --> */}

						<div class="sidebar_section">
							<div class="hello">
								<div class="footer_col_title">say hello</div>
								<div class="footer_contact_form_container">
									<form id="hello_contact_form" class="footer_contact_form" action="post">
										<input id="hello_contact_form_name" class="input_field contact_form_name" type="text" placeholder="Name" required="required" data-error="Name is required."/>
										<input id="hello_contact_form_email" class="input_field contact_form_email" type="email" placeholder="E-mail" required="required" data-error="Valid email is required."/>
										<textarea id="hello_contact_form_message" class="text_field contact_form_message" name="message" placeholder="Message" required="required" data-error="Please, write us a message."></textarea>
										<button id="hello_contact_send_btn" type="submit" class="contact_send_btn trans_200" value="Submit">send</button>
									</form>
								</div>
							</div>
						</div>
						
					</div>
				</div>

			</div>
		</div>
	</div>

	{/* <!-- Newsletter --> */}

	<div class="newsletter">
		<div class="container">
			<div class="row row-equal-height">

				<div class="col-lg-6">
					<div class="newsletter_title">
						<h3>subscribe to our newsletter</h3>
						<span class="newsletter_subtitle">Get the latest offers</span>
					</div>
					<div class="newsletter_form_container">
						<form action="#">
							<div class="newsletter_form_content d-flex flex-row">
								<input id="newsletter_email" class="newsletter_email" type="email" placeholder="Your email here" required="required" data-error="Valid email is required."/>
								<button id="newsletter_submit" type="submit" class="newsletter_submit_btn trans_200" value="Submit">subscribe</button>
							</div>
						</form>
					</div>
				</div>

				<div class="col-lg-6">
					<a href="#">
						<div class="weekly_offer">
							<div class="weekly_offer_content d-flex flex-row">
								<div class="weekly_offer_icon d-flex flex-column align-items-center justify-content-center">
									<img src={require('./images/prize.svg')} alt=""/>
								</div>
								<div class="weekly_offer_text text-center">weekly offer</div>
							</div>
							<div class="weekly_offer_image"  style={ { backgroundImage: `url(${require('./images/weekly.jpg')})` } }></div>
						</div>
					</a>
				</div>

			</div>
		</div>
	</div>

	{/* <!-- Footer --> */}

	<footer class="footer">
		<div class="container">
			<div class="row">
				
				{/* <!-- Footer About --> */}

				<div class="col-lg-3 footer_col">
					<div class="footer_col_title">
						<div class="logo_container">
							<a href="#">
								<div class="logo">
									<img src={require('./images/logo.png')} alt=""/>
									<span>mazad</span>
								</div>
							</a>
						</div>
					</div>
					<div class="footer_social">
						<ul class="footer_social_list">
							<li class="footer_social_item"><a href="#"><i class="fab fa-pinterest"><FontAwesomeIcon className ='font-awesome' icon={faBehance} /></i></a></li>
							<li class="footer_social_item"><a href="#"><i class="fab fa-facebook-f"><FontAwesomeIcon className ='font-awesome' icon={faFacebook} /></i></a></li>
							<li class="footer_social_item"><a href="#"><i class="fab fa-twitter"><FontAwesomeIcon className ='font-awesome' icon={faTwitter} /></i></a></li>
							<li class="footer_social_item"><a href="#"><i class="fab fa-dribbble"><FontAwesomeIcon className ='font-awesome' icon={faGithub} /></i></a></li>
							<li class="footer_social_item"><a href="#"><i class="fab fa-dribbble"><FontAwesomeIcon className ='font-awesome' icon={faTwitter} /></i></a></li>
							<li class="footer_social_item"><a href="#"><i class="fab fa-dribbble"><FontAwesomeIcon className ='font-awesome' icon={faBehance} /></i></a></li>
							<li class="footer_social_item"><a href="#"><i class="fab fa-behance"><FontAwesomeIcon className ='font-awesome' icon={faTwitter} /></i></a></li>
						</ul>
					</div>
					<div class="footer_about">
						<p>Lorem ipsum dolor sit amet, cons ectetur  quis ferme adipiscing elit. Suspen dis se tellus eros, placerat quis ferme ntum et, viverra sit amet lacus. Nam gravida  quis ferme semper augue.</p>
					</div>
				</div>
				
				{/* <!-- Footer Useful Links --> */}

				<div class="col-lg-3 footer_col">
					<div class="footer_col_title">useful links</div>
					<ul class="footer_useful_links">
						<li class="useful_links_item"><a href="#">Listings</a></li>
						<li class="useful_links_item"><a href="#">Favorite Cities</a></li>
						<li class="useful_links_item"><a href="#">Clients Testimonials</a></li>
						<li class="useful_links_item"><a href="#">Featured Listings</a></li>
						<li class="useful_links_item"><a href="#">Properties on Offer</a></li>
						<li class="useful_links_item"><a href="#">Services</a></li>
						<li class="useful_links_item"><a href="#">News</a></li>
						<li class="useful_links_item"><a href="#">Our Agents</a></li>
					</ul>
				</div>

				{/* <!-- Footer Contact Form --> */}
				<div class="col-lg-3 footer_col">
					<Contactform/>
				</div>

				{/* <!-- Footer Contact Info --> */}

				<div class="col-lg-3 footer_col">
					<div class="footer_col_title">contact info</div>
					<ul class="contact_info_list">
						<li class="contact_info_item d-flex flex-row">
							<div><div class="contact_info_icon"><img src={require('./images/placeholder.svg')} alt=""/></div></div>
							<div class="contact_info_text">4127 Raoul Wallenber 45b-c Gibraltar</div>
						</li>
						<li class="contact_info_item d-flex flex-row">
							<div><div class="contact_info_icon"><img src={require('./images/phone-call.svg')} alt=""/></div></div>
							<div class="contact_info_text">2556-808-8613</div>
						</li>
						<li class="contact_info_item d-flex flex-row">
							<div><div class="contact_info_icon"><img src={require('./images/message.svg')} alt=""/></div></div>
							<div class="contact_info_text"><a href="mailto:contactme@gmail.com?Subject=Hello" target="_top">mazadm009@gmail.com</a></div>
						</li>
						<li class="contact_info_item d-flex flex-row">
							<div><div class="contact_info_icon"><img src={require('./images/planet-earth.svg')} alt=""/></div></div>
							<div class="contact_info_text"><a href="https://colorlib.com">www.colorlib.com</a></div>
						</li>
					</ul>
				</div>

			</div>
		</div>
	</footer>
      </div>
   </div>
    );
  }
}

export default News;
