import React, { Component } from "react";

import "../CSS/dashboard.css";
import ListEstateEdit from "./listEstateEdit";
class EstateList extends Component {
    
  state = {
    estate_list: [],
    error_message: "",
    estate_available: "",
      estate_price: "",
      estate_garage: "",
      estate_room: "",
      estate_bathroom: "",
      estate_height: "",
      estate_width: "",
      estate_video: "",
      estate_floor: "",
      estate_location: "",
      estate_description: "",
      estate_date: ""
      
  };

  getListingEstate = async id => {
    // check if we already have the blog
    const previous_estate = this.state.estate_list.find(
      estate => estate.id === id
    );
    if (previous_estate) {
      return; // do nothing, no need to reload a blog we already have
    }
    try {
      const response = await fetch(`http://localhost:8080/estate/get/${id}`);
      const answer = await response.json();
      if (answer.success) {
        // add the user to the current list of blog
        const estate = answer.result;
        const estate_list = [...this.state.estate_list, estate];
        this.setState({ estate_list });
      } else {
        this.setState({ error_message: answer.message });
      }
    } catch (err) {
      this.setState({ error_message: err.message });
    }
  };
  deleteListingEstate = async id => {
    try {
      const response = await fetch(
        `http://localhost:8080/estate/delete/${id}`
      );
      const answer = await response.json();
      if (answer.success) {
        // remove the user from the current list of users
        const estate_list = this.state.estate_list.filter(
          estate => estate.id !== id
        );
        this.setState({ estate_list });
      } else {
        this.setState({ error_message: answer.message });
      }
    } catch (err) {
      this.setState({ error_message: err.message });
    }
  };
  createListingEstate = async props => {
    const {
      estate_available,
      estate_price,
      estate_garage,
      estate_room,
      estate_bathroom,
      estate_height,
      estate_width,
      estate_video,
      estate_floor,
      estate_location,
      estate_description,
      estate_date
     
    } = props;
    console.log("here")
    const url = `http://localhost:8080/estate/new?estate_available=${estate_available}&estate_price=${estate_price}&estate_garage=${estate_garage}&estate_room=${estate_room}&estate_bathroom=${estate_bathroom}&estate_height=${estate_height}&estate_width=${estate_width}&estate_video=${estate_video}&estate_floor=${estate_floor}&estate_location=${estate_location}&estate_description=${estate_description}&estate_date=${estate_date}`;
    try {
      if (
        !props ||
        !(
          props.estate_available &&
          props.estate_price &&
          props.estate_garage &&
          props.estate_room &&
          props.estate_bathroom &&
          props.estate_height &&
          props.estate_width &&
          props.estate_video &&
          props.estate_floor &&
          props.estate_location &&
          props.estate_description &&
          props.estate_date 
        )
      ) 
      {
        throw new Error(`you need all propertis`);
      }

      const response = await fetch(url);
      const answer = await response.json();
      if (answer.success) {
        // we reproduce the user that was created in the database, locally
        const id = answer.result;
        const estate = {
          estate_available,
          estate_price,
          estate_garage,
          estate_room,
          estate_bathroom,
          estate_height,
          estate_width,
          estate_video,
          estate_floor,
          estate_location,
          estate_description,
          estate_date,
          
          id
        };
        const estate_list = [...this.state.estate_list, estate];
        this.setState({ estate_list });
      } else {
        this.setState({ error_message: answer.message });
      }
    } catch (err) {
      this.setState({ error_message: err.message });
    }
  };
  getListingEstateList = async order => {
    try {
      const response = await fetch(
        `http://localhost:8080/estate/list?order=${order}`
      );
      const answer = await response.json();
      if (answer.success) {
        const estate_list = answer.result;
        this.setState({ estate_list });
      } else {
        this.setState({ error_message: answer.message });
      }
    } catch (err) {
      this.setState({ error_message: err.message });
    }
  };
  updateListingEstate = async (id, props) => {
    try {
      if (
        !props ||
        !(
          props.estate_available ||
          props.estate_price ||
          props.estate_garage ||
          props.estate_room ||
          props.estate_bathroom ||
          props.estate_height ||
          props.estate_width ||
          props.estate_video||
          props.estate_floor ||
          props.estate_location ||
          props.estate_description ||
          props.estate_date 
         
        )
      ) {
        throw new Error(`you need at least one property`);
      }
      const response = await fetch(
        `http://localhost:8080/estate/update/${id}?estate_available=${props.estate_available}&estate_price=${props.estate_price}&estate_garage=${props.estate_garage}&estate_room=${props.estate_room}&estate_bathroom=${props.estate_bathroom}&estate_height=${props.estate_height}&estate_width=${props.estate_width}&estate_video=${props.estate_video}&estate_floor=${props.estate_floor}&estate_location=${props.estate_location}&estate_description=${props.estate_description}&estate_date=${props.estate_date}`
      );
      const answer = await response.json();
      if (answer) {
        // we update the user, to reproduce the database changes:
        const estate_list = this.state.estate_list;
        estate_list.map(estate => {
          // if this is the member we need to change, update it. This will apply to exactly
          // one member
          if (estate.id === id) {
            const new_estate= {
              id: estate.id,
              estate_available: props.estate_available ,
              estate_price: props.estate_price ,
              estate_garage:props.estate_garage ,
              estate_room:props.estate_room ,
              estate_bathroom:props.estate_bathroom ,
              estate_height: props.estate_height ,
              estate_width:props.estate_width ,
              estate_video:props.estate_video,
              estate_floor:props.estate_floor ,
              estate_location:props.estate_location ,
              estate_description: props.estate_description ,
              estate_date:props.estate_date 
              
            };

            return new_estate;
          }
          // otherwise, don't change the members at all
          else {
            return estate;
          }
        });
        this.setState({ estate_list });
        //toast("updated");
      } else {
        this.setState({ error_message: answer.message });
      }
    } catch (err) {
      this.setState({ error_message: err.message });
    }
  };

  componentDidMount() {
    this.getListingEstateList();
  }
  onSubmit = evt => {
    evt.preventDefault();
    // extract  blog_title,blog_description,blog_date from state
    const {
      estate_available,
      estate_price,
      estate_garage,
      estate_room,
      estate_bathroom,
      estate_height,
      estate_width,
      estate_video,
      estate_floor,
      estate_location,
      estate_description,
      estate_date
      
    } = this.state;
    // create the blog
    this.createListingEstate({
      estate_available,
      estate_price,
      estate_garage,
      estate_room,
      estate_bathroom,
      estate_height,
      estate_width,
      estate_video,
      estate_floor,
      estate_location,
      estate_description,
      estate_date
     
    });
    // empty the text input fields are reset
    this.setState({
      estate_available: "",
      estate_price: "",
      estate_garage: "",
      estate_room: "",
      estate_bathroom: "",
      estate_height: "",
      estate_width: "",
      estate_floor: "",
      estate_location: "",
      estate_description: "",
      estate_date: "",
      estate_video: ""
    });
  };

  render() {
    const { estate_list } = this.state;

    return (
      <div className="blog grid-container">
        <div className="item3">
          <h1>The List of Estate :</h1>
          {estate_list.map(estate => (
            <ListEstateEdit
              key={estate.id}
              id={estate.id}
              estate_available={estate.estate_available}
              estate_price={estate.estate_price}
              estate_garage={estate.estate_garage}
              estate_room={estate.estate_room}
              estate_bathroom={estate.estate_bathroom}
              estate_height={estate.estate_height}
              estate_width={estate.estate_width}
              estate_floor={estate.estate_floor}
              estate_location={estate.estate_location}
              estate_description={estate.estate_description}
              estate_date={estate.estate_date}
              estate_video={estate.estate_video}
              updateListingEstate={this.updateListingEstate}
              deleteListingEstate={this.deleteListingEstate}
            />
          ))}
        </div>
        <form className="third item4" onSubmit={this.onSubmit}>
          <label>Insert new estate List:     
       </label>
          <br />
          <input
            type="text"
            placeholder="Available"
            onChange={evt => this.setState({ estate_available: evt.target.value })}
            value={this.state.estate_available}
          />
          <input
            type="text"
            placeholder="price"
            onChange={evt => this.setState({ estate_price: evt.target.value })}
            value={this.state.estate_price}
          />
          <input
            type="text"
            placeholder="garage"
            onChange={evt =>
              this.setState({ estate_garage: evt.target.value })
            }
            value={this.state.estate_garage}
          />
            <input
            type="text"
            placeholder="Room"
            onChange={evt =>
              this.setState({ estate_room: evt.target.value })
            }
            value={this.state.estate_room}
          />
            <input
            type="text"
            placeholder="Bath room"
            onChange={evt =>
              this.setState({ estate_bathroom: evt.target.value })
            }
            value={this.state.estate_bathroom}
          />
           <input
            type="text"
            placeholder="Height"
            onChange={evt =>
              this.setState({ estate_height: evt.target.value })
            }
            value={this.state.estate_height}
          />
           <input
            type="text"
            placeholder="Width"
            onChange={evt =>
              this.setState({ estate_width: evt.target.value })
            }
            value={this.state.estate_width}
          />
           <input
            type="text"
            placeholder="Floor"
            onChange={evt =>
              this.setState({estate_floor: evt.target.value })
            }
            value={this.state.estate_floor}
          />
           <input
            type="text"
            placeholder="location"
            onChange={evt =>
              this.setState({ estate_location: evt.target.value })
            }
            value={this.state.estate_location}
          />
             <input
            type="text"
            placeholder="description"
            onChange={evt =>
              this.setState({estate_description: evt.target.value })
            }
            value={this.state.estate_description}
          />
           <input
            type="text"
            placeholder="date"
            onChange={evt =>
              this.setState({estate_date: evt.target.value })
            }
            value={this.state.estate_date}
          />
           <input
            type="text"
            placeholder="video"
            onChange={evt =>
              this.setState({estate_video: evt.target.value })
            }
            value={this.state.estate_video}
          />
          <div>
            <input type="submit" value="ok" />
            <input type="reset" value="cancel" className="button" />
          </div>
        </form>
      </div>
    );
  }
}
export default EstateList;
