import React, { Component } from 'react';
import BlogEdit from './BlogEdit';
import '../CSS/dashboard.css'
class Blog extends Component {
    state={
        blog_list:[],
        blog_date:"",
        blog_description:"",
        id:"",
        blog_title:"",
        error_message:""
	}
	
	getList = async () => {
		try{
		const response = await fetch("http://localhost:8080/blog/list");
		const data = await response.json();
		this.setState({ blog_list: data.result });
		}
		catch(err){
			console.log(err)
		}
      };
      getBlog = async id => {
        // check if we already have the blog
        const previous_blog = this.state.blog_list.find(
          blog => blog.id === id
        );
        if (previous_blog) {
          return; // do nothing, no need to reload a blog we already have
        }
        try {
          const response = await fetch(`http://localhost:8080/blog/get/${id}`);
          const answer = await response.json();
          if (answer.success) {
            // add the user to the current list of blog
            const blog = answer.result;
            const blog_list = [...this.state.blog_list, blog];
            this.setState({ blog_list });
          } else {
            this.setState({ error_message: answer.message });
          }
        } catch (err) {
          this.setState({ error_message: err.message });
        }
      };
      deleteBlog = async id => {
        try {
          const response = await fetch(
            `http://localhost:8080/blog/delete/${id}`
          );
          const answer = await response.json();
          if (answer.success) {
            // remove the user from the current list of users
            const blog_list = this.state.blog_list.filter(
              blog => blog.id !== id
            );
            this.setState({ blog_list });
          } else {
            this.setState({ error_message: answer.message });
          }
        } catch (err) {
          this.setState({ error_message: err.message });
        }
      };
      createBlog = async props => {
          const {  blog_title,blog_description,blog_date } = props;
          const url= `http://localhost:8080/blog/new?blog_title=${blog_title}&blog_description=${blog_description}&blog_date=${blog_date}`;
          try {
          if (!props || !(props.blog_title && props.blog_description && props.blog_date)) {
            throw new Error(
              `you need all propertis`
            );
          }
          
          
          const response = await fetch(
            url
          );
          const answer = await response.json();
          if (answer.success) {
            // we reproduce the user that was created in the database, locally
            const id = answer.result;
            const blog = { blog_title,blog_description,blog_date ,id};
            const blog_list = [...this.state.blog_list, blog];
            this.setState({ blog_list });
          } else {
            this.setState({ error_message: answer.message });
            console.log("not create")
          }
        } catch (err) {
          this.setState({ error_message: err.message });
          console.log("error")
        }
      };
      getBlogList = async order => {
        try {
          const response = await fetch(
            `http://localhost:8080/blog/list?order=${order}`
          );
          const answer = await response.json();
          if (answer.success) {
            const blog_list = answer.result;
            this.setState({ blog_list });
          } else {
            this.setState({ error_message: answer.message });
          }
        } catch (err) {
          this.setState({ error_message: err.message });
        }
      };
      updateBlog = async (id, props) => {  
        try {
          if (
            !props ||
            !(props.blog_title || props.blog_description ||  props.blog_date)
          ) {
            throw new Error(`you need at least one property`);
          }
          const response = await fetch(
            `http://localhost:8080/blog/update/${id}?blog_date=${props.blog_date}&blog_title=${
              props.blog_title
            }&blog_description=${props.blog_description}`
          );
          const answer = await response.json();
          if (answer) {
            // we update the user, to reproduce the database changes:
            const blog_list = this.state.blog_list;
            blog_list.map(blog => {
              // if this is the member we need to change, update it. This will apply to exactly
              // one member
              if (blog.id === id) {
                const new_blog = {
                  id: blog.id,
                  blog_title: props.blog_title,
                  blog_description: props.blog_description,
                  blog_date: props.blog_date
                };
    
                return new_blog;
              }
              // otherwise, don't change the members at all
              else {
                return blog;
              }
            });
            this.setState({ blog_list });
            //toast("updated");
          } else {
            this.setState({ error_message: answer.message });
          }
        } catch (err) {
          this.setState({ error_message: err.message });
        }
      };
    
      componentDidMount(){
		this.getBlogList();
    }
    onSubmit = (evt) => {
        evt.preventDefault();
        // extract  blog_title,blog_description,blog_date from state
        const {  blog_title,blog_description,blog_date } = this.state
        // create the blog
        this.createBlog({ blog_title,blog_description,blog_date})
        // empty the text input fields are reset
        this.setState({blog_title:"", blog_description:"",blog_date:""})
      }
    
    render() {
        const {blog_list}=this.state
       
        return(
            <div className="blog grid-container">
        
           <div className="item3">
           <h1>The List of news :</h1>
            {blog_list.map(blog => (
                    <BlogEdit 
                    key={blog.id}
                    id={blog.id}
                    blog_title={blog.blog_title}
                    blog_description={blog.blog_description}
                    blog_date={blog.blog_date}
                    updateBlog={this.updateBlog}
                    deleteBlog={this.deleteBlog}

                    />
  ))}
            </div>
            <form className="third item4" onSubmit={this.onSubmit}>
            <label>Insert new News:</label>
            <br/>
            <input
              type="text"
              placeholder="title"
              onChange={evt => this.setState({ blog_title: evt.target.value })}
              value={this.state.blog_title}
            />
            <input
              type="text"
              placeholder="date"
              onChange={evt => this.setState({ blog_date: evt.target.value })}
              value={this.state.blog_date}
            />
             <input
              type="text"
              placeholder="description"
              onChange={evt => this.setState({ blog_description: evt.target.value })}
              value={this.state.blog_description}
            />
            <div>
              <input type="submit" value="ok" />
              <input type="reset" value="cancel" className="button" />
            </div>
          </form>
  

            </div>
        )
    }}
    export default Blog;