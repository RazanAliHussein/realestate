import React, { Component } from 'react';
class UserEdit extends Component {
    state = {
        editMode:false
      }
      toggleEditMode = () => {
        const editMode = !this.state.editMode
        this.setState({editMode})
      }
      renderEditMode(){
        const { id,name,phone, address,updateUser, deleteUser } = this.props
        return ( 
        <form className="third" onSubmit={this.onSubmit} onReset={this.toggleEditMode}>
        <input
          type="text"
          placeholder="author"
          name="name_input"
          defaultValue={name}
  
        />
        <input
          type="text"
          placeholder="date"
          name="phone_input"
          defaultValue={phone}
  
        />
         <input
          type="text"
          placeholder="date"
          name="address_input"
          defaultValue={address}
  
        />
       
        <div>
        <input type="submit" value="ok" />
        <input type="reset" value="cancel" className="button" />

        </div>
      </form>)
      }
      renderViewMode(){
        const { id,  name,phone,address, updateUser, deleteUser  } = this.props
        return (
            <div>
              <span>
                {id} - {name}
              </span>
              <button className="success" onClick={this.toggleEditMode}>edit</button>
              <button onClick={() => deleteUser(id)} className="warning">x</button>
            </div>
          )
        
      }
      onSubmit = (evt) => {
        // stop the page from refreshing
        evt.preventDefault()
        // target the form
        const form = evt.target
        // extract the inputs from the form
       
        const name_input = form.name_input 
        const phone_input=form.phone_input
        const address_input=form.address_input
        // extract the values
        const name = name_input.value
        const phone = phone_input.value
        const address = address_input.value
     
        // get the id and the update function from the props
        const { id, updateUser } = this.props
        // run the update contact function
        updateUser(id,{ name,phone,address })
        // toggle back view mode
        this.toggleEditMode()
      }
    
       
    render() {
        const { editMode } = this.state
        if(editMode){
          return this.renderEditMode()
        }
        else{
          return this.renderViewMode()
        }
    
       
    }}
    export default UserEdit;