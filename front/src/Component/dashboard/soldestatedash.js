import React, { Component } from 'react';

import '../CSS/dashboard.css'
class Sold extends Component {
    state={
       sold_list:[],
       
        error_message:""
	}
	
	getList = async () => {
		try{
		const response = await fetch("http://localhost:8080/sold/list");
		const data = await response.json();
		this.setState({ sold_list: data.result });
		}
		catch(err){
			console.log(err)
		}
      };
      getSold = async id => {
        // check if we already have the sold
        const previous_sold = this.state.sold_list.find(
          sold => sold.id === id
        );
        if (previous_sold) {
          return; // do nothing, no need to reload a blog we already have
        }
        try {
          const response = await fetch(`http://localhost:8080/sold/get/${id}`);
          const answer = await response.json();
          if (answer.success) {
            // add the user to the current list of blog
            const sold = answer.result;
            const sold_list = [...this.state.sold_list, sold];
            this.setState({ sold_list });
          } else {
            this.setState({ error_message: answer.message });
          }
        } catch (err) {
          this.setState({ error_message: err.message });
        }
      };
      deleteSold = async id => {
        try {
          const response = await fetch(
            `http://localhost:8080/sold/delete/${id}`
          );
          const answer = await response.json();
          if (answer.success) {
            // remove the user from the current list of users
            const sold_list = this.state.sold_list.filter(
              sold => sold.id !== id
            );
            this.setState({ sold_list });
          } else {
            this.setState({ error_message: answer.message });
          }
        } catch (err) {
          this.setState({ error_message: err.message });
        }
      };
      createSold = async props => {
          const { type, ListingEstate_estate_id, Users_user_id, sold_date } = props;
          const url= `http://localhost:8080/sold/new?type=${type}&ListingEstate_estate_id=${ListingEstate_estate_id}&Users_user_id=${Users_user_id}&sold_date=${sold_date}`;
          try {
          if (!props || !(props.type && props.ListingEstate_estate_id && props.Users_user_id &&props.sold_date)) {
            throw new Error(
              `you need all propertis`
            );
          }
          
          
          const response = await fetch(
            url
          );
          const answer = await response.json();
          if (answer.success) {
            // we reproduce the user that was created in the database, locally
            const id = answer.result;
            const sold = { type, ListingEstate_estate_id, Users_user_id, sold_date,id};
            const sold_list = [...this.state.sold_list, sold];
            this.setState({ sold_list });
          } else {
            this.setState({ error_message: answer.message });
            console.log("not create")
          }
        } catch (err) {
          this.setState({ error_message: err.message });
          console.log("error")
        }
      };
      getSoldList = async order => {
        try {
          const response = await fetch(
            `http://localhost:8080/sold/list?order=${order}`
          );
          const answer = await response.json();
          if (answer.success) {
            const sold_list = answer.result;
            this.setState({ sold_list });
          } else {
            this.setState({ error_message: answer.message });
          }
        } catch (err) {
          this.setState({ error_message: err.message });
        }
      };
     
    
      componentDidMount(){
		this.getSoldList();
    }
    onSubmit = (evt) => {
        evt.preventDefault();
        // extract type, ListingEstate_estate_id, Users_user_id, sold_date from state
        const { type, ListingEstate_estate_id, Users_user_id, sold_date } = this.state
        // create the blog
        this.createSold({ type, ListingEstate_estate_id, Users_user_id, sold_date})
        // empty the text input fields are reset
        this.setState({type:"", ListingEstate_estate_id:"", Users_user_id:"", sold_date:""})
      }
    
    render() {
        const {sold_list}=this.state
       
        return(
            <div className="blog grid-container">
        
           <div className="item3">
           <h1>The List of Sold estate :</h1>
            {sold_list.map(sold => (
                <div key={sold.id}>
                <span>{sold.type}-{sold.date}</span>
                </div>
                   
  ))}
            </div>
            <form className="third item4" onSubmit={this.onSubmit}>
            <label>Insert new Sold:</label>   
            <br/>
            <input
              type="text"
              placeholder="type"
              onChange={evt => this.setState({ type: evt.target.value })}
              value={this.state.type}
            />
            <input
              type="text"
              placeholder="ListingEstate_estate_id"
              onChange={evt => this.setState({ ListingEstate_estate_id: evt.target.value })}
              value={this.state.ListingEstate_estate_id}
            />
             <input
              type="text"
              placeholder="Users_user_id"
              onChange={evt => this.setState({ Users_user_id: evt.target.value })}
              value={this.state.Users_user_id}
            />
              <input
              type="text"
              placeholder="sold_date"
              onChange={evt => this.setState({ sold_date: evt.target.value })}
              value={this.state.sold_date}
            />
            <div>
              <input type="submit" value="ok" />
              <input type="reset" value="cancel" className="button" />
            </div>
          </form>
  

            </div>
        )
    }}
    export default Sold;