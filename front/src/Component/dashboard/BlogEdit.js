import React, { Component } from 'react';
class BlogEdit extends Component {
    state = {
        editMode:false
      }
      toggleEditMode = () => {
        const editMode = !this.state.editMode
        this.setState({editMode})
      }
      renderEditMode(){
        const { id,  blog_title,blog_description,blog_date, updateBlog, deleteBlog } = this.props
        return ( 
        <form className="third" onSubmit={this.onSubmit} onReset={this.toggleEditMode}>
        <input
          type="text"
          placeholder="title"
          name="blog_title_input"
          defaultValue={blog_title}
  
        />
        <input
          type="text"
          placeholder="date"
          name="blog_date_input"
          defaultValue={blog_date}
  
        />
         <input
          type="text"
          placeholder="description"
          name="blog_description_input"
          defaultValue={blog_description}
  
        />
        <div>
        <input type="submit" value="ok" />
        <input type="reset" value="cancel" className="button" />

        </div>
      </form>)
      }
      renderViewMode(){
        const { id,  blog_title,blog_description,blog_date, updateBlog, deleteBlog  } = this.props
        return (
            <div>
              <span>
                {id} - {blog_title}
              </span>
              <button className="success" onClick={this.toggleEditMode}>edit</button>
              <button onClick={() => deleteBlog(id)} className="warning">x</button>
            </div>
          )
        
      }
      onSubmit = (evt) => {
        // stop the page from refreshing
        evt.preventDefault()
        // target the form
        const form = evt.target
        // extract the inputs from the form
        const blog_title_input = form.blog_title_input 
        const blog_description_input = form.blog_description_input 
        const blog_date_input=form.blog_date_input
        // extract the values
        const blog_title = blog_title_input.value
        const blog_description = blog_description_input.value
        const blog_date= blog_date_input.value
        // get the id and the update function from the props
        const { id, updateBlog } = this.props
        // run the update contact function
        updateBlog(id,{ blog_title,blog_description,blog_date })
        // toggle back view mode
        this.toggleEditMode()
      }
    
       
    render() {
        const { editMode } = this.state
        if(editMode){
          return this.renderEditMode()
        }
        else{
          return this.renderViewMode()
        }
    
       
    }}
    export default BlogEdit;