import React, { Component } from 'react';
class ListEstateEdit extends Component {
    state = {
        editMode:false
      }
      toggleEditMode = () => {
        const editMode = !this.state.editMode
        this.setState({editMode})
      }
      renderEditMode(){
        const { id, 
            estate_available,
            estate_price,
            estate_garage,
            estate_room,
            estate_bathroom,
            estate_height,
            estate_width,
            estate_video,
            estate_floor,
            estate_location,
            estate_description,
            estate_date,
             updateListingEstate, deleteListingEstate } = this.props
        return ( 
        <form className="third" onSubmit={this.onSubmit} onReset={this.toggleEditMode}>
        <input
          type="text"
          placeholder="available"
          name="estate_available_input"
          defaultValue={estate_available}
  
        />
        <input
          type="text"
          placeholder="room"
          name="estate_room_input"
          defaultValue={estate_room}
        />
         <input
          type="text"
          placeholder="Price"
          name="estate_price_input"
          defaultValue={estate_price}
  
        />
        <input
          type="text"
          placeholder="garage"
          name="estate_garage_input"
          defaultValue={estate_garage}
  
        />
        <input
          type="text"
          placeholder="bathroom"
          name="estate_bathroom_input"
          defaultValue={estate_bathroom}
  
        />
        <input
          type="text"
          placeholder="height"
          name="estate_height_input"
          defaultValue={estate_height}
  
        />
         <input
          type="text"
          placeholder="width"
          name="estate_width_input"
          defaultValue={estate_width}
  
        />
          <input
          type="text"
          placeholder="floor"
          name="estate_floor_input"
          defaultValue={estate_floor}
        />
         <input
          type="text"
          placeholder="location"
          name="estate_location_input"
          defaultValue={estate_location}
        />
         <input
          type="text"
          placeholder="description"
          name="estate_description_input"
          defaultValue={estate_description}
        />
         <input
          type="text"
          placeholder="date"
          name="estate_date_input"
          defaultValue={estate_date}
        />
         <input
          type="text"
          placeholder="video"
          name="estate_video_input"
          defaultValue={estate_video}
        />
        <div>
        <input type="submit" value="ok" />
        <input type="reset" value="cancel" className="button" />

        </div>
      </form>)
      }
      renderViewMode(){
        const { id,   estate_available,
            estate_price,
            estate_garage,
            estate_room,
            estate_bathroom,
            estate_height,
            estate_width,
            estate_floor,
            estate_location,
            estate_description,
            estate_date,
            estate_video, updateListingEstate, deleteListingEstate } = this.props
        return (
            <div>
              <span>
                {id} - {estate_description}
              </span>
              <button className="success" onClick={this.toggleEditMode}>edit</button>
              <button onClick={() => deleteListingEstate(id)} className="warning">x</button>
            </div>
          )
        
      }
      onSubmit = (evt) => {
        // stop the page from refreshing
        evt.preventDefault()
        // target the form
        const form = evt.target
        // extract the inputs from the form
        const estate_available_input =form.estate_available_input
        const estate_price_input=form.estate_price_input
        const estate_garage_input=form.estate_garage_input
        const estate_room_input=form.estate_room_input
        const estate_bathroom_input=form.estate_bathroom_input
        const estate_height_input=form.estate_height_input
        const estate_width_input=form.estate_width_input
        const estate_floor_input=form.estate_floor_input
        const estate_location_input=form.estate_location_input
        const estate_description_input=form.estate_description_input
        const estate_date_input=form.estate_date_input
        const estate_video_input=form.estate_video_input

        // extract the values
        const estate_available =estate_available_input.value
        const estate_price =estate_price_input.value 
        const estate_garage =estate_garage_input.value
        const estate_room =estate_room_input.value
        const estate_bathroom=estate_bathroom_input.value
        const estate_height=estate_height_input.value
        const estate_width=estate_width_input.value
        const estate_floor=estate_floor_input.value
        const estate_location=estate_location_input.value
        const estate_description=estate_description_input.value
        const estate_date=estate_date_input.value
        const estate_video=estate_video_input.value


      
     
        // get the id and the update function from the props
        const { id, updateListingEstate } = this.props
        // run the update contact function
        updateListingEstate(id,{  estate_available,
            estate_price,
            estate_garage,
            estate_room,
            estate_bathroom,
            estate_height,
            estate_width,
            estate_video,
            estate_floor,
            estate_location,
            estate_description,
            estate_date,
            })
        // toggle back view mode
        this.toggleEditMode()
      }
    
       
    render() {
        const { editMode } = this.state
        if(editMode){
          return this.renderEditMode()
        }
        else{
          return this.renderViewMode()
        }
    
       
    }}
    export default ListEstateEdit;