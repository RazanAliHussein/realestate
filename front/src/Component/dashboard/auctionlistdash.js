import React, { Component } from "react";
import AuctionEdit from './AuctionEdit'
import "../CSS/dashboard.css";
class AuctionList extends Component {
  state = {
    auction_list: [],
    error_message: "",
    estate_available: "",
      estate_start_price: "",
      estate_garage: "",
      estate_room: "",
      estate_bathroom: "",
      estate_height: "",
      estate_width: "",
      estate_floor: "",
      estate_location: "",
      estate_description: "",
      estate_date: "",
      estate_video: ""
  };

  getAuction = async id => {
    // check if we already have the blog
    const previous_auction = this.state.auction_list.find(
      auction => auction.id === id
    );
    if (previous_auction) {
      return; // do nothing, no need to reload a blog we already have
    }
    try {
      const response = await fetch(`http://localhost:8080/auction/get/${id}`);
      const answer = await response.json();
      if (answer.success) {
        // add the user to the current list of blog
        const auction = answer.result;
        const auction_list = [...this.state.auction_list, auction];
        this.setState({ auction_list });
      } else {
        this.setState({ error_message: answer.message });
      }
    } catch (err) {
      this.setState({ error_message: err.message });
    }
  };
  deleteAuction = async id => {
    try {
      const response = await fetch(
        `http://localhost:8080/auction/delete/${id}`
      );
      const answer = await response.json();
      if (answer.success) {
        // remove the user from the current list of users
        const auction_list = this.state.auction_list.filter(
          auction => auction.id !== id
        );
        this.setState({ auction_list });
      } else {
        this.setState({ error_message: answer.message });
      }
    } catch (err) {
      this.setState({ error_message: err.message });
    }
  };
  createAuction = async props => {
    const {
      estate_available,
      estate_start_price,
      estate_garage,
      estate_room,
      estate_bathroom,
      estate_height,
      estate_width,
      estate_floor,
      estate_location,
      estate_description,
      estate_date,
      estate_video
    } = props;
    console.log("here")
    const url = `http://localhost:8080/auction/new?estate_available=${estate_available}&estate_start_price=${estate_start_price}&estate_garage=${estate_garage}&estate_room=${estate_room}&estate_bathroom=${estate_bathroom}&estate_height=${estate_height}&estate_width=${estate_width}&estate_floor=${estate_floor}&estate_location=${estate_location}&estate_description=${estate_description}&estate_date=${estate_date}&estate_video=${estate_video}`;
    try {
      if (
        !props ||
        !(
          props.estate_available &&
          props.estate_start_price &&
          props.estate_garage &&
          props.estate_room &&
          props.estate_bathroom &&
          props.estate_height &&
          props.estate_width &&
          props.estate_floor &&
          props.estate_location &&
          props.estate_description &&
          props.estate_date &&
          props.estate_video
        )
      ) 
      {
        throw new Error(`you need all propertis`);
      }

      const response = await fetch(url);
      const answer = await response.json();
      if (answer.success) {
        // we reproduce the user that was created in the database, locally
        const id = answer.result;
        const auction = {
          estate_available,
          estate_start_price,
          estate_garage,
          estate_room,
          estate_bathroom,
          estate_height,
          estate_width,
          estate_floor,
          estate_location,
          estate_description,
          estate_date,
          estate_video,
          id
        };
        const auction_list = [...this.state.auction_list, auction];
        this.setState({ auction_list });
      } else {
        this.setState({ error_message: answer.message });
      }
    } catch (err) {
      this.setState({ error_message: err.message });
    }
  };
  getAuctionList = async order => {
    try {
      const response = await fetch(
        `http://localhost:8080/auction/list?order=${order}`
      );
      const answer = await response.json();
      if (answer.success) {
        const auction_list = answer.result;
        this.setState({ auction_list });
      } else {
        this.setState({ error_message: answer.message });
      }
    } catch (err) {
      this.setState({ error_message: err.message });
    }
  };
  updateAuction = async (id, props) => {
    try {
      if (
        !props ||
        !(
          props.estate_available ||
          props.estate_start_price ||
          props.estate_garage ||
          props.estate_room ||
          props.estate_bathroom ||
          props.estate_height ||
          props.estate_width ||
          props.estate_floor ||
          props.estate_location ||
          props.estate_description ||
          props.estate_date ||
          props.estate_video
        )
      ) {
        throw new Error(`you need at least one property`);
      }
      const response = await fetch(
        `http://localhost:8080/auction/update/${id}?estate_available=${props.estate_available}&estate_start_price=${props.estate_start_price}&estate_garage=${props.estate_garage}&estate_room=${props.estate_room}&estate_bathroom=${props.estate_bathroom}&estate_height=${props.estate_height}&estate_width=${props.estate_width}&estate_floor=${props.estate_floor}&estate_location=${props.estate_location}&estate_description=${props.estate_description}&estate_date=${props.estate_date}&estate_video=${props.estate_video}`
      );
      const answer = await response.json();
      if (answer) {
        // we update the user, to reproduce the database changes:
        const auction_list = this.state.auction_list;
        auction_list.map(auction => {
          // if this is the member we need to change, update it. This will apply to exactly
          // one member
          if (auction.id === id) {
            const new_auction = {
              id: auction.id,
              estate_available: props.estate_available ,
              estate_start_price: props.estate_start_price ,
              estate_garage:props.estate_garage ,
              estate_room:props.estate_room ,
              estate_bathroom:props.estate_bathroom ,
              estate_height: props.estate_height ,
              estate_width:props.estate_width ,
              estate_floor:props.estate_floor ,
              estate_location:props.estate_location ,
              estate_description: props.estate_description ,
              estate_date:props.estate_date ,
              estate_video:props.estate_video
            };

            return new_auction;
          }
          // otherwise, don't change the members at all
          else {
            return auction;
          }
        });
        this.setState({ auction_list });
        //toast("updated");
      } else {
        this.setState({ error_message: answer.message });
      }
    } catch (err) {
      this.setState({ error_message: err.message });
    }
  };

  componentDidMount() {
    this.getAuctionList();
  }
  onSubmit = evt => {
    evt.preventDefault();
    // extract  blog_title,blog_description,blog_date from state
    const {
      estate_available,
      estate_start_price,
      estate_garage,
      estate_room,
      estate_bathroom,
      estate_height,
      estate_width,
      estate_floor,
      estate_location,
      estate_description,
      estate_date,
      estate_video
    } = this.state;
    // create the blog
    this.createAuction({
      estate_available,
      estate_start_price,
      estate_garage,
      estate_room,
      estate_bathroom,
      estate_height,
      estate_width,
      estate_floor,
      estate_location,
      estate_description,
      estate_date,
      estate_video
    });
    // empty the text input fields are reset
    this.setState({
      estate_available: "",
      estate_start_price: "",
      estate_garage: "",
      estate_room: "",
      estate_bathroom: "",
      estate_height: "",
      estate_width: "",
      estate_floor: "",
      estate_location: "",
      estate_description: "",
      estate_date: "",
      estate_video: ""
    });
  };

  render() {
    const { auction_list } = this.state;

    return (
      <div className="blog grid-container">
        <div className="item3">
          <h1>The List of Auction Estate :</h1>
          {auction_list.map(auction => (
            <AuctionEdit
              key={auction.id}
              id={auction.id}
              estate_available={auction.estate_available}
              estate_start_price={auction.estate_start_price}
              estate_garage={auction.estate_garage}
              estate_room={auction.estate_room}
              estate_bathroom={auction.estate_bathroom}
              estate_height={auction.estate_height}
              estate_width={auction.estate_width}
              estate_floor={auction.estate_floor}
              estate_location={auction.estate_location}
              estate_description={auction.estate_description}
              estate_date={auction.estate_date}
              estate_video={auction.estate_video}
              updateAuction={this.updateAuction}
              deleteAuction={this.deleteAuction}
            />
          ))}
        </div>
        <form className="third item4" onSubmit={this.onSubmit}>
          <label>Insert new Auction List:     
       </label>
          <br />
          <input
            type="text"
            placeholder="Available"
            onChange={evt => this.setState({ estate_available: evt.target.value })}
            value={this.state.estate_available}
          />
          <input
            type="text"
            placeholder="price"
            onChange={evt => this.setState({ estate_start_price: evt.target.value })}
            value={this.state.estate_start_price}
          />
          <input
            type="text"
            placeholder="garage"
            onChange={evt =>
              this.setState({ estate_garage: evt.target.value })
            }
            value={this.state.estate_garage}
          />
            <input
            type="text"
            placeholder="Room"
            onChange={evt =>
              this.setState({ estate_room: evt.target.value })
            }
            value={this.state.estate_room}
          />
            <input
            type="text"
            placeholder="Bath room"
            onChange={evt =>
              this.setState({ estate_bathroom: evt.target.value })
            }
            value={this.state.estate_bathroom}
          />
           <input
            type="text"
            placeholder="Height"
            onChange={evt =>
              this.setState({ estate_height: evt.target.value })
            }
            value={this.state.estate_height}
          />
           <input
            type="text"
            placeholder="Width"
            onChange={evt =>
              this.setState({ estate_width: evt.target.value })
            }
            value={this.state.estate_width}
          />
           <input
            type="text"
            placeholder="Floor"
            onChange={evt =>
              this.setState({estate_floor: evt.target.value })
            }
            value={this.state.estate_floor}
          />
           <input
            type="text"
            placeholder="location"
            onChange={evt =>
              this.setState({ estate_location: evt.target.value })
            }
            value={this.state.estate_location}
          />
             <input
            type="text"
            placeholder="description"
            onChange={evt =>
              this.setState({estate_description: evt.target.value })
            }
            value={this.state.estate_description}
          />
           <input
            type="text"
            placeholder="date"
            onChange={evt =>
              this.setState({estate_date: evt.target.value })
            }
            value={this.state.estate_date}
          />
           <input
            type="text"
            placeholder="video"
            onChange={evt =>
              this.setState({estate_video: evt.target.value })
            }
            value={this.state.estate_video}
          />
          <div>
            <input type="submit" value="ok" />
            <input type="reset" value="cancel" className="button" />
          </div>
        </form>
      </div>
    );
  }
}
export default AuctionList;
