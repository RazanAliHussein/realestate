import React, { Component } from 'react';
class TestimonialEdit extends Component {
    state = {
        editMode:false
      }
      toggleEditMode = () => {
        const editMode = !this.state.editMode
        this.setState({editMode})
      }
      renderEditMode(){
        const { id,testimonial_author,testimonial_description, updateTestimonial, deleteTestimonial } = this.props
        return ( 
        <form className="third" onSubmit={this.onSubmit} onReset={this.toggleEditMode}>
        <input
          type="text"
          placeholder="author"
          name="testimonial_author_input"
          defaultValue={testimonial_author}
  
        />
        <input
          type="text"
          placeholder="date"
          name="testimonial_description_input"
          defaultValue={testimonial_description}
  
        />
       
        <div>
        <input type="submit" value="ok" />
        <input type="reset" value="cancel" className="button" />

        </div>
      </form>)
      }
      renderViewMode(){
        const { id,  testimonial_author,testimonial_description, updateTestimonial, deleteTestimonial  } = this.props
        return (
            <div>
              <span>
                {id} - {testimonial_author}
              </span>
              <button className="success" onClick={this.toggleEditMode}>edit</button>
              <button onClick={() => deleteTestimonial(id)} className="warning">x</button>
            </div>
          )
        
      }
      onSubmit = (evt) => {
        // stop the page from refreshing
        evt.preventDefault()
        // target the form
        const form = evt.target
        // extract the inputs from the form
       
        const testimonial_author_input = form.testimonial_author_input 
        const testimonial_description_input=form.testimonial_description_input
        // extract the values
        const testimonial_author = testimonial_author_input.value
        const testimonial_description = testimonial_description_input.value
     
        // get the id and the update function from the props
        const { id, updateTestimonial } = this.props
        // run the update contact function
        updateTestimonial(id,{ testimonial_author,testimonial_description })
        // toggle back view mode
        this.toggleEditMode()
      }
    
       
    render() {
        const { editMode } = this.state
        if(editMode){
          return this.renderEditMode()
        }
        else{
          return this.renderViewMode()
        }
    
       
    }}
    export default TestimonialEdit;