import React, { Component } from 'react';
import {BrowserRouter as Router,Route,Link} from "react-router-dom";
//Dash Components
import Blog from './blogdash'
import Testimonial from './testimonialdash'
import Auctionlist from './auctionlistdash'
import Listestate from './listestatedash'
import Soldestate from './soldestatedash'
import User from './userdash'
//CSS

import '../CSS/dashboard.css'



class Main extends Component {
  state={
    page:"",
    checkingSession:false
  }
  handleChange=(e)=>{
    this.setState({page:e.target.value})
  }
  RenderView=()=>{
    if(this.state.page===""){
      return(
        <div className="press">Press a button to edit the data of it</div>
      )
    }
    else if(this.state.page==="blog"){
        return(
          <Blog/>
        )
    }
    else if(this.state.page==="testimonial"){
      return(
        <Testimonial/>
      )
    }
    else if(this.state.page==="auction"){
      return(
        <Auctionlist/>
      )
    }
    else if(this.state.page==="list"){
      return(
        <Listestate/>
      )
    }
    else if(this.state.page==="sold"){
      return(
        <Soldestate/>
      )
    }
    else if(this.state.page==="auctionimage"){
      
    }
    else if(this.state.page==="listimage"){
      
    }
    else if(this.state.page==="user"){
      return(
        <User/>
      )
    }
    else if(this.state.page==="inauction"){
      
    }
    else{
      return( <div>OOPsss</div>)
     
    }
  }
  render() {
   
    return (
   <div className="main">
       <Link to="/" className="item1 linktohome">Back to HOME</Link>
       <br/>
       <br/>
       <br/>
     
   <button className="dashbutton" value="blog" onClick={this.handleChange}>Blog</button>
   <button className="dashbutton" value="testimonial"onClick={this.handleChange}>Testimonial</button>
   <button className="dashbutton" value="auction" onClick={this.handleChange}>Auction Estate</button>
   <button className="dashbutton" value="list" onClick={this.handleChange}>Listing Estate</button>
   <button className="dashbutton" value="sold" onClick={this.handleChange}>Sold Estate</button>
   <button className="dashbutton" value="auctionimage" onClick={this.handleChange}>Auction Estate Image</button>
   <button className="dashbutton" value="listimage" onClick={this.handleChange}>Listing Estate Image</button>
   <button className="dashbutton" value="user" onClick={this.handleChange}>User</button>
   <button className="dashbutton" value="inauction" onClick={this.handleChange}>In Auction</button>
   <br/>
   <br/>
   {this.RenderView()}
   </div>
    );
  }
}

export default Main;
