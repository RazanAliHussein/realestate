import React, { Component } from 'react';
import UserEdit from'./userEdit'
import '../CSS/dashboard.css'
class User extends Component {
    state={
        user_list:[],
        error_message:"",
        name:"",
        phone:"",
        address:"",
        user_id:""
	}

      getUser = async id => {
        // check if we already have the blog
        const previous_user = this.state.user_list.find(
          user => user.id === id
        );
        if (previous_user) {
          return; // do nothing, no need to reload a blog we already have
        }
        try {
          const response = await fetch(`http://localhost:8080/user/get/${id}`);
          const answer = await response.json();
          if (answer.success) {
            // add the user to the current list of blog
            const user = answer.result;
            const user_list = [...this.state.user_list, user];
            this.setState({ user_list });
          } else {
            this.setState({ error_message: answer.message });
          }
        } catch (err) {
          this.setState({ error_message: err.message });
        }
      };
      deleteUser = async id => {
        try {
          const response = await fetch(
            `http://localhost:8080/user/delete/${id}`
          );
          const answer = await response.json();
          if (answer.success) {
            // remove the user from the current list of users
            const user_list = this.state.user_list.filter(
              user => user.id !== id
            );
            this.setState({ user_list });
          } else {
            this.setState({ error_message: answer.message });
          }
        } catch (err) {
          this.setState({ error_message: err.message });
        }
      };
      createUser = async props => { 
        const { name,user_id,phone,address} = props;
        const url=`http://localhost:8080/user/new?name=${name}&user_id=${user_id}&phone=${phone}&address=${address}`
        try {
          if (!props || !(props.name&&props.user_id && props.phone && props.address )) {
            throw new Error(
              `you need all propertis`
            );
          }
        
          const response = await fetch(
            url
          );
          const answer = await response.json();
          if (answer.success) {
            // we reproduce the user that was created in the database, locally
            const id = answer.result;
            const user = {  name,phone,address ,id};
            const user_list = [...this.state.user_list, user];
            this.setState({ user_list });
          } else {
            this.setState({ error_message: answer.message });
            console.log("not create")
          }
        } catch (err) {
          this.setState({ error_message: err.message });
        }
      };
      getUserList = async order => {
        try {
          const response = await fetch(
            `http://localhost:8080/user/list`
          );
          const answer = await response.json();
          if (answer.success) {
            const user_list = answer.result;
            this.setState({ user_list });
          } else {
            this.setState({ error_message: answer.message });
          }
        } catch (err) {
          this.setState({ error_message: err.message });
        }
      };
      updateUser = async (id, props) => {   
        try {
          if (
            !props ||
            !(props.name ||props.user_id|| props.phone ||props.address )
          ) {
            throw new Error(`you need at least one property`);
          }
          const response = await fetch(
            `http://localhost:8080/user/update/${id}?name=${props.name}&phone=${props.phone}&address=${props.address}`
          );
          const answer = await response.json();
          if (answer) {
            // we update the user, to reproduce the database changes:
            const user_list = this.state.user_list;
            user_list.map(user => {
              // if this is the member we need to change, update it. This will apply to exactly
              // one member
              if (user.id === id) {
                const new_user = {
                  id: user.id,
                 name: props.name,
                  phone: props.phone,
                  address: props.address
                
                };
    
                return new_user;
              }
              // otherwise, don't change the members at all
              else {
                return user;
              }
            });
            this.setState({ user_list });
            //toast("updated");
          } else {
            this.setState({ error_message: answer.message });
          }
        } catch (err) {
          this.setState({ error_message: err.message });
        }
      };
    
      componentDidMount(){
		this.getUserList();
    }
    onSubmit = (evt) => {
        evt.preventDefault();
        // extract testimonial_description from state
        const {  name,phone,address,user_id } = this.state
        // create the blog
        this.createUser({name,phone,address,user_id})
        // empty the text input fields are reset
        this.setState({name:"",user_id:"", phone:"",address:""})
      }
    
    render() {
        const {user_list}=this.state
       
        return(
            <div className="blog grid-container">
        
           <div className="item3">
           <h1>The List of user :</h1>
            {user_list.map(user => (
                    <UserEdit
                    key={user.id}
                    id={user.id}
                    name={user.name}
                    phone={user.phone}
                    address={user.address}
                    updateUser={this.updateUser}
                    deleteUser={this.deleteUser}

                    />
  ))}
            </div>
            <form className="third item4" onSubmit={this.onSubmit}>
            <label>Insert new User:</label>
            <br/>
            <input
              type="text"
              placeholder="name"
              onChange={evt => this.setState({ name: evt.target.value })}
              value={this.state.name}
            />
             <input
              type="text"
              placeholder="phone"
              onChange={evt => this.setState({ phone: evt.target.value })}
              value={this.state.phone}
            />
             <input
              type="text"
              placeholder="address"
              onChange={evt => this.setState({ address: evt.target.value })}
              value={this.state.address}
            />
             <input
              type="text"
              placeholder="id"
              onChange={evt => this.setState({ user_id: evt.target.value })}
              value={this.state.user_id}
            />
            <div>
              <input type="submit" value="ok" />
              <input type="reset" value="cancel" className="button" />
            </div>
          </form>
  

            </div>
        )
    }}
    export default User;