import React, { Component } from 'react';
import TestimonialEdit from './testimonialEdit'
import '../CSS/dashboard.css'
class Testimonial extends Component {
    state={
        testimonial_list:[],
        error_message:"",
        testimonial_author:"",
        testimonial_description:""
	}

      getTestimonial = async id => {
        // check if we already have the blog
        const previous_testimonial = this.state.testimonial_list.find(
          testimonial => testimonial.id === id
        );
        if (previous_testimonial) {
          return; // do nothing, no need to reload a blog we already have
        }
        try {
          const response = await fetch(`http://localhost:8080/testimonial/get/${id}`);
          const answer = await response.json();
          if (answer.success) {
            // add the user to the current list of blog
            const testimonial = answer.result;
            const testimonial_list = [...this.state.blog_list, testimonial];
            this.setState({ testimonial_list });
          } else {
            this.setState({ error_message: answer.message });
          }
        } catch (err) {
          this.setState({ error_message: err.message });
        }
      };
      deleteTestimonial = async id => {
        try {
          const response = await fetch(
            `http://localhost:8080/testimonial/delete/${id}`
          );
          const answer = await response.json();
          if (answer.success) {
            // remove the user from the current list of users
            const testimonial_list = this.state.testimonial_list.filter(
              testimonial => testimonial.id !== id
            );
            this.setState({ testimonial_list });
          } else {
            this.setState({ error_message: answer.message });
          }
        } catch (err) {
          this.setState({ error_message: err.message });
        }
      };
      createBlog = async props => { 
        const {  testimonial_author,testimonial_description} = props;
        const url=`http://localhost:8080/testimonial/new?testimonial_author=${testimonial_author}&testimonial_description=${testimonial_description}`
        try {
          if (!props || !(props.testimonial_author && props.testimonial_description )) {
            throw new Error(
              `you need all propertis`
            );
          }
        
          const response = await fetch(
            url
          );
          const answer = await response.json();
          if (answer.success) {
            // we reproduce the user that was created in the database, locally
            const id = answer.result;
            const testimonial = {  testimonial_author,testimonial_description ,id};
            const testimonial_list = [...this.state.testimonial_list, testimonial];
            this.setState({ testimonial_list });
          } else {
            this.setState({ error_message: answer.message });
            console.log("not create")
          }
        } catch (err) {
          this.setState({ error_message: err.message });
        }
      };
      getTestimonialList = async order => {
        try {
          const response = await fetch(
            `http://localhost:8080/testimonial/list`
          );
          const answer = await response.json();
          if (answer.success) {
            const testimonial_list = answer.result;
            this.setState({ testimonial_list });
          } else {
            this.setState({ error_message: answer.message });
          }
        } catch (err) {
          this.setState({ error_message: err.message });
        }
      };
      updateTestimonial = async (id, props) => {   
        try {
          if (
            !props ||
            !(props.testimonial_author || props.testimonial_description )
          ) {
            throw new Error(`you need at least one property`);
          }
          const response = await fetch(
            `http://localhost:8080/testimonial/update/${id}?testimonial_author=${props.testimonial_author}&testimonial_description=${
              props.testimonial_description
            }`
          );
          const answer = await response.json();
          if (answer) {
            // we update the user, to reproduce the database changes:
            const testimonial_list = this.state.testimonial_list;
            testimonial_list.map(testimonial => {
              // if this is the member we need to change, update it. This will apply to exactly
              // one member
              if (testimonial.id === id) {
                const new_testimonial = {
                  id: testimonial.id,
                  testimonial_author: props.testimonial_author,
                  testimonial_description: props.testimonial_description,
                
                };
    
                return new_testimonial;
              }
              // otherwise, don't change the members at all
              else {
                return testimonial;
              }
            });
            this.setState({ testimonial_list });
            //toast("updated");
          } else {
            this.setState({ error_message: answer.message });
          }
        } catch (err) {
          this.setState({ error_message: err.message });
        }
      };
    
      componentDidMount(){
		this.getTestimonialList();
    }
    onSubmit = (evt) => {
        evt.preventDefault();
        // extract testimonial_description from state
        const {  testimonial_author,testimonial_description } = this.state
        // create the blog
        this.createBlog({testimonial_author,testimonial_description})
        // empty the text input fields are reset
        this.setState({testimonial_author:"", testimonial_description:""})
      }
    
    render() {
        const {testimonial_list}=this.state
       
        return(
            <div className="blog grid-container">
        
           <div className="item3">
           <h1>The List of news :</h1>
            {testimonial_list.map(testimonial => (
                    <TestimonialEdit
                    key={testimonial.id}
                    id={testimonial.id}
                    testimonial_author={testimonial.testimonial_author}
                    testimonial_description={testimonial.testimonial_description}
                  
                    updateTestimonial={this.updateTestimonial}
                    deleteTestimonial={this.deleteTestimonial}

                    />
  ))}
            </div>
            <form className="third item4" onSubmit={this.onSubmit}>
            <label>Insert new News:</label>
            <br/>
            <input
              type="text"
              placeholder="author"
              onChange={evt => this.setState({ testimonial_author: evt.target.value })}
              value={this.state.testimonial_author}
            />
             <input
              type="text"
              placeholder="description"
              onChange={evt => this.setState({ testimonial_description: evt.target.value })}
              value={this.state.testimonial_description}
            />
            <div>
              <input type="submit" value="ok" />
              <input type="reset" value="cancel" className="button" />
            </div>
          </form>
  

            </div>
        )
    }}
    export default Testimonial;