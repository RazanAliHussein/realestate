import sqlite from "sqlite";
import SQL from "sql-template-strings";

const initializeDatabase = async () => {
  const db = await sqlite.open("./db.sqlite");
  /**
   * Create the table
   **/

  await db.run(`CREATE TABLE IF NOT EXISTS AuctionEstate (
    estate_id integer NOT NULL CONSTRAINT AuctionEstate_pk PRIMARY KEY AUTOINCREMENT,
    estate_available boolean NOT NULL,
    estate_start_price integer NOT NULL,
    estate_garage integer NOT NULL,
    estate_room integer NOT NULL,
    estate_bathroom integer NOT NULL,
    estate_height integer NOT NULL,
    estate_width integer NOT NULL,
    estate_floor integer NOT NULL,
    estate_location text NOT NULL,
    estate_description clob NOT NULL,
    estate_date date NOT NULL,
    estate_video text NOT NULL
);`);
  await db.run(`CREATE TABLE IF NOT EXISTS AuctionEstateImage (
    estate_image text NOT NULL,
    AuctionEstate_estate_id integer NOT NULL,
    CONSTRAINT AuctionEstateImage_pk PRIMARY KEY (AuctionEstate_estate_id,estate_image),
    CONSTRAINT AuctionEstateImage_AuctionEstate FOREIGN KEY (AuctionEstate_estate_id)
    REFERENCES AuctionEstate (estate_id)
);
`);
  await db.run(`CREATE TABLE IF NOT EXISTS Blog (
    blog_id integer NOT NULL CONSTRAINT Blog_pk PRIMARY KEY AUTOINCREMENT,
    blog_title text NOT NULL,
    blog_description clob NOT NULL,
    blog_date date NOT NULL
);`);
await db.run(`CREATE TABLE IF NOT EXISTS sell (
 sell_id integer NOT NULL CONSTRAINT Sell_pk PRIMARY KEY AUTOINCREMENT,
  name text NOT NULL,
  description clob NOT NULL,
  phone integer NOT NULL,
  type text NOT NULL,
  email text NOT NULL,
  address text NOT NULL,
  price text NOT NULL
);`);
  await db.run(`
CREATE TABLE IF NOT EXISTS InAuction (
    inauction_date date NOT NULL,
    time datetime NOT NULL,
    price integer NOT NULL,
    AuctionEstate_estate_id integer NOT NULL,
    Users_user_id integer NOT NULL,
    CONSTRAINT InAuction_pk PRIMARY KEY (AuctionEstate_estate_id,Users_user_id,price),
    CONSTRAINT InAuction_AuctionEstate FOREIGN KEY (AuctionEstate_estate_id)
    REFERENCES AuctionEstate (estate_id),
    CONSTRAINT InAuction_Users FOREIGN KEY (Users_user_id)
    REFERENCES Users (auth0_sub)
);`);
  await db.run(`CREATE TABLE IF NOT EXISTS ListingEstate (
    estate_id integer NOT NULL CONSTRAINT ListingEstate_pk PRIMARY KEY AUTOINCREMENT,
    estate_available boolean NOT NULL,
    estate_price integer NOT NULL,
    estate_garage integer NOT NULL,
    estate_room integer NOT NULL,
    estate_bathroom integer NOT NULL,
    estate_height integer NOT NULL,
    estate_width integer NOT NULL,
    estate_video text NOT NULL,
    estate_floor integer NOT NULL,
    estate_location text NOT NULL,
    estate_description clob NOT NULL,
    estate_date date NOT NULL
);`);
  await db.run(`CREATE TABLE IF NOT EXISTS ListingEstateImages (
    estate_image text NOT NULL,
    ListingEstate_estate_id integer NOT NULL,
    CONSTRAINT ListingEstateImages_pk PRIMARY KEY (ListingEstate_estate_id,estate_image),
    CONSTRAINT ListingEstateImages_ListingEstate FOREIGN KEY (ListingEstate_estate_id)
    REFERENCES ListingEstate (estate_id)
);`);
  await db.run(`CREATE TABLE IF NOT EXISTS SoldEstate (
    sold_id integer NOT NULL CONSTRAINT SoldEstate_pk PRIMARY KEY AUTOINCREMENT,
    type text NOT NULL,
    sold_date date NOT NULL,
    ListingEstate_estate_id integer NOT NULL,
    Users_user_id integer NOT NULL,
    CONSTRAINT SoldEstate_ListingEstate FOREIGN KEY (ListingEstate_estate_id)
    REFERENCES ListingEstate (estate_id),
    CONSTRAINT SoldEstate_Users FOREIGN KEY (Users_user_id)
    REFERENCES Users (user_id)
);`);
  await db.run(`CREATE TABLE IF NOT EXISTS Testimonial (
    testimonial_id integer NOT NULL CONSTRAINT Testimonial_pk PRIMARY KEY AUTOINCREMENT,
    testimonial_author text NOT NULL,
    testimonial_description clob NOT NULL
);
`);

await db.run(`CREATE TABLE IF NOT EXISTS "Users" (
	"user_id"	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
	"auth0_sub"	TEXT,
	"nickname"	TEXT
)`)
await db.run(`CREATE TABLE IF NOT EXISTS AuthUser (
    user_id INTEGER PRIMARY KEY AUTOINCREMENT,
    auth0_sub TEXT UNIQUE, 
    nickname TEXT

);`);
  ///////////////////////////////////////////////////////////////////////////////////////
  /**
   * creates a AuctionEstate
   * @param {object} props an object with keys
   * @returns {number} the id of the created AuctionEstate (or an error if things went wrong)
   */
  const createAuctionEstate = async props => {
    if (
      !props ||
      !props.estate_available ||
      !props.estate_start_price ||
      !props.estate_garage ||
      !props.estate_room ||
      !props.estate_bathroom ||
      !props.estate_height ||
      !props.estate_width ||
      !props.estate_floor ||
      !props.estate_location ||
      !props.estate_description ||
      !props.estate_date ||
      !props.estate_video
    ) {
      throw new Error(`you must provide prop`);
    }
    const {
      estate_available,
      estate_start_price,
      estate_garage,
      estate_room,
      estate_bathroom,
      estate_height,
      estate_width,
      estate_floor,
      estate_location,
      estate_description,
      estate_date,
      estate_video
    } = props;
    try {
      const result = await db.run(SQL`INSERT INTO AuctionEstate(estate_available ,estate_start_price,estate_garage,estate_room,estate_bathroom,estate_height,estate_width,
        estate_floor,estate_location,estate_description,estate_date,estate_video) VALUES (${estate_available},
         ${estate_start_price}, ${estate_garage}, ${estate_room}, ${estate_bathroom}, ${estate_height},
         ${estate_width}, ${estate_floor},${estate_location},${estate_description},${estate_date},${estate_video}
            )`);
      const id = result.stmt.lastID;
      return id;
    } catch (e) {
      throw new Error(`couldn't insert this combination: ` + e.message);
    }
  };
  /**
   * creates a AuctionEstateImage
   * @param {object} props an object with keys
   * @returns {number} the id of the created AuctionEstateImage (or an error if things went wrong)
   */
  const createAuctionEstateImage = async props => {
    if (!props || !props.AuctionEstate_estate_id || !props.estate_image) {
      throw new Error(
        `you must provide a AuctionEstate_estate_id and an estate_image`
      );
    }
    const { AuctionEstate_estate_id, estate_image } = props;
    try {
      const result = await db.run(
        SQL`INSERT INTO AuctionEstateImage (AuctionEstate_estate_id,estate_image) VALUES (${AuctionEstate_estate_id}, ${estate_image})`
      );
      const id = result.stmt.lastID;
      return id;
    } catch (e) {
      throw new Error(`couldn't insert this combination: ` + e.message);
    }
  };
  /**
   * creates a Blog
   * @param {object} props an object with keys
   * @returns {number} the id of the created Blog (or an error if things went wrong)
   */
  const createBlog = async props => {
    if (
      !props ||
      !props.blog_title ||
      !props.blog_description ||
      !props.blog_date
    ) {
      throw new Error(`you must provide aLL PROPERTIS`);
    }
    const { blog_title, blog_description, blog_date } = props;
    try {
      const result = await db.run(
        SQL`INSERT INTO Blog (blog_title,blog_description,blog_date) VALUES (${blog_title}, ${blog_description},${blog_date})`
      );
      const id = result.stmt.lastID;
      return id;
    } catch (e) {
      throw new Error(`couldn't insert this combination: ` + e.message);
    }
  };
    /**
   * creates a sell
   * @param {object} props an object with keys
   * @returns {number} the id of the created sell (or an error if things went wrong)
   */
  const createSell = async props => {
    if (
      !props ||
      !props.name ||
      !props.phone ||
      !props.description||
      !props.email ||
      !props.address||
      !props.price ||
      !props.type
    ) {
      throw new Error(`you must provide aLL PROPERTIS`);
    }
    const { name, phone, type,price,description,email,address } = props;
    try {
      const result = await db.run(
        SQL`INSERT INTO Sell (name, phone, type,price,description,email,address) VALUES (${name}, ${phone},${type},${price},${description},${email},${address})`
      );
      const id = result.stmt.lastID;
      return id;
    } catch (e) {
      throw new Error(`couldn't insert this combination: ` + e.message);
    }
  };
  /**
   * creates a InAuction
   * @param {object} props an object with keys
   * @returns {number} the id of the created InAuction (or an error if things went wrong)
   */
  const createInAuction = async props => {
    if (
      !props ||
      !props.AuctionEstate_estate_id ||
      !props.Users_user_id ||
      !props.inauction_date ||
      !props.time ||
      !props.price
    ) {
      throw new Error(`you must provide aLL PROPERTIS`);
    }
    const {
      AuctionEstate_estate_id,
      Users_user_id,
      inauction_date,
      time,
      price
    } = props;
    try {
      const result = await db.run(
        SQL`INSERT INTO InAuction (AuctionEstate_estate_id,Users_user_id,inauction_date,time,price) VALUES (${AuctionEstate_estate_id}, ${Users_user_id},${inauction_date},${time},${price})`
      );
      const id = result.stmt.lastID;
      return id;
    } catch (e) {
      throw new Error(`couldn't insert this combination: ` + e.message);
    }
  };
  /**
   * creates a ListingEstate
   * @param {object} props an object with keys
   * @returns {number} the id of the created ListingEstate (or an error if things went wrong)
   */
  const createListingEstate = async props => {
    if (
      !props ||
      !props.estate_available ||
      !props.estate_price ||
      !props.estate_garage ||
      !props.estate_room ||
      !props.estate_bathroom ||
      !props.estate_height ||
      !props.estate_width ||
      !props.estate_video ||
      !props.estate_floor ||
      !props.estate_location ||
      !props.estate_description ||
      !props.estate_date
    ) {
      throw new Error(`you must provide aLL PROPERTIS`);
    }
    const {
      estate_available,
      estate_price,
      estate_garage,
      estate_room,
      estate_bathroom,
      estate_height,
      estate_width,
      estate_video,
      estate_floor,
      estate_location,
      estate_description,
      estate_date
    } = props;
    try {
      const result = await db.run(SQL`INSERT INTO ListingEstate (estate_available,estate_price,estate_garage,estate_room,estate_bathroom,estate_height,estate_width,estate_video,estate_floor,estate_location,estate_description,estate_date) VALUES (${estate_available}, ${estate_price},${estate_garage},${estate_room},${estate_bathroom},${estate_height},${estate_width},${estate_video},${estate_floor},${estate_location},${estate_description},${estate_date})`);
      const id = result.stmt.lastID;
      return id;
    } catch (e) {
      throw new Error(`couldn't insert this combination: ` + e.message);
    }
  };

  /**
   * creates a ListingEstateImages
   * @param {object} props an object with keys
   * @returns {number} the id of the created ListingEstateImages (or an error if things went wrong)
   */
  const createListingEstateImages = async props => {
    if (!props || !props.ListingEstate_estate_id || !props.estate_image) {
      throw new Error(
        `you must provide a ListingEstate_estate_id and an estate_image`
      );
    }
    const { ListingEstate_estate_id, estate_image } = props;
    try {
      const result = await db.run(
        SQL`INSERT INTO ListingEstateImages (ListingEstate_estate_id,estate_image) VALUES (${ListingEstate_estate_id}, ${estate_image})`
      );
      const id = result.stmt.lastID;
      return id;
    } catch (e) {
      throw new Error(`couldn't insert this combination: ` + e.message);
    }
  };
  /**
   * creates a SoldEstate
   * @param {object} props an object with keys
   * @returns {number} the id of the created SoldEstate (or an error if things went wrong)
   */
  const createSoldEstate = async props => {
    if (
      !props ||
      !props.type ||
      !props.ListingEstate_estate_id ||
      !props.Users_user_id ||
      !props.sold_date
    ) {
      throw new Error(`you must provide aLL PROPERTIS`);
    }
    const { type, ListingEstate_estate_id, Users_user_id, sold_date } = props;
    try {
      const result = await db.run(
        SQL`INSERT INTO SoldEstate (type,ListingEstate_estate_id,Users_user_id,sold_date ) VALUES (${type}, ${ListingEstate_estate_id},${Users_user_id},${sold_date})`
      );
      const id = result.stmt.lastID;
      return id;
    } catch (e) {
      throw new Error(`couldn't insert this combination: ` + e.message);
    }
  };
  /**
   * creates a Testimonial
   * @param {object} props an object with keys
   * @returns {number} the id of the created Testimonial (or an error if things went wrong)
   */
  const createTestimonial = async props => {
    if (!props || !props.testimonial_author || !props.testimonial_description) {
      throw new Error(`you must provide aLL PROPERTIS`);
    }
    const { testimonial_author, testimonial_description } = props;
    try {
      const result = await db.run(
        SQL`INSERT INTO Testimonial (testimonial_author,testimonial_description ) VALUES (${testimonial_author}, ${testimonial_description})`
      );
      const id = result.stmt.lastID;
      return id;
    } catch (e) {
      throw new Error(`couldn't insert this combination: ` + e.message);
    }
  };
  /**
   * creates a Users
   * @param {object} props an object with keys
   * @returns {number} the id of the created Users (or an error if things went wrong)
   */
  const createUser = async props => {
    if (
      !props ||
      !props.name ||
      !props.user_id ||
      !props.phone ||
      !props.address
    ) {
      throw new Error(`you must provide aLL PROPERTIS`);
    }
    const { name, user_id, phone, address } = props;
    try {
      const result = await db.run(
        SQL`INSERT INTO Users (name,user_id,phone,address ) VALUES (${name}, ${user_id},${phone},${address})`
      );
      const id = result.stmt.lastID;
      return id;
    } catch (e) {
      throw new Error(`couldn't insert this combination: ` + e.message);
    }
  };

  ////////////////////////////////////////////////////////////////////////////////////
  /**
   * deletes a AuctionEstate
   * @param {number} id the id of the AuctionEstate to delete
   * @returns {boolean} `true` if the AuctionEstate was deleted, an error otherwise
   */
  const deleteAuctionEstate = async id => {
    try {
      const result = await db.run(
        SQL`DELETE FROM AuctionEstate WHERE estate_id = ${id}`
      );
      if (result.stmt.changes === 0) {
        throw new Error(`AuctionEstate "${id}" does not exist`);
      }
      return true;
    } catch (e) {
      throw new Error(
        `couldn't delete the AuctionEstate "${id}": ` + e.message
      );
    }
  };
  /**
   * deletes a AuctionEstate
   * @param {number} id the id of the AuctionEstate to delete
   * @returns {boolean} `true` if the AuctionEstate was deleted, an error otherwise
   */
  const deleteAuctionEstateImage = async (id) => {
    try {
      const result = await db.run(
        SQL`DELETE FROM AuctionEstateImage WHERE  estate_image=${id}`
      );
      if (result.stmt.changes === 0) {
        throw new Error(`AuctionEstateImage "${id}" does not exist`);
      }
      return true;
    } catch (e) {
      throw new Error(
        `couldn't delete the AuctionEstateImage "${id}": ` + e.message
      );
    }
  };
  /**
   * deletes a Blog
   * @param {number} id the id of the Blog to delete
   * @returns {boolean} `true` if the Blog was deleted, an error otherwise
   */
  const deleteBlog = async id => {
    try {
      const result = await db.run(SQL`DELETE FROM Blog WHERE blog_id = ${id}`);
      if (result.stmt.changes === 0) {
        throw new Error(`Blog "${id}" does not exist`);
      }
      return true;
    } catch (e) {
      throw new Error(`couldn't delete the Blog "${id}": ` + e.message);
    }
  };
  /**
   * deletes a InAuction
   * @param {number} id the id of the InAuction to delete
   * @returns {boolean} `true` if the InAuction was deleted, an error otherwise
   */
  const deleteInAuction = async (
    AuctionEstate_estate_id,
    Users_user_id,
    price
  ) => {
    try {
      const result = await db.run(
        SQL`DELETE FROM InAuction WHERE AuctionEstate_estate_id = ${AuctionEstate_estate_id} && Users_user_id=${Users_user_id} && price=${price} `
      );
      if (result.stmt.changes === 0) {
        throw new Error(`InAuction does not exist`);
      }
      return true;
    } catch (e) {
      throw new Error(`couldn't delete the InAuction : ` + e.message);
    }
  };
  /**
   * deletes a ListingEstate
   * @param {number} id the id of the ListingEstate to delete
   * @returns {boolean} `true` if the ListingEstate was deleted, an error otherwise
   */
  const deleteListingEstate = async id => {
    try {
      const result = await db.run(
        SQL`DELETE FROM ListingEstate WHERE estate_id = ${id}`
      );
      if (result.stmt.changes === 0) {
        throw new Error(`ListingEstate "${id}" does not exist`);
      }
      return true;
    } catch (e) {
      throw new Error(
        `couldn't delete the ListingEstate "${id}": ` + e.message
      );
    }
  };
  /**
   * deletes a ListingEstateImages
   * @param {number} id the id of the ListingEstateImages to delete
   * @returns {boolean} `true` if the ListingEstateImages was deleted, an error otherwise
   */
  const deleteListingEstateImages = async (id) => {
    try {
      const result = await db.run(
        SQL`DELETE FROM ListingEstateImages WHERE estate_image=${id}`
      );
      if (result.stmt.changes === 0) {
        throw new Error(`ListingEstateImages "${id}" does not exist`);
      }
      return true;
    } catch (e) {
      throw new Error(
        `couldn't delete the ListingEstateImages "${id}": ` + e.message
      );
    }
  };
  /**
   * deletes a SoldEstate
   * @param {number} id the id of the SoldEstate to delete
   * @returns {boolean} `true` if the SoldEstate was deleted, an error otherwise
   */
  const deleteSoldEstate = async id => {
    try {
      const result = await db.run(
        SQL`DELETE FROM SoldEstate WHERE sold_id = ${id}`
      );
      if (result.stmt.changes === 0) {
        throw new Error(`SoldEstate "${id}" does not exist`);
      }
      return true;
    } catch (e) {
      throw new Error(`couldn't delete the SoldEstate "${id}": ` + e.message);
    }
  };
  /**
   * deletes a Testimonial
   * @param {number} id the id of the Testimonial to delete
   * @returns {boolean} `true` if the Testimonial was deleted, an error otherwise
   */
  const deleteTestimonial = async id => {
    try {
      const result = await db.run(
        SQL`DELETE FROM Testimonial WHERE testimonial_id = ${id}`
      );
      if (result.stmt.changes === 0) {
        throw new Error(`Testimonial "${id}" does not exist`);
      }
      return true;
    } catch (e) {
      throw new Error(`couldn't delete the Testimonial "${id}": ` + e.message);
    }
  };
  /**
   * deletes a Users
   * @param {number} id the id of the Users to delete
   * @returns {boolean} `true` if the Users was deleted, an error otherwise
   */
  const deleteUsers = async id => {
    try {
      const result = await db.run(SQL`DELETE FROM Users WHERE user_id = ${id}`);
      if (result.stmt.changes === 0) {
        throw new Error(`Users "${id}" does not exist`);
      }
      return true;
    } catch (e) {
      throw new Error(`couldn't delete the Users "${id}": ` + e.message);
    }
  };
  //////////////////////////////////////////////////////////////////////////////////////
  /**
   * Edits a AuctionEstate
   * @param {number} id the id of the AuctionEstate to edit
   * @param {object} props an object with at least one of `name` or `email`
   */

  const updateAuctionEstate = async (id, props) => {
    const {
      estate_available,
      estate_start_price,
      estate_garage,
      estate_room,
      estate_bathroom,
      estate_height,
      estate_width,
      estate_floor,
      estate_location,
      estate_description,
      estate_date,
      estate_video
    } = props;
    const result = await db.run(SQL`UPDATE AuctionEstate SET estate_available=${estate_available},
    estate_start_price=${estate_start_price},estate_garage= ${estate_garage}, estate_room=${estate_room},estate_bathroom= ${estate_bathroom}, estate_height=${estate_height},
    estate_width=${estate_width}, estate_floor=${estate_floor},estate_location=${estate_location},estate_description=${estate_description},estate_date=${estate_date},estate_video=${estate_video} WHERE estate_id = ${id}`);
    if (result.stmt.changes === 0) {
      return false;
    }
    return true;
  };
  /**
   * Edits a AuctionEstateImage
   * @param {number} id the id of the AuctionEstateImage  to edit
   * @param {object} props an object with at least one of `name` or `email`
   */

  const updateAuctionEstateImage = async (id,  props) => {
    const { AuctionEstate_estate_id, estate_image } = props;
    const result = await db.run(SQL`UPDATE AuctionEstateImage  SET AuctionEstate_estate_id=${AuctionEstate_estate_id},
    estate_image=${estate_image} WHERE AuctionEstate_estate_id = ${id}`);
    if (result.stmt.changes === 0) {
      return false;
    }
    return true;
  };
  /**
   * Edits a Blog
   * @param {number} id the id of the Blog  to edit
   * @param {object} props an object with at least one of `name` or `email`
   */

  const updateBlog = async (id, props) => {
    const { blog_title, blog_description, blog_date } = props;
    const result = await db.run(SQL`UPDATE Blog  SET blog_title=${blog_title},
    blog_description=${blog_description},blog_date=${blog_date} WHERE blog_id = ${id} `);
    if (result.stmt.changes === 0) {
      return false;
    }
    return true;
  };
  /**
   * Edits a ListingEstate
   * @param {number} id the id of the ListingEstate to edit
   * @param {object} props an object with at least one of `name` or `email`
   */

  const updateListingEstate = async (id, props) => {
    const {
      estate_available,
      estate_price,
      estate_garage,
      estate_room,
      estate_bathroom,
      estate_height,
      estate_width,
      estate_video,
      estate_floor,
      estate_location,
      estate_description,
      estate_date
    } = props;
    const result = await db.run(SQL`UPDATE ListingEstate SET estate_available=${estate_available},
    estate_price=${estate_price},estate_garage= ${estate_garage}, estate_room=${estate_room},estate_bathroom= ${estate_bathroom}, estate_height=${estate_height},
    estate_width=${estate_width}, estate_floor=${estate_floor},estate_location=${estate_location},estate_description=${estate_description},estate_date=${estate_date},estate_video=${estate_video} WHERE estate_id = ${id}`);
    if (result.stmt.changes === 0) {
      return false;
    }
    return true;
  };
  /**
   * Edits a ListingEstateImages
   * @param {number} id the id of the ListingEstateImages  to edit
   * @param {object} props an object with at least one of `name` or `email`
   */

  const updateListingEstateImages = async (id,  props) => {
    const { ListingEstate_estate_id,estate_image} = props;
    const result = await db.run(SQL`UPDATE ListingEstateImages  SET ListingEstate_estate_id=${ListingEstate_estate_id},
    estate_image=${estate_image} WHERE ListingEstate_estate_id = ${id} `);
    if (result.stmt.changes === 0) {
      return false;
    }
    return true;
  };
  /**
   * Edits a Testimonial
   * @param {number} id the id of the Testimonial  to edit
   * @param {object} props an object with at least one of `name` or `email`
   */

  const updateTestimonial = async (id, props) => {
    const { testimonial_author,testimonial_description} = props;
    const result = await db.run(SQL`UPDATE Testimonial  SET testimonial_author=${testimonial_author},
    testimonial_description=${testimonial_description} WHERE testimonial_id = ${id} `);
    if (result.stmt.changes === 0) {
      return false;
    }
    return true;
  };
  /**
   * Edits a Users
   * @param {number} id the id of the Users  to edit
   * @param {object} props an object with at least one of `name` or `email`
   */

  const updateUsers = async (id, props) => {
    const { name,phone,address} = props;
    const result = await db.run(SQL`UPDATE Users  SET name=${name},
    phone=${phone},address=${address} WHERE user_id = ${id} `);
    if (result.stmt.changes === 0) {
      return false;
    }
    return true;
  };
  //////////////////////////////////////////////////////////////////////////////////////
  /**
   * Retrieves a AuctionEstate
   * @param {number} id the id of the AuctionEstate
   * @returns {object} an object , representing a AuctionEstate, or an error 
   */
  const getAuctionEstate = async (id) => {
    try{
      const AuctionEstateList = await db.all(SQL`SELECT estate_id AS id, estate_available ,estate_start_price,estate_garage,estate_room, estate_bathroom,estate_height,estate_width
      estate_floor, estate_location, estate_description,estate_date, estate_video FROM AuctionEstate WHERE estate_id = ${id}`);
      const AuctionEstate = AuctionEstateList[0]
      if(!AuctionEstate){
        throw new Error(`AuctionEstate ${id} not found`)
      }
      return AuctionEstate
    }catch(e){
      throw new Error(`couldn't get the AuctionEstate ${id}: `+e.message)
    }
  }
   /**
   * Retrieves a AuctionEstate
   * @param {number} id the id of the AuctionEstate
   * @returns {object} an object , representing a AuctionEstate, or an error 
   */
  const getAuctionEstateImage = async (id) => {
    try{
      const AuctionEstateImageList = await db.all(SQL`SELECT AuctionEstate_estate_id,estate_image FROM AuctionEstateImage WHERE AuctionEstate_estate_id = ${id}`);
      //const AuctionEstateImage = AuctionEstateImageList[0]
      if(!AuctionEstateImageList){
        throw new Error(`AuctionEstate ${id} not found`)
      }
      return AuctionEstateImageList
    }catch(e){
      throw new Error(`couldn't get the AuctionEstate ${id}: `+e.message)
    }
  }
  /**
   * Retrieves a Blog
   * @param {number} id the id of the Blog
   * @returns {object} an object with  and `id`, representing a Blog, or an error 
   */
  const getBlog = async (id) => {
    try{
      const BlogList = await db.all(SQL`SELECT blog_id AS id, blog_title,blog_description,blog_date FROM Blog WHERE blog_id = ${id}`);
      const Blog = BlogList[0]
      if(!Blog){
        throw new Error(`Blog ${id} not found`)
      }
      return Blog
    }catch(e){
      throw new Error(`couldn't get the Blog ${id}: `+e.message)
    }
  }
  /**
   * Retrieves a InAuction
   * @param {number} id the id of the InAuction
   * @returns {object} an object with  and `id`, representing a InAuction, or an error 
   */
  const getInAuction = async (Users_user_id) => {
    try{
      const InAuctionList = await db.all(SQL`SELECT AuctionEstate_estate_id,Users_user_id,inauction_date,time,price FROM InAuction WHERE Users_user_id=${Users_user_id}`);
     // const InAuction = InAuctionList[0]
      if(!InAuction){
        throw new Error(`InAuction ${id} not found`)
      }
      return InAuction
    }catch(e){
      throw new Error(`couldn't get the InAuction ${id}: `+e.message)
    }
  }
   /**
   * Retrieves a ListingEstate
   * @param {number} id the id of the ListingEstate
   * @returns {object} an object , representing a ListingEstate, or an error 
   */
  const getListingEstate = async (id) => {
    try{
      const ListingEstateList = await db.all(SQL`SELECT estate_id AS id, estate_available ,estate_price,estate_garage,estate_room, estate_bathroom,estate_height,estate_width
      estate_floor, estate_location, estate_description,estate_date, estate_video FROM ListingEstate WHERE estate_id = ${id}`);
      const ListingEstate = ListingEstateList[0]
      if(!ListingEstate){
        throw new Error(`ListingEstate ${id} not found`)
      }
      return ListingEstate
    }catch(e){
      throw new Error(`couldn't get the ListingEstate ${id}: `+e.message)
    }
  }
 /**
   * Retrieves a ListingEstateImages
   * @param {number} id the id of the ListingEstateImages
   * @returns {object} an object , representing a ListingEstateImages, or an error 
   */
  const getListingEstateImages = async (id) => {
    try{
      const ListingEstateImagesList = await db.all(SQL`SELECT ListingEstate_estate_id,estate_image FROM ListingEstateImages WHERE ListingEstate_estate_id = ${id} `);
      //const ListingEstateImages = ListingEstateImagesList[0]
      if(!ListingEstateImagesList){
        throw new Error(`ListingEstateImages ${id} not found`)
      }
      return ListingEstateImagesList
    }catch(e){
      throw new Error(`couldn't get the ListingEstateImages ${id}: `+e.message)
    }
  }
/**
   * Retrieves a SoldEstate
   * @param {number} id the id of the SoldEstate
   * @returns {object} an object, and `id`, representing a SoldEstate, or an error 
   */
  const getSoldEstate = async (id) => {
    try{
      const SoldEstateList = await db.all(SQL`SELECT sold_id As id,type,ListingEstate_estate_id,Users_user_id,sold_date FROM SoldEstate WHERE sold_id = ${id}`);
      const SoldEstate = SoldEstateList[0]
      if(!SoldEstate){
        throw new Error(`SoldEstate ${id} not found`)
      }
      return SoldEstate
    }catch(e){
      throw new Error(`couldn't get the SoldEstate ${id}: `+e.message)
    }
  }
  /**
   * Retrieves a Testimonial
   * @param {number} id the id of the Testimonial
   * @returns {object} an object, and `id`, representing a Testimonial, or an error 
   */
  const getTestimonial = async (id) => {
    try{
      const TestimonialList = await db.all(SQL`SELECT testimonial_id As id,testimonial_author,testimonial_description FROM Testimonial WHERE testimonial_id = ${id}`);
      const Testimonial = TestimonialList[0]
      if(!Testimonial){
        throw new Error(`Testimonial ${id} not found`)
      }
      return Testimonial
    }catch(e){
      throw new Error(`couldn't get the Testimonial ${id}: `+e.message)
    }
  }
   /**
   * Retrieves a Users
   * @param {number} id the id of the Users
   * @returns {object} an object, and `id`, representing a Users, or an error 
   */
  const getUsers = async (id) => {
    try{
      const UsersList = await db.all(SQL`SELECT user_id As id,name,phone,address FROM Users WHERE user_id = ${id}`);
      const Users = UsersList[0]
      if(!Users){
        throw new Error(`Users ${id} not found`)
      }
      return Users
    }catch(e){
      throw new Error(`couldn't get the Users ${id}: `+e.message)
    }
  }

//////////////////////////////////////////////////////////////////////////////////////////////
 /**
   * retrieves the AuctionEstate from the database
   * @param {string} orderBy an optional 
   * @returns {array} the list of AuctionEstate
   */
  const getAuctionEstateList = async (orderBy) => {
    try{
      
      let statement = `SELECT estate_id AS id, estate_available ,estate_start_price,estate_garage,estate_room, estate_bathroom,estate_height,estate_width
      estate_floor, estate_location, estate_description,estate_date, estate_video FROM AuctionEstate`
      switch(orderBy){
        case 'estate_date': statement+= ` ORDER BY estate_date`; break;
        case 'estate_start_price': statement+= ` ORDER BY estate_start_price`; break;
        default: break;
      }
      const rows = await db.all(statement)
      if(!rows.length){
        throw new Error(`no rows found`)
      }
      return rows
    }catch(e){
      throw new Error(`couldn't retrieve AuctionEstate: `+e.message)
    }
  }
  /**
   * retrieves the AuctionEstateImage from the database
   * @param {string} orderBy an optional 
   * @returns {array} the list of AuctionEstateImage
   */
  const getAuctionEstateImageList = async (orderBy) => {
    try{
      
      let statement = `SELECT AuctionEstate_estate_id,estate_image FROM AuctionEstateImage`
      switch(orderBy){
        case 'AuctionEstate_estate_id': statement+= ` ORDER BY AuctionEstate_estate_id`; break;
        case 'estate_image': statement+= ` ORDER BY estate_image`; break;
        default: break;
      }
      const rows = await db.all(statement)
      if(!rows.length){
        throw new Error(`no rows found`)
      }
      return rows
    }catch(e){
      throw new Error(`couldn't retrieve AuctionEstateImage: `+e.message)
    }
  }
  const getBlogList = async (orderBy) => {
    try{
      
      let statement = `SELECT blog_id AS id, blog_title,blog_description,blog_date FROM Blog`
      switch(orderBy){
        case 'blog_date': statement+= ` ORDER BY blog_date`; break;
        case 'blog_title': statement+= ` ORDER BY blog_title`; break;
        default: break;
      }
      const rows = await db.all(statement)
      if(!rows.length){
        throw new Error(`no rows found`)
      }
      return rows
    }catch(e){
      throw new Error(`couldn't retrieve Blog: `+e.message)
    }
  }

  const getInAuctionList = async (orderBy) => {
    try{
      
      let statement = `SELECT AuctionEstate_estate_id,Users_user_id,inauction_date,time,price FROM InAuction`
      switch(orderBy){
        case 'inauction_date': statement+= ` ORDER BY inauction_date`; break;
        case 'price': statement+= ` ORDER BY price`; break;
        default: break;
      }
      const rows = await db.all(statement)
      if(!rows.length){
        throw new Error(`no rows found`)
      }
      return rows
    }catch(e){
      throw new Error(`couldn't retrieve InAuction: `+e.message)
    }
  }
  const getListingEstateList = async (orderBy) => {
    try{
      
      let statement = `SELECT estate_id AS id, estate_available,estate_price,estate_garage,estate_room,estate_bathroom,estate_height,estate_width,
      estate_video, estate_floor,estate_location,estate_description,estate_date FROM ListingEstate`
      switch(orderBy){
        case 'estate_price': statement+= ` ORDER BY estate_price`; break;
        case 'estate_date': statement+= ` ORDER BY estate_date`; break;
        default: break;
      }
      const rows = await db.all(statement)
      if(!rows.length){
        throw new Error(`no rows found`)
      }
      return rows
    }catch(e){
      throw new Error(`couldn't retrieve ListingEstate: `+e.message)
    }
  }
 /**
   * retrieves the ListingEstateImages from the database
   * @param {string} orderBy an optional 
   * @returns {array} the list of ListingEstateImages
   */
  const getListingEstateImagesList = async (orderBy) => {
    try{
      
      let statement = `SELECT ListingEstate_estate_id,estate_image FROM ListingEstateImages`
      switch(orderBy){
        case 'ListingEstate_estate_id': statement+= ` ORDER BY ListingEstate_estate_id`; break;
        case 'estate_image': statement+= ` ORDER BY estate_image`; break;
        default: break;
      }
      const rows = await db.all(statement)
      if(!rows.length){
        throw new Error(`no rows found`)
      }
      return rows
    }catch(e){
      throw new Error(`couldn't retrieve ListingEstateImages: `+e.message)
    }
  }
   /**
   * retrieves the SoldEstate from the database
   * @param {string} orderBy an optional string
   * @returns {array} the list of SoldEstate
   */
  const getSoldEstateList = async (orderBy) => {
    try{
      
      let statement = `SELECT sold_id AS id, type,ListingEstate_estate_id,Users_user_id,sold_date FROM SoldEstate`
      switch(orderBy){
        case 'type': statement+= ` ORDER BY type`; break;
        case 'sold_date': statement+= ` ORDER BY sold_date`; break;
        default: break;
      }
      const rows = await db.all(statement)
      if(!rows.length){
        throw new Error(`no rows found`)
      }
      return rows
    }catch(e){
      throw new Error(`couldn't retrieve SoldEstate: `+e.message)
    }
  }
 /**
   * retrieves the Testimonial from the database
   * @param {string} orderBy an optional 
   * @returns {array} the list of Testimonial
   */
  const getTestimonialList = async (orderBy) => {
    try{
      
      let statement = `SELECT testimonial_id AS id, testimonial_author,testimonial_description FROM Testimonial`
      switch(orderBy){
        case 'testimonial_author': statement+= ` ORDER BY testimonial_author`; break;
        case 'testimonial_description': statement+= ` ORDER BY testimonial_description`; break;
        default: break;
      }
      const rows = await db.all(statement)
      if(!rows.length){
        throw new Error(`no rows found`)
      }
      return rows
    }catch(e){
      throw new Error(`couldn't retrieve Testimonial: `+e.message)
    }
  }
  /**
   * retrieves the Users from the database
   * @param {string} orderBy an optional 
   * @returns {array} the list of Users
   */
  const getUsersList = async (orderBy) => {
    try{
      
      let statement = `SELECT user_id AS id, name,phone,address FROM Users`
      switch(orderBy){
        case 'name': statement+= ` ORDER BY name`; break;
        case 'phone': statement+= ` ORDER BY phone`; break;
        default: break;
      }
      const rows = await db.all(statement)
      if(!rows.length){
        throw new Error(`no rows found`)
      }
      return rows
    }catch(e){
      throw new Error(`couldn't retrieve Users: `+e.message)
    }
  }
const getCurrentAuction = async (id)=>{
  try{
    const InAuctionList = await db.all(SQL`select max(price) as price from InAuction WHERE AuctionEstate_estate_id = ${id}`);
    const currentprice = InAuctionList[0]
    if(!currentprice){
      throw new Error(`Auction ${id} not found`)

    }
    return currentprice
  }catch(e){
    throw new Error(`couldn't get the Auction ${id}: `+e.message)
  }
}
/////////////////////////////////////////////////////////////////////////////////////////////
  const controller = {
    createAuctionEstate,
    createSell,
    createAuctionEstateImage,
    createBlog,
    createInAuction,
    createListingEstate,
    createListingEstateImages,
    createSoldEstate,
    createTestimonial,
    createUser,
    deleteAuctionEstate,
    deleteAuctionEstateImage,
    deleteBlog,
    deleteInAuction,
    deleteListingEstate,
    deleteListingEstateImages,
    deleteSoldEstate,
    deleteTestimonial,
    deleteUsers,
    updateAuctionEstate,
    updateAuctionEstateImage,
    updateBlog,
    updateListingEstate,
    updateListingEstateImages,
    updateTestimonial,
    updateUsers,
    getAuctionEstate,
    getAuctionEstateImage,
    getBlog,
    getInAuction,
    getListingEstate,
    getListingEstateImages,
    getSoldEstate,
    getTestimonial,
    getUsers,
    getAuctionEstateList,
    getAuctionEstateImageList,
    getBlogList,
    getInAuctionList,
    getListingEstateList,
    getListingEstateImagesList,
    getSoldEstateList,
    getTestimonialList,
    getUsersList,
    getCurrentAuction
  };

  return controller;
};

export default initializeDatabase;
