import app from "./app";
import { authenticateUser, logout, isLoggedIn } from './auth'
import initializeDatabase from "./db";
var nodemailer = require("nodemailer");
const start = async () => {
  const controller = await initializeDatabase();
  //for testing
  app.get("/", (req, res) => res.send("ok"));
///////////////Authentication/////////////////////////

app.get('/mypage', isLoggedIn, ( req, res ) => {
  const username = req.user.name
  res.send({success:true, result: 'ok, user '+username+' has access to this page'})
})

///////////////Authentication/////////////////////////
  ////////////////////////////AuctionEstate///////////////////////////////////////////
  //create
  app.get("/auction/new", async (req, res, next) => {
    try {
      const {
        estate_available,
        estate_start_price,
        estate_garage,
        estate_room,
        estate_bathroom,
        estate_height,
        estate_width,
        estate_floor,
        estate_location,
        estate_description,
        estate_date,
        estate_video
      } = req.query;
      const result = await controller.createAuctionEstate({
        estate_available,
        estate_start_price,
        estate_garage,
        estate_room,
        estate_bathroom,
        estate_height,
        estate_width,
        estate_floor,
        estate_location,
        estate_description,
        estate_date,
        estate_video
      });
      res.json({ success: true, result });
    } catch (e) {
      next(e);
    }
  });
  //read
  app.get("/auction/get/:id", async (req, res, next) => {
    try {
      const { id } = req.params;
      const result = await controller.getAuctionEstate(id);
      res.json({ success: true, result });
    } catch (e) {
      next(e);
    }
  });
  //Delete
  app.get("/auction/delete/:id", async (req, res, next) => {
    try {
      const { id } = req.params;
      const result = await controller.deleteAuctionEstate(id);
      res.json({ success: true, result });
    } catch (e) {
      next(e);
    }
  });
  //update
  app.get("/auction/update/:id", async (req, res, next) => {
    try {
      const { id } = req.params;
      const {
        estate_available,
        estate_start_price,
        estate_garage,
        estate_room,
        estate_bathroom,
        estate_height,
        estate_width,
        estate_floor,
        estate_location,
        estate_description,
        estate_date,
        estate_video
      } = req.query;
      const result = await controller.updateAuctionEstate(id, {
        estate_available,
        estate_start_price,
        estate_garage,
        estate_room,
        estate_bathroom,
        estate_height,
        estate_width,
        estate_floor,
        estate_location,
        estate_description,
        estate_date,
        estate_video
      });
      res.json({ success: true, result });
    } catch (e) {
      next(e);
    }
  });
  //list
  app.get("/auction/list", async (req, res, next) => {
    try {
      const { order } = req.query;
      const result = await controller.getAuctionEstateList(order);
      res.json({ success: true, result });
    } catch (e) {
      next(e);
    }
  });
  ////////////////////////////AuctionEstate///////////////////////////////////////////
  ////////////////////////////AuctionEstateImage///////////////////////////////////////////
  //create
  app.get("/auction/image/new", async (req, res, next) => {
    try {
      const { AuctionEstate_estate_id, estate_image } = req.query;
      const result = await controller.createAuctionEstateImage({
        AuctionEstate_estate_id,
        estate_image
      });
      res.json({ success: true, result });
    } catch (e) {
      next(e);
    }
  });
  //read
  app.get("/auction/image/get/:id", async (req, res, next) => {
    try {
      const { id } = req.params;
      const result = await controller.getAuctionEstateImage(id);
      res.json({ success: true, result });
    } catch (e) {
      next(e);
    }
  });
  //Delete
  // http://razanestate.com/auction/image/sort//asc
  app.get("/auction/image/delete/:id", async (req, res, next) => {
    try {
      // params: { xxx: "blah", yyy:"" }
      // query: { a: "1", b:"2", v: "3" }
      // body: {}
      const { id } = req.query;
      const result = await controller.deleteAuctionEstateImage(id);
      res.json({ success: true, result });
    } catch (e) {
      next(e);
    }
  });
  //update
  app.get("/auction/image/update/:id", async (req, res, next) => {
    try {
      const { id } = req.params;
      const { AuctionEstate_estate_id, estate_image } = req.query;
      const result = await controller.updateAuctionEstate(id, {
        AuctionEstate_estate_id,
        estate_image
      });
      res.json({ success: true, result });
    } catch (e) {
      next(e);
    }
  });
  //list
  app.get("/auction/image/list", async (req, res, next) => {
    try {
      const { order } = req.query;
      const result = await controller.getAuctionEstateImageList(order);
      res.json({ success: true, result });
    } catch (e) {
      next(e);
    }
  });
  ////////////////////////////AuctionEstateImage///////////////////////////////////////////
  ////////////////////////////Blog///////////////////////////////////////////
  //create
  app.get("/blog/new", async (req, res, next) => {
    try {
      const { blog_title, blog_description, blog_date } = req.query;
      const result = await controller.createBlog({
        blog_title,
        blog_description,
        blog_date
      });
      res.json({ success: true, result });
    } catch (e) {
      next(e);
    }
  });
  //read
  app.get("/blog/get/:id", async (req, res, next) => {
    try {
      const { id } = req.params;
      const result = await controller.getBlog(id);
      res.json({ success: true, result });
    } catch (e) {
      next(e);
    }
  });
  //Delete

  app.get("/blog/delete/:id", async (req, res, next) => {
    try {
      const { id } = req.params;
      const result = await controller.deleteBlog(id);
      res.json({ success: true, result });
    } catch (e) {
      next(e);
    }
  });
  //update
  app.get("/blog/update/:id", async (req, res, next) => {
    try {
      const { id } = req.params;
      const { blog_title, blog_description, blog_date } = req.query;
      const result = await controller.updateBlog(id, {
        blog_title,
        blog_description,
        blog_date
      });
      res.json({ success: true, result });
    } catch (e) {
      next(e);
    }
  });
  //list
  app.get("/blog/list", async (req, res, next) => {
    try {
      const { order } = req.query;
      const result = await controller.getBlogList(order);
      res.json({ success: true, result });
    } catch (e) {
      next(e);
    }
  });
  ////////////////////////////Blog///////////////////////////////////////////
  ////////////////////////////InAuction///////////////////////////////////////////
  //create
  app.get("/inauction/new",isLoggedIn, async (req, res, next) => {
     
    try {
      const {
        AuctionEstate_estate_id,
        inauction_date,
        time,
        price
      } = req.query;
     const Users_user_id = req.user.sub
     console.log("request: ",req);
      const result = await controller.createInAuction({
        AuctionEstate_estate_id,
        Users_user_id,
        inauction_date,
        time,
        price
      });
      res.json({ success: true, result });
    } catch (e) {
      next(e);
    }
  });
  //read
  app.get("/inauction/get/:id", async (req, res, next) => {
    try {
      const { id } = req.params;
      const result = await controller.getInAuction(id);
      res.json({ success: true, result });
    } catch (e) {
      next(e);
    }
  });
  //list
  app.get("/inauction/list", async (req, res, next) => {
    try {
      const { order } = req.query;
      const result = await controller.getInAuction(order);
      res.json({ success: true, result });
    } catch (e) {
      next(e);
    }
  });
  ////////////////////////////InAuction///////////////////////////////////////////
  ///sell
  //create
  app.get("/sell/new", async (req, res, next) => {
    try {
      const {
        name,
        phone,
        address,
        price,
        description,
        email,
        type
      } = req.query;
      const result = await controller.createSell({
        name,
        phone,
        address,
        price,
        description,
        email,
        type
      });
      res.json({ success: true, result });
    } catch (e) {
      next(e);
    }
  });
  //sell
  ////////////////////////////ListingEstate///////////////////////////////////////////
  //create
  app.get("/estate/new", async (req, res, next) => {
    try {
      const {
        estate_available,
        estate_price,
        estate_garage,
        estate_room,
        estate_bathroom,
        estate_height,
        estate_width,
        estate_video,
        estate_floor,
        estate_location,
        estate_description,
        estate_date
      } = req.query;
      const result = await controller.createListingEstate({
        estate_available,
        estate_price,
        estate_garage,
        estate_room,
        estate_bathroom,
        estate_height,
        estate_width,
        estate_video,
        estate_floor,
        estate_location,
        estate_description,
        estate_date
      });
      res.json({ success: true, result });
    } catch (e) {
      next(e);
    }
  });
  //read
  app.get("/estate/get/:id", async (req, res, next) => {
    try {
      const { id } = req.params;
      const result = await controller.getListingEstate(id);
      res.json({ success: true, result });
    } catch (e) {
      next(e);
    }
  });
  //Delete

  app.get("/estate/delete/:id", async (req, res, next) => {
    try {
      const { id } = req.params;
      const result = await controller.deleteListingEstate(id);
      res.json({ success: true, result });
    } catch (e) {
      next(e);
    }
  });
  //update
  app.get("/estate/update/:id", async (req, res, next) => {
    try {
      const { id } = req.params;
      const {
        estate_available,
        estate_price,
        estate_garage,
        estate_room,
        estate_bathroom,
        estate_height,
        estate_width,
        estate_video,
        estate_floor,
        estate_location,
        estate_description,
        estate_date
      } = req.query;
      const result = await controller.updateListingEstate(id, {
        estate_available,
        estate_price,
        estate_garage,
        estate_room,
        estate_bathroom,
        estate_height,
        estate_width,
        estate_video,
        estate_floor,
        estate_location,
        estate_description,
        estate_date
      });
      res.json({ success: true, result });
    } catch (e) {
      next(e);
    }
  });
  //list
  app.get("/estate/list", async (req, res, next) => {
    try {
      const { order } = req.query;
      const result = await controller.getListingEstateList(order);
      res.json({ success: true, result });
    } catch (e) {
      next(e);
    }
  });
  ////////////////////////////ListingEstate///////////////////////////////////////////
  ////////////////////////////ListingEstateImage///////////////////////////////////////////
  //create
  app.get("/estate/image/new", async (req, res, next) => {
    try {
      const { ListingEstate_estate_id, estate_image } = req.query;
      const result = await controller.createListingEstateImages({
        ListingEstate_estate_id,
        estate_image
      });
      res.json({ success: true, result });
    } catch (e) {
      next(e);
    }
  });
  //read
  app.get("/estate/image/get/:id", async (req, res, next) => {
    try {
      const { id } = req.params;
      const result = await controller.getListingEstateImages(id);
      res.json({ success: true, result });
    } catch (e) {
      next(e);
    }
  });
  //Delete

  app.get("/estate/image/delete/:id", async (req, res, next) => {
    try {
      const { id } = req.query;
      const result = await controller.deleteListingEstateImages(id);
      res.json({ success: true, result });
    } catch (e) {
      next(e);
    }
  });
  //update
  app.get("/estate/image/update/:id", async (req, res, next) => {
    try {
      const { id } = req.params;
      const { ListingEstate_estate_id, estate_image } = req.query;
      const result = await controller.updateListingEstateImages(id, {
        ListingEstate_estate_id,
        estate_image
      });
      res.json({ success: true, result });
    } catch (e) {
      next(e);
    }
  });
  //list
  app.get("/estate/image/list", async (req, res, next) => {
    try {
      const { order } = req.query;
      const result = await controller.getListingEstateImagesList(order);
      res.json({ success: true, result });
    } catch (e) {
      next(e);
    }
  });
  ////////////////////////////AuctionEstateImage///////////////////////////////////////////
  ////////////////////////////SoldEstate///////////////////////////////////////////
  //create
  app.get("/sold/new", async (req, res, next) => {
    try {
      const {
        type,
        ListingEstate_estate_id,
        Users_user_id,
        sold_date
      } = req.query;
      Blog;
      const result = await controller.createSoldEstate({
        type,
        ListingEstate_estate_id,
        Users_user_id,
        sold_date
      });
      res.json({ success: true, result });
    } catch (e) {
      next(e);
    }
  });
  //read
  app.get("/sold/get/:id", async (req, res, next) => {
    try {
      const { id } = req.params;
      const result = await controller.getSoldEstate(id);
      res.json({ success: true, result });
    } catch (e) {
      next(e);
    }
  });
  //Delete

  app.get("/sold/delete/:id", async (req, res, next) => {
    try {
      const { id } = req.query;
      const result = await controller.deleteSoldEstate(id);
      res.json({ success: true, result });
    } catch (e) {
      next(e);
    }
  });
  //update
  // app.get("/sold/update/:id", async (req, res, next) => {
  //   try {
  //     const { id } = req.params;
  //     const {
  //       type,ListingEstate_estate_id,Users_user_id,sold_date
  //     } = req.query;
  //     const result = await controller.updateSoldEstate(id, {
  //       type,ListingEstate_estate_id,Users_user_id,sold_date
  //     });
  //     res.json({ success: true, result });
  //   } catch (e) {
  //     next(e);
  //   }
  // });
  //list
  app.get("/sold/list", async (req, res, next) => {
    try {
      const { order } = req.query;
      const result = await controller.getSoldEstateList(order);
      res.json({ success: true, result });
    } catch (e) {
      next(e);
    }
  });
  ////////////////////////////SoldEstate///////////////////////////////////////////
  ////////////////////////////Testimonial ///////////////////////////////////////////
  //create
  app.get("/testimonial/new", async (req, res, next) => {
    try {
      const { testimonial_author, testimonial_description } = req.query;
      const result = await controller.createTestimonial({
        testimonial_author,
        testimonial_description
      });
      res.json({ success: true, result });
    } catch (e) {
      next(e);
    }
  });
  //read
  app.get("/testimonial/get/:id", async (req, res, next) => {
    try {
      const { id } = req.params;
      const result = await controller.getTestimonial(id);
      res.json({ success: true, result });
    } catch (e) {
      next(e);
    }
  });
  //Delete

  app.get("/testimonial/delete/:id", async (req, res, next) => {
    try {
      const { id } = req.params;
      const result = await controller.deleteTestimonial(id);
      res.json({ success: true, result });
    } catch (e) {
      next(e);
    }
  });
  //update
  app.get("/testimonial/update/:id", async (req, res, next) => {
    try {
      const { id } = req.params;
      const { testimonial_author, testimonial_description } = req.query;
      const result = await controller.updateTestimonial(id, {
        testimonial_author,
        testimonial_description
      });
      res.json({ success: true, result });
    } catch (e) {
      next(e);
    }
  });
  //list
  app.get("/testimonial/list", async (req, res, next) => {
    try {
      const { order } = req.query;
      const result = await controller.getTestimonialList(order);
      res.json({ success: true, result });
    } catch (e) {
      next(e);
    }
  });
  ////////////////////////////Testimonial ///////////////////////////////////////////
  ////////////////////////////Users///////////////////////////////////////////
  //create
  app.get("/user/new", async (req, res, next) => {
    try {
      const { name, user_id, phone, address } = req.query;
      const result = await controller.createUser({
        name,
        user_id,
        phone,
        address
      });
      res.json({ success: true, result });
    } catch (e) {
      next(e);
    }
  });
  //read
  app.get("/user/get/:id", async (req, res, next) => {
    try {
      const { id } = req.params;
      const result = await controller.getUsers(id);
      res.json({ success: true, result });
    } catch (e) {
      next(e);
    }
  });
  //Delete

  app.get("/user/delete/:id", async (req, res, next) => {
    try {
      const { id } = req.params;
      const result = await controller.deleteUsers(id);
      res.json({ success: true, result });
    } catch (e) {
      next(e);
    }
  });
  //update
  app.get("/user/update/:id", async (req, res, next) => {
    try {
      const { id } = req.params;
      const { name, phone, address } = req.query;
      const result = await controller.updateUsers(id, {
        name,
        phone,
        address
      });
      res.json({ success: true, result });
    } catch (e) {
      next(e);
    }
  });
  //list
  app.get("/user/list", async (req, res, next) => {
    try {
      const { order } = req.query;
      const result = await controller.getUsersList(order);
      res.json({ success: true, result });
    } catch (e) {
      next(e);
    }
  });
  ///in auction price/////////

  app.get("/currentprice/get/:id", async (req, res, next) => {
    try {
      const { id } = req.params;

      const result = await controller.getCurrentAuction(id);
      res.json({ success: true, result });
    } catch (e) {
      next(e);
    }
  });
  ///////////////////////////Users///////////////////////////////////////////
  //errror
  app.use((err, res, req, next) => {
    console.error(err);
    const message = err.message;
    res.status(500).json({ success: false, message });
  });
  //For Sending emails
  app.post("/send", function(req, res, next) {
    const transporter = nodemailer.createTransport({
      service: "gmail",
      auth: {
        user: "mazadm009@gmail.com",
        pass: "mazad@123"
      }
    });
    const mailOptions = {
      from: `${req.body.email}`,
      to: "mazadm009@gmail.com",
      subject: `${req.body.name}`,
      text: `${req.body.message}`,
      replyTo: `${req.body.email}`
    };
    transporter.sendMail(mailOptions, function(err, res) {
      if (err) {
        console.error("there was an error: ", err);
      } else {
        console.log("here is the res: ", res);
      }
    });
  });
  //test
  app.listen(8080, () => console.log("server listening on port 8080"));
};

start();
